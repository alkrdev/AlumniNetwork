# AlumniNetwork

A Social Media concept created with ASP.NET Core API with React Frontend, featuring Entity Framework, Swagger / Open API and Keycloak.

## Install

```
git clone git@gitlab.com:alkrdev/AlumniNetwork.git
```

## Usage

1. Configure AlumniNetwork\appsettings.json and edit the Database ConnectionStrings and keycloak/OpenID settings to reflect your own environment. The App supports Heroku PGSQL, Local PGSQL and MSSQL.

    appsettings.json example:

    ```
    "ConnectionStrings": {
    "AlumniDbMSSQL": "server=; database=AlumniDb; Trusted_connection=true;",
    "AlumniDbPGSQL": "Server=;Port=5432;User Id=;Password=;Database=AlumniDb;"
    },
    "DatabaseType": "2", // 0 = Heroku, 1 = Local PGSQL, 2 = Local MSSQL
    "OpenIDKeyURI": "",
    "OpenIDIssuerURI": ""
    ```

2. Configure AlumniNetwork\ClientApp\public\keycloak.json to reflect the keycloak realm settings for the frontend.

    keycloak.json example:

    ```
    {
        "realm": "Alumni",
        "auth-server-url": "",
        "ssl-required": "external",
        "resource": "alumni-react-client",
        "public-client": true,
        "confidential-port": 0
    }

    ```

3. Create the AlumniDb database by running the following command in the PM console.

    ```
    add-migration initialdb
    update-database
    ```

4. Build and Run the Application in Visual Studio 2022. The Swagger / Open API webUI will start automatically.

## Dependencies

A keycloak instance must be used for authentication. A Docker image is located in AlumniNetwork\Keycloak

If the Docker image is deployed on Heroku, you must set the Config Vars for the App:

```
    KEYCLOAK_USER = admin
    KEYCLOAK_PASSWORD = {yourpassword}
    PROXY_ADDRESS_FORWARDING = true
```

## Contributors

-   [Alexander Kristensen (@alkrdev)](@alkrdev)
-   [Oliver Engermann (@OliverEng)](@OliverEng)
-   [Morten Bay Nielsen (@morten.bay)](@morten.bay)

## License

Noroff Accelerate, 2022.
