using AlumniNetwork.DbContexts;
using AlumniNetwork.Interfaces;
using AlumniNetwork.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using AlumniNetwork.Enums;
using AlumniNetwork.Utils;
using System.Reflection;
using Newtonsoft.Json;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
var configuration = builder.Configuration;

builder.Services.AddAutoMapper(typeof(Program));


// Dependency Injection for Repository Pattern
builder.Services.AddScoped<IEventRepository, EventRepository>();
builder.Services.AddScoped<IGroupRepository, GroupRepository>();
builder.Services.AddScoped<IPostRepository, PostRepository>();
builder.Services.AddScoped<ITopicRepository, TopicRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();

// Add DBContext with options based on the Database type.
var DatabaseTarget = builder.Configuration.GetValue<DatabaseType>("DatabaseType");

if (DatabaseTarget == DatabaseType.Heroku)
{
    // Production / Heroku builds use PostgreSQL with ConnectionString optained from Heroku Environment vars
    builder.Services.AddDbContext<AlumniDb>(opt => opt.UseNpgsql(HerokuConnectionHelper.GetConnectionString()));
}
else if (DatabaseTarget == DatabaseType.DevPGSQL)
{
    // Local build with PGSQL and ConnectionString from appsetting.json
    builder.Services.AddDbContext<AlumniDb>(opt => opt.UseNpgsql(builder.Configuration.GetConnectionString("AlumniDbPGSQL")));
}
else if (DatabaseTarget == DatabaseType.DevMSSQL)
{
    // Local build with MSSQL and ConnectionString from appsetting.json
    builder.Services.AddDbContext<AlumniDb>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("AlumniDbMSSQL")));
}
else
{
    throw new Exception("Invalid DatabaseTarget");
}

// Add Authentication with JwTBearer configuration.
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
{
    opt.TokenValidationParameters = new TokenValidationParameters
    {
        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
        {
            var client = new HttpClient();
            var keyuri = builder.Configuration.GetValue<string>("OpenIDKeyURI");
            //Retrieves the keys from keycloak instance to verify token
            var response = client.GetAsync(keyuri).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
            return keys.Keys;
        },

        ValidIssuers = new List<string>
        {
            builder.Configuration.GetValue<string>("OpenIDIssuerURI")
        },

        //This checks the token for a the 'aud' claim value
        ValidAudience = "account",
    };
});


// Add Swagger
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new()
    {
        Title = "AlumniNetwork",
        Version = "v1",
        Description = "ASP.NET Core Web API for Noroff Alumni: A social media platform for Noroff students and alumni.",
        TermsOfService = new Uri("https://example.com/terms"),
        Contact = new()
        {
            Name = "Alexander Kristensen",
            // Email = string.Empty,
            Url = new Uri("https://gitlab.com/alkrdev"),
        },
        License = new()
        {
            Name = "Use under MIT",
            Url = new Uri("https://opensource.org/licenses/MIT"),
        }
    });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme() {
        Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            Scheme = "Bearer",
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 1safsfsdfdfd\"",
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement {
        {
            new OpenApiSecurityScheme {
                Reference = new OpenApiReference {
                    Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                }
            },
            new string[] {}
        }
    });
    
    // Set the comments path for the Swagger JSON and UI
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AlumniNetwork v1"));
};

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

// Use Authentication and Authorization, N.B. needs to be run after UseRouting.
app.UseAuthentication();
app.UseAuthorization();

app.MapFallbackToFile("/index.html");

app.MapControllers().RequireAuthorization();


app.Run();
