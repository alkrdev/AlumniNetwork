﻿using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.User;
using AutoMapper;

namespace AlumniNetwork.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                // Turn related groups into int list with group ids
                .ForMember(dest => dest.Groups, opt => opt
                    .MapFrom(src => src.Groups.Select(g => g.Id).ToList()))
                // Turn related events into int list with event ids
                .ForMember(dest => dest.Events, opt => opt
                    .MapFrom(src => src.Events.Select(e => e.Id).ToList()))
                // Turn related posts into int list with post ids
                .ForMember(dest => dest.Posts, opt => opt
                    .MapFrom(src => src.Posts.Select(p => p.Id).ToList()))
                // Turn related topics into int list with topic ids
                .ForMember(dest => dest.Topics, opt => opt
                    .MapFrom(src => src.Topics.Select(t => t.Id).ToList()))
                // Turn related RSVPs into int list with RSVP ids
                .ForMember(dest => dest.RSVPs, opt => opt
                    .MapFrom(src => src.RSVPs.Select(r => r.Id).ToList()))
                .ReverseMap();
            CreateMap<UserCreateDTO, User>();
            CreateMap<UserEditDTO, User>();
        }
    }
}
