﻿using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Group;
using AutoMapper;

namespace AlumniNetwork.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>()
                // Turn related members into int list with user ids
                .ForMember(dest => dest.Members, opt => opt
                    .MapFrom(src => src.Members.Select(m => m.Id).ToList()))
                // Turn related events into int list with event ids
                .ForMember(dest => dest.Events, opt => opt
                    .MapFrom(src => src.Events.Select(e => e.Id).ToList()))
                .ReverseMap();
            CreateMap<GroupCreateDTO, Group>();
            CreateMap<GroupEditDTO, Group>();
        }
    }
}
