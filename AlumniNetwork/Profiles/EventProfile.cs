﻿using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Event;
using AutoMapper;

namespace AlumniNetwork.Profiles
{
    public class EventProfile : Profile
    {
        public EventProfile()
        {
            CreateMap<Event, EventReadDTO>()
                // Turn related posts into int list with post ids
                .ForMember(dest => dest.Posts, opt => opt
                    .MapFrom(src => src.Posts.Select(p => p.Id).ToList()))
                // Turn related RSVPs into int list RSVP ids
                .ForMember(dest => dest.RSVPs, opt => opt
                    .MapFrom(src => src.RSVPs.Select(r => r.Id).ToList()))
                // Turn related users into int list with user ids
                .ForMember(dest => dest.Users, opt => opt
                    .MapFrom(src => src.Users.Select(u => u.Id).ToList()))
                // Turn related groups into int list with group ids
                .ForMember(dest => dest.Groups, opt => opt
                    .MapFrom(src => src.Groups.Select(g => g.Id).ToList()))
                // Turn related posts into int list with topic ids
                .ForMember(dest => dest.Topics, opt => opt
                    .MapFrom(src => src.Topics.Select(t => t.Id).ToList()))
                .ReverseMap();
            CreateMap<EventCreateDTO, Event>();
            CreateMap<EventEditDTO, Event>();


        }
    }
}
