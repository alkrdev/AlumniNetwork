﻿using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Post;
using AutoMapper;

namespace AlumniNetwork.Profiles
{
    public class PostProfile: Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostReadDTO>()
                // Set created by user to id
                .ForMember(dest => dest.CreatedByUser, opt => opt
                    .MapFrom(src => src.CreatedByUserId))
                // Set name of created by user
                .ForMember(dest => dest.CreatedByUserName, opt => opt
                    .MapFrom(src => src.CreatedByUser.Name))
                // Set profile image of created by user
                .ForMember(dest => dest.CreatedByUserImage, opt => opt
                    .MapFrom(src => src.CreatedByUser.ProfileImage))
                // Set reply parent to id
                .ForMember(dest => dest.ReplyParent, opt => opt
                    .MapFrom(src => src.ReplyParent.Id))
                // Set target user to id
                .ForMember(dest => dest.TargetUser, opt => opt
                    .MapFrom(src => src.TargetUser.Id))
                // Set target user name to name of target user
                .ForMember(dest => dest.TargetUserName, opt => opt
                    .MapFrom(src => src.TargetUser.Name))
                // Set target event to id
                .ForMember(dest => dest.TargetEvent, opt => opt
                    .MapFrom(src => src.TargetEvent.Id))
                // Set target event name to name of target event
                .ForMember(dest => dest.TargetEventName, opt => opt
                    .MapFrom(src => src.TargetEvent.Body))
                // Set target group to id
                .ForMember(dest => dest.TargetGroup, opt => opt
                    .MapFrom(src => src.TargetGroup.Id))
                // Set target group name to name of target group
                .ForMember(dest => dest.TargetGroupName, opt => opt
                    .MapFrom(src => src.TargetGroup.Name))
                // Set target topic to id
                .ForMember(dest => dest.TargetTopic, opt => opt
                    .MapFrom(src => src.TargetTopic.Id))
                // Set target topic name to name of target topic
                .ForMember(dest => dest.TargetTopicName, opt => opt
                    .MapFrom(src => src.TargetTopic.Name))
                .ReverseMap();

            CreateMap<PostCreateDTO, Post>();
            CreateMap<PostEditDTO, Post>();
        }
    }
}
