﻿using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.RSVP;
using AutoMapper;

namespace AlumniNetwork.Profiles
{
    public class RSVPProfile : Profile
    {
        public RSVPProfile()
        {
            CreateMap<RSVP, RSVPReadDTO>()
                // Get the user id from the related user
                .ForMember(dest => dest.User, opt => opt
                    .MapFrom(src => src.User.Id))
                // Get the event id from the related event
                .ForMember(dest => dest.Event, opt => opt
                    .MapFrom(src => src.Event.Id))
                .ReverseMap();
            CreateMap<RSVPCreateDTO, RSVP>();
            CreateMap<RSVPEditDTO, RSVP>();
        }
    }
}
