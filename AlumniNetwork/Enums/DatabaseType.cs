﻿namespace AlumniNetwork.Enums
{
    /// <summary>
    /// Enum for database types.
    /// </summary>
    public enum DatabaseType
    {
        Heroku,
        DevPGSQL,
        DevMSSQL
    }
}
