﻿using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Event;
using AlumniNetwork.Models.DTO.RSVP;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace AlumniNetwork.Controllers
{
    /// <summary>
    /// Controller for handling requests related to events.
    /// </summary>
    [Route("api/event")]
    [ApiController]
    //[Produces(MediaTypeNames.Application.Json)]
    //[Consumes(MediaTypeNames.Application.Json)]
    //[ApiConventionType(typeof(DefaultApiConventions))]
    public class EventsController : ControllerBase
    {
        // Dependency injection of repository and automapper:
        private readonly IEventRepository _eventRepository;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Events controller constructor.
        /// </summary>
        /// <param name="eventRepository"></param>
        public EventsController(IEventRepository eventRepository, IUserRepository userRepository, IGroupRepository groupRepository, IMapper mapper)
        {
            _eventRepository = eventRepository;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
            _mapper = mapper;
        }

        // GET: api/event
        /// <summary>
        /// Get all events.
        /// </summary>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(EventReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetEvents([FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";

            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Get the events from the repository
                var domainEvents = await _eventRepository.GetEventsAsync(requestingUser.Id, searchQueryString, (int)offset, (int)limit);

                // Map the domain events to DTOs
                var dtoEvents = _mapper.Map<IEnumerable<EventReadDTO>>(domainEvents);

                // add attending users to each event
                foreach (var dtoEvent in dtoEvents)
                {
                    dtoEvent.AttendingUsers = await _eventRepository.GetAttendingUsersAsync(dtoEvent.Id);
                }

                // Return the DTOs
                return Ok(dtoEvents);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while getting events.");
            }
        }

        // GET: api/event/:event_id
        /// <summary>
        /// Get a single event by id.
        /// </summary>
        /// <param name="eventId"></param>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(EventReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EventReadDTO>> GetEvent(int id)
        {
            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                
                // Get the event from the repository
                var domainEvent = await _eventRepository.GetEventByIdAsync(id);

                // Check if the event was found
                if(domainEvent == null)
                {
                    return NotFound($"Event id {id} was not found.");
                }

                // Check if user is allowed to view the event
                if (!_eventRepository.CheckUserIsInvited(requestingUser.Id, id) && !_eventRepository.CheckUserIsMember(requestingUser.Id, domainEvent))
                {
                    // Return 403 if user is not allowed to view the event
                    return Unauthorized("You are not allowed to view this event.");
                }
                
                // Map the domain event to a DTO
                var dtoEvent = _mapper.Map<EventReadDTO>(domainEvent);
                dtoEvent.AttendingUsers = await _eventRepository.GetAttendingUsersAsync(id);

                // Return the DTO
                return Ok(dtoEvent);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while getting event.");
            }
        }

        // POST: api/event
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a new event.
        /// </summary>
        /// <param name="dtoEvent">Event to create</param>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(EventReadDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EventReadDTO>> PostEvent(EventCreateDTO dtoEvent)
        {

            // Map DTO to domain model
            var domainEvent = _mapper.Map<Event>(dtoEvent);
            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Add the event to the repository
                var createdEvent = _eventRepository.CreateEventAsync(requestingUser.Id, domainEvent);
                // return created event 201
                return CreatedAtAction(nameof(GetEvent), new { id = createdEvent.Id }, _mapper.Map<EventReadDTO>(createdEvent.Result));
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while creating event.");
            }
        }
        // PUT: api/event/:event_id
        /// <summary>
        /// Update a single event by id.
        /// </summary>
        /// <param name="eventId">Id of the event to update</param>
        /// <param name="dtoEvent">Event object to use for update</param>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(typeof(EventReadDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EventReadDTO>> PutEvent(int eventId, EventEditDTO dtoEvent)
        {
            // Map DTO to domain model
            var domainEvent = _mapper.Map<Event>(dtoEvent);
            // Set id of event to update to id in route
            domainEvent.Id = eventId;

            // Check if event can be found
            if (!_eventRepository.EventExists(eventId))
            {
                // Return 404 if not found
                return NotFound("Event not found.");
            }

            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                
                // Check if user is the owner of the event.
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    // Return 403 if not
                    return Forbid("You are not the owner of this event.");
                }
                // Check if user is a member of any target audiences.
                if (!_eventRepository.CheckUserIsMember(requestingUser.Id, domainEvent))
                {
                    // Return 403 if not
                    return Forbid("You are not a member of the target audience.");
                }
                // Update the event in the repository
                var updatedEvent = await _eventRepository.UpdateEventAsync(eventId, domainEvent);
            }
            catch
            {
                return Problem("Something went wrong while updating event.");
            }
            // Return 201 if the event was updated
            return CreatedAtAction("GetEvent", new { id = domainEvent.Id }, dtoEvent);
        }

        // POST: api/event/:event_id/invite/group/:group_id
        /// <summary>
        /// Create a group invitation for an event.
        /// </summary>
        /// <param name="eventId">Id of the event to create invitation for.</param>
        /// <param name="groupId">Id of the group to invite to the event.</param>
        [HttpPost("{eventId}/invite/group/{groupId}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> InviteGroupToEvent(int eventId, int groupId)
        {
            try
            {
                //Check if event can be found
                if (!_eventRepository.EventExists(eventId))
                {
                    return NotFound("Event not found.");
                }
                if (!_groupRepository.GroupExists(groupId))
                {
                    return NotFound("Group not found.");
                }
                
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                
                // Check if user is owner of the group
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    // Return 403 if not
                    return Forbid("You are not the owner of this event.");
                }
                // Create the invitation
                await _eventRepository.CreateEventGroupInviteAsync(eventId, groupId);
                // Return 200 if the invitation was created
                return Ok("Group invitation created.");
                
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while creating group invitation.");
            }
        }

        // DELETE: api/event/:event_id/invite/group/:group_id
        /// <summary>
        /// Delete a group invitation for an event
        /// </summary>
        /// <param name="eventId">Id of the event to delete invitiation for.</param>
        /// <param name="groupId">Id of the group  to delete invitation for.</param>
        [HttpDelete("{eventId}/invite/group/{groupId}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteGroupInviteToEvent(int eventId, int groupId)
        {
            try
            {
                //Check if the event exists
                if (!_eventRepository.EventExists(eventId))
                {
                    // Return 404 if not
                    return NotFound("Event not found.");
                }
                //Check if the group exist
                if (!_groupRepository.GroupExists(groupId))
                {
                    // Return 404 if not
                    return NotFound("Group not found.");
                }

                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Check if user is owner of the group
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    // Return 403 if not
                    return Forbid("You are not the owner of this event.");
                }
                // Delete the invitation
                await _eventRepository.DeleteGroupEventInviteAsync(eventId, groupId);
                // Return 200 if the invitation was deleted
                return Ok("Group invitation deleted.");
            }
            catch
            {
                return Problem("Something went wrong while deleting group invitation.");
            }
        }

        // POST: api/event/:event_id/invite/user/:user_id
        /// <summary>
        /// Create a user invitation for an event
        /// </summary>
        /// <param name="eventId">Id of the event to create invitation for.</param>
        /// <param name="userId">Id of the user to invite to the event.</param>
        [HttpPost("{eventId}/invite/user/{userId}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> InviteUserToEvent(int eventId, int userId)
        {
            try
            {
                // Check if event can be found
                if (!_eventRepository.EventExists(eventId))
                {
                    // Return 404 if not found
                    return NotFound("Event not found.");
                }

                // Check if user can be found
                if (!_userRepository.UserExists(userId))
                {
                    // Return 404 if not found
                    return NotFound("User not found.");
                }

                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Check if user is owner of the group
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    // Return 403 if not
                    return Forbid("You are not the owner of this event.");
                }
                
                // Create the event user invite
                await _eventRepository.CreateEventUserInviteAsync(eventId, userId);
                // Return 200 if successful
                return Ok("User invitation created.");
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while creating user invitation.");
            }
        }

        // DELETE: api/event/:event_id/invite/user/:user_id
        /// <summary>
        /// Delete a user invitation for an event
        /// </summary>
        /// <param name="eventId">Id of the event to delete invitation for.</param>
        /// <param name="userId">Id of the user to delete invitation for.</param>
        [HttpDelete("{eventId}/invite/user/{userId}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteUserInviteToEvent(int eventId, int userId)
        {
            try
            {
                //Check if event can be found
                if (!_eventRepository.EventExists(eventId))
                {
                    // Return 404 if not found
                    return NotFound("Event not found.");
                }

                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Check if user is owner of the group
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    return Forbid("You are not the owner of this event.");
                }
                // Delete the event user invite
                await _eventRepository.DeleteUserEventInviteAsync(eventId, userId);
                // Return 200 if successful
                return Ok("User invitation deleted.");
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while deleting user invitation.");
            }
        }

        // POST: api/event/:event_id/invite/topic/:topic_id
        /// <summary>
        /// Create a topic invitation for an event
        /// </summary>
        /// <param name="eventId">Id of the event to create invitation for.</param>
        /// <param name="topicId">Id of the topic to invite to the event.</param>
        [HttpPost("{eventId}/invite/topic/{topicId}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> InviteTopicToEvent(int eventId, int topicId)
        {
            try
            {
                // Check if event can be found
                if (!_eventRepository.EventExists(eventId))
                {
                    // Return 404 if not found
                    return NotFound("Event not found.");
                }

                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Check if user is owner of the group
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    // Return 403 if not
                    return Forbid("You are not the owner of this event.");
                }
                // Create the event topic invite
                await _eventRepository.CreateEventTopicInviteAsync(eventId, topicId);
                // Return 200 if successful
                return Ok("Topic invitation created.");
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while creating topic invitation.");
            }
        }

        // DELETE: api/event/:event_id/invite/topic/:topic_id
        /// <summary>
        /// Delete a topic invitation for an event
        /// </summary>
        /// <param name="eventId">Id of the event to delete invitation for.</param>
        /// <param name="topicId">Id of the topic to delete invitation for.</param>
        [HttpDelete("{eventId}/invite/topic/{topicId}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteTopicInviteToEvent(int eventId, int topicId)
        {
            try
            {
                // Check if event can be found
                if (!_eventRepository.EventExists(eventId))
                {
                    // Return 404 if not found
                    return NotFound("Event not found.");
                }

                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Check if user is owner of the group
                if (!_eventRepository.CheckUserIsOwner(requestingUser.Id, eventId))
                {
                    // Return 403 if not
                    return Forbid("You are not the owner of this event.");
                }
                // Delete the event topic invite
                await _eventRepository.DeleteTopicEventInviteAsync(eventId, topicId);
                // Return 200 if successful
                return Ok("Topic invitation deleted.");
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while deleting topic invitation.");
            }
        }

        /// <summary>
        /// Get all events user has RSVP for
        /// </summary>
        /// <returns></returns>
        // GET: api/event/user
        [HttpGet("user")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<List<EventReadDTO>>> GetEventsForUser()
        {
            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Get events user has RSVP for
                var events = await _eventRepository.GetEventsForUserAsync(requestingUser.Id);

                var dtoEvents = _mapper.Map<List<EventReadDTO>>(events);
                
                // add attending users to each event
                foreach (var dtoEvent in dtoEvents)
                {
                    dtoEvent.AttendingUsers = await _eventRepository.GetAttendingUsersAsync(dtoEvent.Id);
                }
                
                // Return the events
                return Ok(dtoEvents);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while getting events for user.");
            }
        }

        /// <summary>
        /// Get all events for a group
        /// </summary>
        /// <returns></returns>
        // GET: api/event/group
        [HttpGet("group/{id}")]
        [Authorize]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<List<EventReadDTO>>> GetEventsForGroup(int id)
        {
            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Check if the user is a member of the passed group
                if (_groupRepository.CheckUserIsMember(requestingUser.Id, id))
                {
                    // Get the Events in the group
                    var events = await _eventRepository.GetEventsbyGroupAsync(id);

                    if(events == null)
                    {
                        return NotFound("No Group Events Found!");
                    }

                    // Return the events
                    return Ok(_mapper.Map<List<EventReadDTO>>(events));
                }
                else
                {
                    // Return 403 Forbidden if the user is not part of the group
                    return StatusCode(StatusCodes.Status403Forbidden, "Forbidden, you are not a member of this group");
                }
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while getting events for user.");
            }
            
        }



        // POST: api/event/:event_id/rsvp
        /// <summary>
        /// Create a new RSVP event record.
        /// </summary>
        /// <param name="eventId">Id of the event to create RSVP record for.</param>
        [HttpPost("{eventId}/rsvp")]
        [Authorize]
        [ProducesResponseType(typeof(CreatedResult), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<RSVPReadDTO>> RSVPToEvent(int eventId)
        {
            // Get user from token
            var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

            // Domain model
            var rsvp = new RSVP()
            {
                EventId = eventId,
                UserId = requestingUser.Id
            };

            // Check if an RSVP with the same user and event already exists
            if (_eventRepository.RSVPExists(rsvp.EventId, rsvp.UserId))
            {
                // Return 403 if it does
                return Forbid("You have already RSVP'd to this event.");
            }

            try
            {
                // Get event by id
                var eventToRSVPTo = await _eventRepository.GetEventByIdAsync(eventId);

                // Check if event can be found
                if (eventToRSVPTo == null)
                {
                    // Return 404 if not found
                    return NotFound("Event not found.");
                }

                // Check if user is allowed to view the event
                if (!_eventRepository.CheckUserIsInvited(requestingUser.Id, eventId) && !_eventRepository.CheckUserIsMember(requestingUser.Id, eventToRSVPTo))
                {
                    // Return 403 if user is not allowed to view the event
                    return Unauthorized("You are not a member of any target audiences or invited to this event.");
                }

                // Create RSVP record
                var domainRSVP = await _eventRepository.CreateEventRSVPAsync(eventId, rsvp);
                // Map to DTO and return as 201
                var dtoRSVPRead = _mapper.Map<RSVPReadDTO>(domainRSVP);
                return CreatedAtAction(nameof(RSVPToEvent), new { eventId = eventId, id = domainRSVP.Id }, dtoRSVPRead);

            }
            catch
            {
                // Return 500 if error
                return Problem("Something went wrong while RSVPing to event.");
            }
        }

        // DELETE: api/event/:event_id/rsvp

        /// <summary>
        /// Delete an RSVP event record.
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        [HttpDelete("{eventId}/rsvp")]
        [Authorize]
        public async Task<ActionResult> DeleteRSVP(int eventId)
        {
            // Get user from token
            var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

            // Check if RSVP exists
            if (!_eventRepository.RSVPExists(eventId, requestingUser.Id))
            {
                // Return 404 if not
                return NotFound("RSVP not found.");
            }

            try
            {
                // Delete RSVP
                await _eventRepository.DeleteEventRSVPAsync(eventId, requestingUser.Id);
                // Return 200 if successful
                return Ok("RSVP deleted.");
            }
            catch
            {
                // Return 500 if error
                return Problem("Something went wrong while deleting RSVP.");
            }
        }
    }
}
