﻿using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Topic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Runtime.InteropServices;

namespace AlumniNetwork.Controllers
{
    /// <summary>
    /// Controller for handlings requests related to topics.
    /// </summary>
    [Route("api/topic")]
    [ApiController]
    //[Produces(MediaTypeNames.Application.Json)]
    //[Consumes(MediaTypeNames.Application.Json)]
    //[ApiConventionType(typeof(DefaultApiConventions))]
    public class TopicsController : ControllerBase
    {
        // Dependency injection of repository and automapper:
        private readonly ITopicRepository _topicRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Topics controller constructor.
        /// </summary>
        /// <param name="topicRepository"></param>
        public TopicsController(IMapper mapper, ITopicRepository topicRepository, IUserRepository userRepository)
        {
            _topicRepository = topicRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        // GET: api/topic
        /// <summary>
        /// Get all topics.
        /// </summary>
        /// <param name="searchQueryString">Search query string used to filter events with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop paginiation at.</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(TopicReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetTopics( [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get all topics from repository
                var domainTopics = await _topicRepository.GetTopicsAsync(searchQueryString, (int)offset, (int)limit);
                // Map domain topics to DTO topics
                var dtoTopics = _mapper.Map<IEnumerable<TopicReadDTO>>(domainTopics);
                // Return DTO topics
                return Ok(dtoTopics);
            }
            catch
            {
                // Return 500 Internal Server Error
                return Problem("Something went wrong while trying to get topics.");
            }
        }
        // GET api/topic/:topic_id
        /// <summary>
        /// Fetch a single topic by id.
        /// </summary>
        /// <param name="topicId">Id of topic to fetch</param>
        [HttpGet("{topicId}")]
        [ProducesResponseType(typeof(TopicReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TopicReadDTO>> GetTopic(int topicId)
        {
            try
            {
                // Get topic from repository
                var domainTopic = await _topicRepository.GetTopicByIdAsync(topicId);
                // If topic is null, return 404 Not Found
                if (domainTopic == null)
                {
                    // Return 404 Not Found
                    return NotFound("Topic not found.");
                }
                // Map domain topic to DTO topic
                var dtoTopic = _mapper.Map<TopicReadDTO>(domainTopic);
                // Return DTO topic
                return Ok(dtoTopic);
            }
            catch
            {
                // Return 500 Internal Server Error
                return Problem("Something went wrong while trying to get topic.");
            }
        }

        // POST: api/topic
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a new topic.
        /// </summary>
        /// <param name="dtoTopic">Topic object parameters from body</param>
        [HttpPost]
        [ProducesResponseType(typeof(TopicReadDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TopicReadDTO>> PostTopic(TopicCreateDTO dtoTopic)
        {
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Map dto to domain model
                var domainTopic = _mapper.Map<Topic>(dtoTopic);
                // Add topic to database
                await _topicRepository.CreateTopicAsync(requestingUser.Id, domainTopic);
                // Return created topic as dto
                return CreatedAtAction(nameof(GetTopic), new { topicId = domainTopic.Id }, _mapper.Map<TopicReadDTO>(domainTopic));
            }
            catch
            {
                // Return 500 Internal Server Error
                return Problem("Something went wrong while trying to create topic.");
            }
        }

        // POST: api/topic/:topic_id/join
        /// <summary>
        /// Create a new topic membership record
        /// </summary>
        /// <param name="topicId">Id of topic to create membership record for.</param>
        [HttpPost("{topicId}/join")]
        [ProducesResponseType(typeof(ObjectResult), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> PostTopicMembership(int topicId)
        {
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Add topic membership to database
                return await _topicRepository.CreateTopicMembershipRecord(requestingUser.Id, topicId);
            }
            catch
            {
                // Return 500 Internal Server Error
                return Problem("Something went wrong while trying to create topic membership record.");
            }
        }
    }
}
