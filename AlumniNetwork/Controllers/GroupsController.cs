﻿using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Group;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Runtime.InteropServices;

namespace AlumniNetwork.Controllers
{
    /// <summary>
    /// Controller for handling requests related to groups.
    /// </summary>
    [Route("api/group")]
    [ApiController]
    //[Produces(MediaTypeNames.Application.Json)]
    //[Consumes(MediaTypeNames.Application.Json)]
    //[ApiConventionType(typeof(DefaultApiConventions))]
    public class GroupsController : ControllerBase
    {
        // Dependency injection of repository and automapper:
        private readonly IGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Groups controller constructor.
        /// </summary>
        /// <param name="groupRepository"></param>
        public GroupsController(IMapper mapper, IGroupRepository groupRepository, IUserRepository userRepository)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        // GET: api/group
        /// <summary>
        /// Fetches a list of all groups accessable to the requesting user.
        /// </summary>
        /// <param name="searchQueryString">Search query string used to filter events with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop paginiation at.</param>
        [HttpGet]
        [ProducesResponseType(typeof(GroupReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetGroups( [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);    
                // Get groups from repository
                var domainGroups = await _groupRepository.GetGroupsAsync(requestingUser.Id, searchQueryString, (int)offset, (int)limit);
                // Map domain groups to DTO
                var dtoGroups = _mapper.Map<IEnumerable<GroupReadDTO>>(domainGroups);
                // Return dtoGroups as ok
                return Ok(dtoGroups);
            }
            catch
            {
                return Problem("Something went wrong while fetching groups from the database.");
            }
        }

        // GET: api/group/:group_id
        /// <summary>
        /// Fetches the group corresponding to the provided group id
        /// </summary>
        /// <param name="groupId">The provided group id used to locate and fetch group.</param>
        [HttpGet("{groupId}")]
        [ProducesResponseType(typeof(GroupReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<GroupReadDTO>> GetGroup(int groupId)
        {
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Get group from repository
                var domainGroup = await _groupRepository.GetGroupByIdAsync(groupId);

                // Check if group exists
                if (domainGroup == null)
                {
                    // Return not found
                    return NotFound("Group not found.");
                }

                if (domainGroup.IsPrivate && !_groupRepository.CheckUserIsMember(requestingUser.Id, groupId))
                {
                    // Return forbidden
                    return Forbid("User is not part of private group.");
                }

                // Map domain group to DTO
                var dtoGroup = _mapper.Map<GroupReadDTO>(domainGroup);
                // Return dtoGroup as ok
                return Ok(dtoGroup);
            }
            catch
            {
                // Return internal server error
                return Problem("Something went wrong while fetching group from the database.");
            }
        }

        // POST: api/group
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new group, simultaneously creating a new group membership record for requesting user
        /// </summary>
        /// <param name="dtoGroup">Group object parameters obtained from body.</param>
        [HttpPost]
        [ProducesResponseType(typeof(GroupReadDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<GroupReadDTO>> PostGroup(GroupCreateDTO dtoGroup)
        {
            // Get user from token
            var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
            // Map dtoGroup to domainGroup
            var domainGroup = _mapper.Map<Group>(dtoGroup);
            try
            {
                // Add group to repository
                var newGroup = await _groupRepository.CreateGroupAsync(requestingUser.Id, domainGroup);
            }
            catch
            {
                return Problem("Something went wrong while creating group in the database.");
            }

            // Return created group
            return CreatedAtAction("GetGroup", new { groupId = domainGroup.Id }, _mapper.Map<GroupReadDTO>(domainGroup));
        }
        // POST: api/group/:group_id/join
        /// <summary>
        /// Create a new group membership record.
        /// </summary>
        /// <param name="userId">Optional user id to create group membership record for (default id is requesting user).</param>
        /// <param name="groupId">Group id to create group membership record for.</param>
        [HttpPost("{groupId}/join")]
        [ProducesResponseType(typeof(GroupReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<GroupReadDTO>> PostGroupMember([FromHeader] int? userId, int groupId)
        {
            // get user from token
            var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
            
            // If there is no userId passed along as parameter, use id of the requesting user.
            userId??= requestingUser.Id;

            try
            {
                // Get group from repository
                var domainGroup = await _groupRepository.GetGroupByIdAsync(groupId);
                // Check if group exists
                if (domainGroup == null)
                {
                    // Return not found
                    return NotFound("Group not found.");
                }
                // If the group is private and requesting user is not a member, return 403 forbidden response.
                if (domainGroup.IsPrivate && !_groupRepository.CheckUserIsMember(requestingUser.Id, groupId))
                {
                    return Forbid("User is not part of private group; and can't join or add other members.");
                }
                // Add group membership to repository
                var membershipRecord = await _groupRepository.AddGroupMemberAsync((int)userId, groupId);
            }
            catch
            {
                // Return internal server error
                return Problem("Something went wrong while creating group membership in the database.");
            }
            // Return ok with updated group as DTO
            return Ok(_mapper.Map<GroupReadDTO>(await _groupRepository.GetGroupByIdAsync(groupId)));

        }

        [HttpDelete("{groupId}/leave")]
        public async Task<ActionResult<GroupReadDTO>> DeleteGroupMember([FromHeader] int? userId, int groupId)
        {
            // get user from token
            var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
            
            // If there is no userId passed along as parameter, use id of the requesting user.
            userId??= requestingUser.Id;

            try
            {
                 // Get group from repository
                var domainGroup = await _groupRepository.GetGroupByIdAsync(groupId);
                // Check if group exists
                if (domainGroup == null)
                {
                    // Return not found
                    return NotFound("Group not found.");
                }
                // If the user is not attempting to remove himself or is not an owner, return 403 forbidden response.
                if (requestingUser.Id != userId)
                {
                    if (!domainGroup.Members.FirstOrDefault().Equals(requestingUser.Id))
                    {
                        return Forbid("User is not the owner of the group or is not attempting to remove himself.");   
                    }
                }
                // Add group membership to repository
                var membershipRecord = await _groupRepository.RemoveGroupMemberAsync((int)userId, groupId);
            }
            catch
            {
                // Return internal server error
                return Problem("Something went wrong while creating group membership in the database.");
            }
            // Return ok with updated group as DTO
            return Ok(_mapper.Map<GroupReadDTO>(await _groupRepository.GetGroupByIdAsync(groupId)));
        }

    }
}
