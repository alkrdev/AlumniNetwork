﻿using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.Post;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Net.WebSockets;
using System.Runtime.InteropServices;

namespace AlumniNetwork.Controllers
{   /// <summary>
    /// Controller for handling requests related to posts.
    /// </summary>
    [Route("api/post")]
    [ApiController]
    //[Produces(MediaTypeNames.Application.Json)]
    //[Consumes(MediaTypeNames.Application.Json)]
    //[ApiConventionType(typeof(DefaultApiConventions))]
    public class PostsController : ControllerBase
    {
        // Dependency injection of repository and automapper:
        private readonly IPostRepository _postRepository;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly ITopicRepository _topicRepository;
        private readonly IEventRepository _eventRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Posts controller constructor.
        /// </summary>
        /// <param name="postRepository"></param>
        public PostsController(IMapper mapper, IPostRepository postRepository, IUserRepository userRepository, IGroupRepository groupRepository, ITopicRepository topicRepository, IEventRepository eventRepository)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
            _topicRepository = topicRepository;
            _eventRepository = eventRepository;
            _mapper = mapper;
        }

        // GET: api/post
        /// <summary>
        /// Fetches a list of post, for which the requesting user is subscribed.
        /// </summary>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(Group), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetPosts( [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                var domainPosts = await _postRepository.GetSubsribedPostsAsync(requestingUser.Id, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // GET: api/post/user
        /// <summary>
        /// Fetches a list of post where requesting user was set as a direct recipient
        /// </summary>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet("user")]
        [Authorize]
        [ProducesResponseType(typeof(Group), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetDirectPosts( [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Get posts where user is a direct recipient
                var domainPosts = await _postRepository.GetDirectPostsAsync(requestingUser.Id, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // GET: api/post/user/user_id
        /// <summary>
        /// Fetches a list of post where requesting user was set as a direct recipient
        /// </summary>
        /// <param name="userId">Id of user to fetch direct posts for.</param>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet("user/other/{userId}")]
        [Authorize]
        [ProducesResponseType(typeof(Group), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetDirectPostsId( int userId, [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get posts where user is a direct recipient
                var domainPosts = await _postRepository.GetDirectPostsAsync(userId, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // GET: api/post/:post_id
        /// <summary>
        /// Get a single post by id.
        /// </summary>
        /// <param name="id"></param>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(Post), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PostReadDTO>> GetPost(int id)
        {
            try
            {
                // Get the id of the user from the token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Get the event from the repository
                var domainPost = await _postRepository.GetPostByIdAsync(id);

                // Check if the event was found
                if (domainPost == null)
                {
                    return NotFound($"Post id {id} was not found.");
                }

                // not users own post, check if they should be able to view it.
                switch (domainPost.PostTarget)
                {
                    case Target.User:
                        // users are allowed to view user posts.
                        break;
                    case Target.Group:
                        if(!_groupRepository.CheckUserIsMember(requestingUser.Id,domainPost.TargetGroup.Id))
                        {
                            return Unauthorized("You are not allowed to view this post.");
                        }
                        break;
                    case Target.Topic:
                        if(!_topicRepository.CheckUserIsMember(requestingUser.Id,domainPost.TargetTopic.Id))
                        {
                            return Unauthorized("You are not allowed to view this post.");
                        }
                        break;
                    case Target.Event:
                        if(!_eventRepository.CheckUserIsMember(requestingUser.Id,domainPost.TargetEvent))
                        {
                            return Unauthorized("You are not allowed to view this post.");
                        }
                        break;
                    default:
                        return BadRequest("Invalid PostTarget");
                }

                // Map the domain event to a DTO

                int ReplyParentId = 0;
                if (domainPost.ReplyParent != null)
                {
                    ReplyParentId = domainPost.ReplyParent.Id;
                }

                var dtoPost = _mapper.Map<PostReadDTO>(domainPost);

                // Return the DTO
                return Ok(dtoPost);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while getting event.");
            }
        }

        // GET: api/post/user/:user_id
        /// <summary>
        /// Fetches a list of post where requesting user was set as a direct recipient from a specific sender
        /// </summary>
        /// <param name="userId">Id of the sending user to retrieve posts from.</param>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet("user/{userId}")]
        [Authorize]
        [ProducesResponseType(typeof(Group), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetDirectPostsFromUser( int userId, [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Get posts where user is a direct recipient
                var domainPosts = await _postRepository.GetDirectPostsByUserAsync(requestingUser.Id, userId, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // GET: api/post/user/:user_id
        /// <summary>
        /// Fetches a list of post related to a specific group
        /// </summary>
        /// <param name="groupId">Id of the group to fetch posts for.</param>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet("group/{groupId}")]
        [Authorize]
        [ProducesResponseType(typeof(Group), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetGroupPosts( int groupId, [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get posts from database
                var domainPosts = await _postRepository.GetPostsByGroupAsync(groupId, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // GET: api/post/event/:event_id
        /// <summary>
        /// Fetches a list of post related to a specific event
        /// </summary>
        /// <param name="eventId">Id of the event to fetch posts for.</param>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet("event/{eventId}")]
        [Authorize]
        [ProducesResponseType(typeof(Group), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetEventPosts( int eventId, [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get posts from database
                var domainPosts = await _postRepository.GetPostsByEventAsync(eventId, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // GET: api/post/topic/:topic_id
        /// <summary>
        /// Fetches a list of post related to a specific topic
        /// </summary>
        /// <param name="topicId">Id of the topic to fetch posts for.</param>
        /// <param name="searchQueryString">Search query string used to filter posts with.</param>
        /// <param name="offset">Offset to start pagination at.</param>
        /// <param name="limit">Limit to stop pagination at.</param>
        [HttpGet("topic/{topicId}")]
        [Authorize]
        [ProducesResponseType(typeof(PostReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetTopicPosts( int topicId, [FromHeader] string? searchQueryString, [FromHeader] int? offset, [FromHeader] int? limit)
        {
            // Set null values from header to 0 or empty
            offset ??= 0;
            limit ??= 0;
            searchQueryString??= "";
            try
            {
                // Get posts from database
                var domainPosts = await _postRepository.GetPostsByTopicAsync(topicId, searchQueryString, (int)offset, (int)limit);
                // Map domain posts to DTOs
                var dtoPosts = _mapper.Map<IEnumerable<PostReadDTO>>(domainPosts);
                // Return DTOs
                return Ok(dtoPosts);
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while fetching posts.");
            }
        }

        // POST: api/post
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a new post
        /// </summary>
        /// <param name="dtoPost">Post object parameters from body.</param>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(PostReadDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PostReadDTO>> PostPost(PostCreateDTO dtoPost)
        {
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                // Convert dto to domain model - can't use automapper here since we pass an int to User object.
                var domainPost = new Post()
                {
                    Body = dtoPost.Body,
                    PostTarget = dtoPost.PostTarget,
                    CreatedByUser = requestingUser,
                };

                //Resolve the Post target objects.
                switch (dtoPost.PostTarget)
                {
                    case Target.User:
                        domainPost.TargetUser = await _userRepository.GetUserByIdAsync(dtoPost.TargetUser);
                        if (domainPost.TargetUser == null) return BadRequest($"TargetUser Id {dtoPost.TargetUser} not found.");
                        break;
                    case Target.Group:
                        domainPost.TargetGroup = await _groupRepository.GetGroupByIdAsync(dtoPost.TargetGroup);
                        if (domainPost.TargetGroup == null) return BadRequest($"TargetGroup Id {dtoPost.TargetGroup} not found.");
                        break;
                    case Target.Topic:
                        domainPost.TargetTopic = await _topicRepository.GetTopicByIdAsync(dtoPost.TargetTopic);
                        if (domainPost.TargetTopic == null) return BadRequest($"TargetTopic Id {dtoPost.TargetTopic} not found.");
                        break;
                    case Target.Event:
                        domainPost.TargetEvent = await _eventRepository.GetEventByIdAsync(dtoPost.TargetEvent);
                        if (domainPost.TargetEvent == null) return BadRequest($"TargetEvent Id {dtoPost.TargetEvent} not found.");
                        break;
                    default:
                        return BadRequest("Invalid PostTarget");
                }

                //Resolve ReplyParent if it is set
                if (dtoPost.ReplyParent > 0)
                {
                    var replyParent = await _postRepository.GetPostByIdAsync(dtoPost.ReplyParent);
                    if (replyParent == null) return BadRequest($"ReplyParent Id {dtoPost.ReplyParent} not found.");
                    domainPost.ReplyParent = replyParent;
                }


                // Check if user is allowed to create post to target
                if (_postRepository.AllowedToPostToTarget(requestingUser.Id, domainPost))
                {
                    // Create post in database
                    var createdPost = await _postRepository.CreatePostAsync(requestingUser.Id, domainPost);

                    // Map domain post to DTO
                    var dtoCreatedPost = _mapper.Map<PostReadDTO>(createdPost);

                    // Return DTO
                    return CreatedAtAction("GetPost", new { id = dtoCreatedPost.Id }, dtoCreatedPost);
                }
                else
                {
                    // Return 403 if user is not allowed to create post
                    return Forbid("User is not allowed to create post to target.");
                }
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while creating post.");
            }
        }

        // POST: api/post/:post_id
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update an existing post
        /// </summary>
        /// <param name="postId">Id of the post to update.</param>
        /// <param name="dtoPost">Post object parameters from body.</param>
        [HttpPut("{postId}")]
        [Authorize]
        [ProducesResponseType(typeof(PostReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PostReadDTO>> PutPost(int postId, PostEditDTO dtoPost)
        {
            try
            {
                // Get user from token
                var requestingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);
                // Get the original post
                var originalPost = await _postRepository.GetPostByIdAsync(postId);
                // Convert dto to domain model
                var domainPost = originalPost;
                domainPost.Body = dtoPost.Body;

                // Check if user is owner of original post
                if (originalPost.CreatedByUser.Id == requestingUser.Id)
                {
                    // Check that target event, group, topic, or user has not changed.
                    if (originalPost.TargetEvent != domainPost.TargetEvent || originalPost.TargetGroup != domainPost.TargetGroup || originalPost.TargetTopic != domainPost.TargetTopic || originalPost.TargetUser != domainPost.TargetUser)
                    {
                        // If it has, return forbidden status code.
                        return new ForbidResult("Target event, group, topic, or user cannot be changed.");
                    }
                    
                    // Update post in database
                    var updatedPost = await _postRepository.UpdatePostAsync(requestingUser.Id, postId, domainPost);
                    // Map updated post to DTO
                    var dtoUpdatedPost = _mapper.Map<PostReadDTO>(updatedPost);
                    // Return DTO
                    return Ok(dtoUpdatedPost);
                }
                else
                {
                    // Return 403 if user is not allowed to update post
                    return Forbid("User is not allowed to update post.");
                }         
            }
            catch
            {
                // Return 500 if something went wrong
                return Problem("Something went wrong while updating post.");
            }
        }
    }
}
