﻿using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO.User;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mime;

namespace AlumniNetwork.Controllers
{
    /// <summary>
    /// Controller for handling requests related to users.
    /// </summary>
    [Route("api/user")]
    [ApiController]
    //[Produces(MediaTypeNames.Application.Json)]
    //[Consumes(MediaTypeNames.Application.Json)]
    //[ApiConventionType(typeof(DefaultApiConventions))]
    public class UsersController : ControllerBase
    {
       // Dependency injection of repository and automapper:
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Users controller constructor.
        /// </summary>
        /// <param name="userRepository"></param>
        public UsersController(IMapper mapper, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }


        [HttpGet("GetUserFromJwt")]
        public async Task<ActionResult<User>> GetUserFromJwt(StringValues stringValues)
        {
            // Get the JWT from the header
            var jwtString = stringValues.ToString().Replace("Bearer ", "");
            // Parse the JWT
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwtString);
            // Get the subject from the JWT
            var subject = token.Subject;
            // Get user from subject subject
            return await _userRepository.GetUserFromJWTAsync(subject);
        }

        // GET: api/user
        /// <summary>
        /// See other with the location header of the currently authenticated user's profile
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(Uri), StatusCodes.Status200OK)] // from redirect 303 see other.
        [ProducesResponseType(typeof(Uri), StatusCodes.Status303SeeOther)]
        [ProducesResponseType(typeof(Uri), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> GetUser()
        {
            try
            {
                
                var user = await _userRepository.GetUserAsync(Request.Headers["Authorization"]);
                if(user == null)
                {
                    return NotFound();
                }
                else
                {
                    //return Redirect($"/user/{_userRepository.GetUserAsync(Request.Headers["Authorization"])}");
                    // Returns 303 See Other with the location header set to the URL of the currently authenticated user’s profile;

                    string location = $"user/{user.Id}";
                    this.Response.Headers.Add("Location", location);
                    return new StatusCodeResult(303);

                }
            }
            catch
            {
                // Returns 500 Internal Server Error if something goes wrong;
                return Problem("Something went wrong");
            }
        }

        // GET: api/user/:user_id
        /// <summary>
        /// Fetches a user by id.
        /// </summary>
        /// <param name="userId">Id of user to fetch.</param>
        [HttpGet("{userId}")]
        [ProducesResponseType(typeof(UserReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserReadDTO>> GetUserById(int userId)
        {
            try
            {
                // Get user from repository
                var domainUser = await _userRepository.GetUserByIdAsync(userId);

                // If user is not found, return 404 Not Found
                if (domainUser == null)
                {
                    return NotFound("User not found");
                }
                // Map domain user to DTO
                var dtoUser = _mapper.Map<UserReadDTO>(domainUser);
                // Return 200 OK with user
                return Ok(dtoUser);
            }
            catch
            {
                // Returns 500 Internal Server Error if something goes wrong;
                return Problem("Something went wrong");
            }
        }

        // Patch: api/user/:user_id
        /// <summary>
        /// Updates the user corresponding to the provided user id
        /// </summary>
        /// <param name="userId">Id of the user to update.</param>
        /// <param name="dtoUser">User object parameters from the body</param>
        /// <returns></returns>
        [HttpPatch("{userId}")]
        [ProducesResponseType(typeof(UserReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserReadDTO>> PatchUser(int userId, UserEditDTO dtoUser)
        {
            var requestingUser = await _userRepository.GetUserAsync(Request.Headers["Authorization"]);
            if(requestingUser == null)
            {
                return NotFound("User not found");
            }
            if (userId != requestingUser.Id)
            {
                return BadRequest("You can only edit your own profile");
            }

            // Map dto to domain model
            var domainUser = _mapper.Map<User>(dtoUser);
            domainUser.Subject = requestingUser.Subject;
            
            // Set id of user to route parameter
            domainUser.Id = userId;
            try
            {
                // Update user in repository
                await _userRepository.UpdateUserByIdAsync(domainUser);
                // Return updated user as DTO
                return Ok(_mapper.Map<UserReadDTO>(domainUser));
            }
            catch
            {
                // Returns 500 Internal Server Error if something goes wrong;
                return Problem("Something went wrong");
            }
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(UserReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<User>> PostUser()
        {
            try
            {
                // First check if the user already exist.
                var existingUser = await _userRepository.GetUserFromJWTAsync(Request.Headers["Authorization"]);

                if (existingUser == null)
                {
                    // Create the new user
                    var jwtString = Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
                    var handler = new JwtSecurityTokenHandler();
                    var token = handler.ReadJwtToken(jwtString);
                    var subject = token.Subject;
                    var name = token.Claims.First(c => c.Type == "name").Value;

                    var newUser = new User()
                    { 
                        Name = name, 
                        Subject = subject 
                    };

                    return await _userRepository.CreateUserAsync(newUser);
                }
                else
                {
                    // User already, do nothing.
                    return BadRequest("User already exist");
                }
            }
            catch (Exception)
            {
                return Problem();
            }
        }

        /// <summary>
        /// Get the current amount of users in the database
        /// </summary>
        [HttpGet("count")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<int>> GetUserCount()
        {
            try
            {
                return  Ok(_userRepository.GetUserCountAsync());
            }
            catch (Exception)
            {
                return Problem();
            }
        }
    }
}
