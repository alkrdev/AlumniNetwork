﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetwork.Migrations
{
    public partial class initialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Body = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    BannerImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    GroupImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsPrivate = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Topics",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subject = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ProfileImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    FunFact = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventGroupInvite",
                columns: table => new
                {
                    EventsId = table.Column<int>(type: "int", nullable: false),
                    GroupsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventGroupInvite", x => new { x.EventsId, x.GroupsId });
                    table.ForeignKey(
                        name: "FK_EventGroupInvite_Events_EventsId",
                        column: x => x.EventsId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventGroupInvite_Groups_GroupsId",
                        column: x => x.GroupsId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventTopicInvite",
                columns: table => new
                {
                    EventsId = table.Column<int>(type: "int", nullable: false),
                    TopicsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventTopicInvite", x => new { x.EventsId, x.TopicsId });
                    table.ForeignKey(
                        name: "FK_EventTopicInvite_Events_EventsId",
                        column: x => x.EventsId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventTopicInvite_Topics_TopicsId",
                        column: x => x.TopicsId,
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventUserInvite",
                columns: table => new
                {
                    EventsId = table.Column<int>(type: "int", nullable: false),
                    UsersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventUserInvite", x => new { x.EventsId, x.UsersId });
                    table.ForeignKey(
                        name: "FK_EventUserInvite_Events_EventsId",
                        column: x => x.EventsId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventUserInvite_Users_UsersId",
                        column: x => x.UsersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupMember",
                columns: table => new
                {
                    GroupsId = table.Column<int>(type: "int", nullable: false),
                    MembersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMember", x => new { x.GroupsId, x.MembersId });
                    table.ForeignKey(
                        name: "FK_GroupMember_Groups_GroupsId",
                        column: x => x.GroupsId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupMember_Users_MembersId",
                        column: x => x.MembersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Body = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    PostTarget = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedByUserId = table.Column<int>(type: "int", nullable: true),
                    TargetUserId = table.Column<int>(type: "int", nullable: true),
                    TargetEventId = table.Column<int>(type: "int", nullable: true),
                    TargetGroupId = table.Column<int>(type: "int", nullable: true),
                    TargetTopicId = table.Column<int>(type: "int", nullable: true),
                    ReplyParentId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Events_TargetEventId",
                        column: x => x.TargetEventId,
                        principalTable: "Events",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Groups_TargetGroupId",
                        column: x => x.TargetGroupId,
                        principalTable: "Groups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Posts_ReplyParentId",
                        column: x => x.ReplyParentId,
                        principalTable: "Posts",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Topics_TargetTopicId",
                        column: x => x.TargetTopicId,
                        principalTable: "Topics",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Users_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Users_TargetUserId",
                        column: x => x.TargetUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "RSVPs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GuestCount = table.Column<int>(type: "int", nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RSVPs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RSVPs_Events_EventId",
                        column: x => x.EventId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RSVPs_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TopicMember",
                columns: table => new
                {
                    MembersId = table.Column<int>(type: "int", nullable: false),
                    TopicsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicMember", x => new { x.MembersId, x.TopicsId });
                    table.ForeignKey(
                        name: "FK_TopicMember_Topics_TopicsId",
                        column: x => x.TopicsId,
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicMember_Users_MembersId",
                        column: x => x.MembersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Events",
                columns: new[] { "Id", "BannerImage", "Body", "Created", "EndTime", "LastUpdated", "StartTime" },
                values: new object[,]
                {
                    { 1, null, "Beer brewing contest, who can make the best Stout or IPA?", new DateTime(2022, 10, 21, 8, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 5, 19, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 10, 21, 9, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 5, 11, 30, 0, 0, DateTimeKind.Utc) },
                    { 2, null, "Learn how to make beer at home. Simply prepare, brew, ferment, bottle, and enjoy!", new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 1, 15, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 1, 9, 0, 0, 0, DateTimeKind.Utc) },
                    { 3, null, "Meet up at Central Perk coffee shop", new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 1, 15, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 1, 9, 0, 0, 0, DateTimeKind.Utc) },
                    { 4, null, "Joeys Birthday party!", new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 4, 21, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 4, 17, 0, 0, 0, DateTimeKind.Utc) },
                    { 5, null, "Preparing cake for Joeys Birthday party!", new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 3, 15, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 10, 22, 7, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 11, 3, 12, 0, 0, 0, DateTimeKind.Utc) }
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Description", "GroupImage", "IsPrivate", "Name" },
                values: new object[,]
                {
                    { 1, "A group for everyone who loves beer", null, false, "Beer Brewing" },
                    { 2, "A group focused on studying Mathematics", null, false, "Math Study Group" },
                    { 3, "A group for Car Enthusiast", null, false, "Car Enthusiasts" },
                    { 4, "A Private group that requires an invitation", null, true, "The Private Group" },
                    { 5, "A group for all the F.R.I.E.N.D.S", null, false, "F.R.I.E.N.D.S" }
                });

            migrationBuilder.InsertData(
                table: "Topics",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "All things to do with Beer", "Beer" },
                    { 2, "Great drinks and booze talk", "Drinks and booze" },
                    { 3, "All things to do with Math", "Math" },
                    { 4, "All things to do with Cars", "Cars" },
                    { 5, "Lets talk about the best Coffee and delicious food.", "Cafés and Restaurants" },
                    { 6, "Socal Activities and meetups.", "Socal Activities" },
                    { 7, "We love cake", "Cake" },
                    { 8, "Time to celebrate another year!", "Birthdays" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "FunFact", "Name", "ProfileImage", "Status", "Subject" },
                values: new object[,]
                {
                    { 1, "Does not share food!", "Joey Tribbiani", "https://i.imgur.com/Hd8FGCy.jpeg", "Is eating a whole turkey", "ebdcca85-9ece-4e10-90a5-584fc09449ec" },
                    { 2, "Has been married 3 times", "Ross Geller", "https://i.imgur.com/F7WpuUd.jpeg", "Teaching a paleontology class", "46d8795e-9f5a-4877-b97b-a884bdcb4597" },
                    { 3, "Is ultra-competitive", "Monica Geller", "https://i.imgur.com/9UH4g7K.jpeg", "Cleaning my appartment", "e7c80f0c-b535-4774-b399-c9f947be4bc8" },
                    { 4, "I make jokes when i'm uncomfortable", "Chandler Bing", "https://i.imgur.com/WQizcSs.jpeg", "Looking for my Duck, have you seen it?", "2096b784-b62f-4ec5-b40e-86cbdd72c883" },
                    { 5, "Is afraid of fish", "Rachel Green", "https://i.imgur.com/AJPYRF6.jpeg", "Working at the Central Perk coffee shop", "b842ea46-b650-4939-bb37-29fc34c6a67d" },
                    { 6, "Was once Homeless", "Phoebe Buffay", "https://i.imgur.com/ChIyiSH.jpeg", "Playing my guitar in the subway", "42bedda6-836e-4201-90a7-88a4efc4f332" }
                });

            migrationBuilder.InsertData(
                table: "EventGroupInvite",
                columns: new[] { "EventsId", "GroupsId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 5 },
                    { 4, 5 },
                    { 5, 5 }
                });

            migrationBuilder.InsertData(
                table: "EventTopicInvite",
                columns: new[] { "EventsId", "TopicsId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 6 },
                    { 2, 1 },
                    { 2, 2 },
                    { 2, 6 },
                    { 4, 8 },
                    { 5, 7 }
                });

            migrationBuilder.InsertData(
                table: "EventUserInvite",
                columns: new[] { "EventsId", "UsersId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 4 },
                    { 2, 1 },
                    { 2, 2 },
                    { 2, 4 }
                });

            migrationBuilder.InsertData(
                table: "GroupMember",
                columns: new[] { "GroupsId", "MembersId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 2, 3 },
                    { 3, 1 },
                    { 3, 4 },
                    { 4, 3 },
                    { 4, 4 },
                    { 5, 1 },
                    { 5, 2 },
                    { 5, 3 },
                    { 5, 4 },
                    { 5, 5 },
                    { 5, 6 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "Body", "Created", "CreatedByUserId", "LastUpdated", "PostTarget", "ReplyParentId", "TargetEventId", "TargetGroupId", "TargetTopicId", "TargetUserId" },
                values: new object[,]
                {
                    { 1, "This is the first post, targeted a user", new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 2, "This is the Second post, targeted a group", new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 2, null, null },
                    { 3, "This is the Fourth post, targeted a topic ", new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 2, null, null, null, 1, null },
                    { 4, "This is the Third post, targeted a event", new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 1, 10, 0, 0, 0, DateTimeKind.Utc), 3, null, 1, null, null, null },
                    { 5, "Im hungover, great party last night!", new DateTime(2022, 10, 2, 4, 0, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 2, 4, 0, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 6, "Hey Ross, I ate your leftover sandwitch, hope you don't mind? It was really good.", new DateTime(2022, 10, 2, 4, 1, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 2, 4, 1, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 2 },
                    { 7, "Hi Monica, How You Doin'?", new DateTime(2022, 10, 2, 4, 2, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 2, 4, 2, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 3 },
                    { 8, "Hey Chandler bing bong, have you seen our duck? I think he ran out the hallway.", new DateTime(2022, 10, 2, 4, 3, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 2, 4, 3, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 4 },
                    { 9, "Hi Rachel, How You Doin'?", new DateTime(2022, 10, 2, 4, 4, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 2, 4, 4, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 5 },
                    { 10, "Hey Phoebe, How You Doin'?", new DateTime(2022, 10, 2, 4, 5, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 2, 4, 5, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 6 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "Body", "Created", "CreatedByUserId", "LastUpdated", "PostTarget", "ReplyParentId", "TargetEventId", "TargetGroupId", "TargetTopicId", "TargetUserId" },
                values: new object[,]
                {
                    { 11, "You ate my sandwitch? MYYY SANDWITCH!!!", new DateTime(2022, 10, 3, 4, 1, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 3, 4, 1, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 12, "Just finished my Lecture for the day! Going to the Central Perk coffee shop", new DateTime(2022, 10, 3, 4, 2, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 3, 4, 2, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 2 },
                    { 13, "Hi sister, meet me at the coffee shop", new DateTime(2022, 10, 3, 4, 2, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 3, 4, 2, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 3 },
                    { 14, "Yo Chandler, still working on your W.E.N.U.S?", new DateTime(2022, 10, 3, 4, 3, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 3, 4, 3, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 4 },
                    { 15, "Rachel call me back!", new DateTime(2022, 10, 3, 4, 4, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 3, 4, 4, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 5 },
                    { 16, "Hey have you seen Joey today?", new DateTime(2022, 10, 3, 4, 5, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 3, 4, 5, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 6 },
                    { 17, "Joey did you eat my homemade cookies?", new DateTime(2022, 10, 4, 20, 0, 0, 0, DateTimeKind.Utc), 3, new DateTime(2022, 10, 4, 20, 0, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 18, "Ross you are late for our family dinner!", new DateTime(2022, 10, 4, 20, 1, 0, 0, DateTimeKind.Utc), 3, new DateTime(2022, 10, 4, 20, 1, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 2 },
                    { 19, "Note to self. don't leave cookies where Joey can get them", new DateTime(2022, 10, 4, 20, 2, 0, 0, DateTimeKind.Utc), 3, new DateTime(2022, 10, 4, 20, 2, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 3 },
                    { 20, "Chandler you better not start making jokes when, we get to my parrent house!", new DateTime(2022, 10, 4, 20, 3, 0, 0, DateTimeKind.Utc), 3, new DateTime(2022, 10, 4, 20, 3, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 4 },
                    { 21, "Hey Rachel can I borrow a dress? I need it for that Birthday party.", new DateTime(2022, 10, 4, 20, 4, 0, 0, DateTimeKind.Utc), 3, new DateTime(2022, 10, 4, 20, 4, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 5 },
                    { 22, "Hey Phoebe what are you up to?", new DateTime(2022, 10, 4, 20, 5, 0, 0, DateTimeKind.Utc), 3, new DateTime(2022, 10, 4, 20, 5, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 6 },
                    { 23, "Hey Joey, no I haven't seen Duck Jr.?", new DateTime(2022, 10, 4, 20, 6, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 4, 20, 6, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 24, "Yeah Still working, boss needs to give me a raise!", new DateTime(2022, 10, 4, 20, 7, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 4, 20, 7, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 2 },
                    { 25, "Yeah, I am going to be a late for dinner. Could I BE' working any more?", new DateTime(2022, 10, 4, 20, 8, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 4, 20, 8, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 3 },
                    { 26, "Ohh Jolly time, I am stuck at work. The W.E.N.U.S is not working!", new DateTime(2022, 10, 4, 20, 9, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 4, 20, 9, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 4 },
                    { 27, "Hey Rachel, wanna hear a joke? Knock knock..", new DateTime(2022, 10, 4, 20, 10, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 4, 20, 10, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 5 },
                    { 28, "Phoebe, why did the chicken cross the road?", new DateTime(2022, 10, 4, 20, 11, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 4, 20, 11, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 6 },
                    { 29, "Joey can you help me hang a few lamps?", new DateTime(2022, 10, 15, 13, 0, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 15, 13, 0, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 30, "Ross I tried to call you, but your phone is going to straight voice!", new DateTime(2022, 10, 15, 13, 1, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 15, 13, 1, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 2 },
                    { 31, "I am going shopping tomorrow, wanna go?", new DateTime(2022, 10, 15, 13, 2, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 15, 13, 2, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 3 },
                    { 32, "Chandler you must help me plan a surprise party for Phoebe!", new DateTime(2022, 10, 15, 13, 3, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 15, 13, 3, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 4 },
                    { 33, "How does this work?", new DateTime(2022, 10, 15, 13, 4, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 15, 13, 4, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 5 },
                    { 34, "Phoebe are you still dating that Astronaut?", new DateTime(2022, 10, 15, 13, 5, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 15, 13, 5, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 6 },
                    { 35, "Have your keys!", new DateTime(2022, 10, 15, 13, 6, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 15, 13, 6, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 1 },
                    { 36, "I need to borrow money for a new Guitar!", new DateTime(2022, 10, 15, 13, 7, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 15, 13, 7, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 2 },
                    { 37, "Monica what was the name of that movie again?", new DateTime(2022, 10, 15, 13, 8, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 15, 13, 8, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 3 },
                    { 38, "I hope you like my new song! Smelly cat!", new DateTime(2022, 10, 15, 13, 9, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 15, 13, 9, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 4 },
                    { 39, "I have a interesting story for you, see you in 10!", new DateTime(2022, 10, 15, 13, 10, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 15, 13, 10, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 5 },
                    { 40, "Smelly cat smelly cat what are they feeding you!", new DateTime(2022, 10, 15, 13, 11, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 15, 13, 11, 0, 0, DateTimeKind.Utc), 0, null, null, null, null, 6 },
                    { 41, "This new Stout called DarkMatter is awesome!", new DateTime(2022, 10, 11, 22, 0, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 11, 22, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 1, null, null },
                    { 42, "I Really like a Royal brew!", new DateTime(2022, 10, 11, 23, 0, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 11, 23, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 1, null, null },
                    { 43, "There is a discount on Budlight at Costco!", new DateTime(2022, 10, 12, 0, 0, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 12, 0, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 1, null, null },
                    { 44, "Hey friends, have anyone seen my keys?, I can't find them", new DateTime(2022, 10, 12, 1, 0, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 10, 12, 1, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 5, null, null },
                    { 45, "Anyone wanna go grab a Sandwitch?", new DateTime(2022, 10, 12, 2, 0, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 12, 2, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 5, null, null },
                    { 46, "Who's ready to paaarty, tonight?", new DateTime(2022, 10, 12, 3, 0, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 12, 3, 0, 0, 0, DateTimeKind.Utc), 1, null, null, 5, null, null },
                    { 47, "Who Buys a gift for Joey?", new DateTime(2022, 10, 11, 23, 0, 0, 0, DateTimeKind.Utc), 6, new DateTime(2022, 10, 11, 23, 0, 0, 0, DateTimeKind.Utc), 3, null, 4, null, null, null },
                    { 48, "I can't I got to work", new DateTime(2022, 12, 10, 1, 0, 0, 0, DateTimeKind.Utc), 4, new DateTime(2022, 10, 11, 23, 2, 0, 0, DateTimeKind.Utc), 3, null, 4, null, null, null },
                    { 49, "I will buy the gift! No Fuss!", new DateTime(2022, 10, 12, 1, 0, 0, 0, DateTimeKind.Utc), 5, new DateTime(2022, 10, 12, 1, 0, 0, 0, DateTimeKind.Utc), 3, null, 4, null, null, null },
                    { 50, "Ford is a crappy car!", new DateTime(2022, 10, 12, 11, 0, 0, 0, DateTimeKind.Utc), 1, new DateTime(2022, 12, 10, 12, 0, 0, 0, DateTimeKind.Utc), 2, null, null, null, 4, null },
                    { 51, "Not a Mustang!", new DateTime(2022, 12, 10, 13, 0, 0, 0, DateTimeKind.Utc), 2, new DateTime(2022, 10, 11, 23, 14, 0, 0, DateTimeKind.Utc), 2, null, null, null, 4, null }
                });

            migrationBuilder.InsertData(
                table: "TopicMember",
                columns: new[] { "MembersId", "TopicsId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "TopicMember",
                columns: new[] { "MembersId", "TopicsId" },
                values: new object[,]
                {
                    { 1, 6 },
                    { 1, 8 },
                    { 2, 1 },
                    { 2, 3 },
                    { 2, 6 },
                    { 2, 8 },
                    { 3, 6 },
                    { 3, 8 },
                    { 4, 1 },
                    { 4, 3 },
                    { 4, 6 },
                    { 4, 8 },
                    { 5, 4 },
                    { 5, 6 },
                    { 5, 8 },
                    { 6, 6 },
                    { 6, 8 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EventGroupInvite_GroupsId",
                table: "EventGroupInvite",
                column: "GroupsId");

            migrationBuilder.CreateIndex(
                name: "IX_EventTopicInvite_TopicsId",
                table: "EventTopicInvite",
                column: "TopicsId");

            migrationBuilder.CreateIndex(
                name: "IX_EventUserInvite_UsersId",
                table: "EventUserInvite",
                column: "UsersId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMember_MembersId",
                table: "GroupMember",
                column: "MembersId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_CreatedByUserId",
                table: "Posts",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ReplyParentId",
                table: "Posts",
                column: "ReplyParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetEventId",
                table: "Posts",
                column: "TargetEventId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetGroupId",
                table: "Posts",
                column: "TargetGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetTopicId",
                table: "Posts",
                column: "TargetTopicId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetUserId",
                table: "Posts",
                column: "TargetUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RSVPs_EventId",
                table: "RSVPs",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_RSVPs_UserId",
                table: "RSVPs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TopicMember_TopicsId",
                table: "TopicMember",
                column: "TopicsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventGroupInvite");

            migrationBuilder.DropTable(
                name: "EventTopicInvite");

            migrationBuilder.DropTable(
                name: "EventUserInvite");

            migrationBuilder.DropTable(
                name: "GroupMember");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "RSVPs");

            migrationBuilder.DropTable(
                name: "TopicMember");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "Topics");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
