/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
		extend: {
      colors: {
        publicGroup: "#51D05D",
        main: "#FF2F02",
        maindark: "#D62F02",
        accent: "#02FFB7",
        accentdark: "#02CFA5",
        secondary: "#FF4A4A",
      },
			fontFamily: {
        body: ['Outfit', 'sans-serif'],
      },
      fontWeight: {
        hairline: 100,
        thin: 200,
        light: 300,
        normal: 400,
        medium: 500,
        semibold: 600,
        bold: 700,
        extrabold: 800,
        black: 900,
      },
      backgroundImage: {
        'eventImage': "url('/images/party.bmp')",
      }
		}
  },
  plugins: [],
}
