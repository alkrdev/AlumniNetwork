import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import isoWeek from 'dayjs/plugin/isoWeek';

const Calendar = ({ events }) => {
	const [currentDate, setCurrentDate] = useState(dayjs());
	const [month, setMonth] = useState({ name: '', number: -1 });
	const [year, setYear] = useState(-1);
	const [numbers, setNumbers] = useState([]);
	const [offset, setOffset] = useState(-1);
	const [activeDays, setActiveDays] = useState([]);

	const monthNames = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	];

	const changeMonth = (e, direction) => {
		if (direction === 'backward') {
			setCurrentDate(dayjs(currentDate).subtract(1, 'month'));
		} else if (direction === 'forward') {
			setCurrentDate(dayjs(currentDate).add(1, 'month'));
		}
	};

	useEffect(() => {
		setMonth({ name: monthNames[currentDate.get('month')], number: currentDate.get('month') });
		setYear(currentDate.get('year'));
		setNumbers(Array.from({ length: currentDate.daysInMonth() }, (x, i) => i + 1));

		dayjs.extend(isoWeek);

		var startOfMonth = dayjs(currentDate).startOf('month').isoWeekday();
		var newOffset = startOfMonth - 1;
		setOffset(newOffset);
	}, [currentDate]);

	useEffect(() => {
		if (events !== undefined && events !== null && !events.length < 1) {
			var dateNumbers = events.map((e) => dayjs(e.startTime).date());
			setActiveDays(dateNumbers);
		}
	}, [events]);

	return (
		<div className='flex items-center justify-center py-8 px-4'>
			<div className='max-w-sm w-full shadow-lg'>
				<div className='md:p-8 p-5 dark:bg-gray-200 bg-white rounded-t'>
					<div className='px-4 flex items-center justify-between'>
						<span tabIndex='0' className='focus:outline-none  text-base font-bold dark:text-black-100 text-black-800'>
							{month.name} {year}
						</span>
						<div className='flex items-center'>
							<button
								onClick={(e) => changeMonth(e, 'backward')}
								aria-label='calendar backward'
								className='focus:text-black-400 hover:text-black-400 text-black-800 dark:text-black-100'
							>
								<svg
									xmlns='http://www.w3.org/2000/svg'
									className='icon icon-tabler icon-tabler-chevron-left'
									width='24'
									height='24'
									viewBox='0 0 24 24'
									strokeWidth='1.5'
									stroke='currentColor'
									fill='none'
									strokeLinecap='round'
									strokeLinejoin='round'
								>
									<path stroke='none' d='M0 0h24v24H0z' fill='none' />
									<polyline points='15 6 9 12 15 18' />
								</svg>
							</button>
							<button
								onClick={(e) => changeMonth(e, 'forward')}
								aria-label='calendar forward'
								className='focus:text-black-400 hover:text-black-400 ml-3 text-black-800 dark:text-black-100'
							>
								<svg
									xmlns='http://www.w3.org/2000/svg'
									className='icon icon-tabler  icon-tabler-chevron-right'
									width='24'
									height='24'
									viewBox='0 0 24 24'
									strokeWidth='1.5'
									stroke='currentColor'
									fill='none'
									strokeLinecap='round'
									strokeLinejoin='round'
								>
									<path stroke='none' d='M0 0h24v24H0z' fill='none' />
									<polyline points='9 6 15 12 9 18' />
								</svg>
							</button>
						</div>
					</div>
					<div className='flex items-center justify-between pt-12 overflow-x-auto'>
						<table className='w-full'>
							<thead>
								<tr>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>Mo</p>
										</div>
									</th>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>Tu</p>
										</div>
									</th>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>We</p>
										</div>
									</th>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>Th</p>
										</div>
									</th>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>Fr</p>
										</div>
									</th>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>Sa</p>
										</div>
									</th>
									<th>
										<div className='w-full flex justify-center'>
											<p className='text-base text-center text-black-800 dark:text-black-100'>Su</p>
										</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									{Array.from({ length: offset }, (x, i) => i + 1).map((offsetElement, index) => (
										<td key={index}>
											<EmptyCalendarElement />
										</td>
									))}
									{numbers.map((data, index) => {
										var numberIsIncluded = activeDays.includes(data);
										var underlineValue = numberIsIncluded
											? 'underline text-thin cursor-pointer hover:bg-accentdark border border-indigo-500'
											: '';

										return (
											index + offset < 7 && (
												<td key={index} className={underlineValue}>
													<CalendarElement data={data} currentDate={currentDate} />
												</td>
											)
										);
									})}
								</tr>

								{Array.from({ length: 5 }, (x, i) => i + 1).map((x, index) => (
									<tr key={index}>
										{numbers.map((data, index) => {
											var numberIsIncluded = activeDays.includes(data);
											var underlineValue = numberIsIncluded
												? 'underline text-thin cursor-pointer hover:bg-accentdark border border-indigo-500'
												: '';

											return (
												index + offset >= 7 * x &&
												index + offset < 7 * (x + 1) && (
													<td key={index} className={underlineValue}>
														<CalendarElement data={data} currentDate={currentDate} />
													</td>
												)
											);
										})}
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

const CalendarElement = ({ data, underlined, currentDate }) =>
	currentDate.date() === data && currentDate.get('month') === dayjs().get('month') ? (
		<div className='w-full h-full'>
			<div className='flex items-center justify-center w-full rounded-full'>
				<a
					role='link'
					tabIndex='0'
					className='focus:outline-none  focus:ring-2 focus:ring-offset-2 hover:bg-red-400 text-base w-8 h-8 flex items-center justify-center text-white bg-main rounded-full'
				>
					{data}
				</a>
			</div>
		</div>
	) : (
		<div className='px-2 py-2 flex w-full justify-center'>
			<p className='text-base text-black-500 dark:text-black-100'>{data}</p>
		</div>
	);

const EmptyCalendarElement = () => (
	<div className='px-2 py-2 flex w-full justify-center'>
		<p className='text-base text-black-500 dark:text-black-100'></p>
	</div>
);

export default Calendar;

{
	/* https://tailwindcomponents.com/component/free-tailwind-css-calendar-component
more free and premium Tailwind CSS components at https://tailwinduikit.com/ */
}
