import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const Post = ({ post, type }) => {
	const [postTarget, setPostTarget] = useState();

	const Target = {
		0: 'User',
		1: 'Group',
		2: 'Topic',
		3: 'Event',
	};

	const navigate = useNavigate();

	const GetPostTarget = async () => {
		switch (post.postTarget) {
			case 0:
				setPostTarget('\u21d2 ' + Target[post.postTarget] + ' : ' + post.targetUserName);
				break;
			case 1:
				setPostTarget('\u21d2 ' + Target[post.postTarget] + ' : ' + post.targetGroupName);
				break;
			case 2:
				setPostTarget('\u21d2 ' + Target[post.postTarget] + ' : ' + post.targetTopicName);
				break;
			case 3:
				setPostTarget('\u21d2 ' + Target[post.postTarget] + ' : ' + post.targetEventName);
				break;
			default:
				setPostTarget('\u21d2 ' + Target[post.postTarget] + ' : ' + post.targetGroupName);
				setPostTarget('');
		}
	};

	const NavigateToProfile = (id) => {
		navigate(`/profile/${id}`);
		window.location.reload();
	};

	useEffect(() => {
		GetPostTarget();
	}, []);

	const textSize = type === 'profile' ? 'text-md' : 'text-3xl';

	const postPath = post.createdByUserImage ? post.createdByUserImage : 'https://via.placeholder.com/25';
	const postDate = new Date(post.created).toLocaleDateString('en-uk', {
		weekday: 'short',
		year: '2-digit',
		month: 'short',
		day: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
	});
	const updateDate = new Date(post.lastUpdated).toLocaleDateString('en-uk', {
		year: '2-digit',
		month: 'numeric',
		day: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
	});

	return (
		<div className='rounded-3xl border-black border-2 overflow-hidden w-11/12'>
			<div className='flex items-center gap-2 mx-4 my-2'>
				{/* {post.topics.map((topic, index) => {
                    return <div key={index} className="text-white text-sm h-5 bg-main rounded-3xl px-2">{topic}</div>
                })} */}
			</div>
			{/*<h2 className={`${textSize} font-semibold mx-4`}>{post.body}</h2>*/}
			<div className='flex items-center gap-2 m-4'>
				<img
					onClick={() => NavigateToProfile(post.createdByUser)}
					className='rounded-full w-auto h-20 hover:cursor-pointer'
					src={postPath}
					alt='profileimage'
				></img>
				<div className='max-w-2/12 mr-4'>
					<div onClick={() => NavigateToProfile(post.createdByUser)} className='text-xs font-black hover:cursor-pointer'>
						{post.createdByUserName}
					</div>
					<div className='text-xs font-thin'>{postDate}</div>
					<div className='text-[10px] font-thin text-slate-600'>Updated: {updateDate}</div>
				</div>
				<div className='${textSize} w-8/12 break-words justify-self-start'>{post.body}</div>
			</div>
			<div
				onClick={() => NavigateToProfile(post.createdByUser)}
				className='bg-accent px-6 py-1 rounded-br-3xl text-sm hover:cursor-pointer'
			>
				{postTarget}
			</div>
		</div>
	);
};

export default Post;
