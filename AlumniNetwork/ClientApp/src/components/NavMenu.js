import React from 'react';
import './NavMenu.css';
import keycloak from '../keycloak';

import Logo from '../images/logo.bmp';
import { Link } from 'react-router-dom';

const NavMenu = () => {
	return (
		<header>
			<nav className='bg-white w-10/12 py-2.5 mx-auto'>
				<div className='container flex flex-wrap justify-between items-center mx-auto'>
					<Link to='/' className='flex items-center'>
						<img src={Logo} className='mr-3 h-6 sm:h-9' alt='Alumni Logo' />
						<span className='self-center text-xl font-semibold whitespace-nowrap uppercase text-black'>
							Alumni
						</span>
					</Link>
					{keycloak.authenticated && <div className='w-auto' id='navbar-default'>
						<ul className='flex p-4 flex-row space-x-8 mt-0 text-sm font-medium bg-white'>
							<li>
								<Link
									to='/timelines'
									className='block py-2 pr-4 pl-3 text-black font-bold hover:underline decoration-main decoration-2'
									aria-current='page'
								>
									Timelines
								</Link>
							</li>
							<li>
								<Link
									to='/groups'
									className='block py-2 pr-4 pl-3 text-black font-bold hover:underline decoration-main decoration-2'
								>
									Groups
								</Link>
							</li>
							<li>
								<Link
									to='/profile'
									className='block py-2 pr-4 pl-3 text-black font-bold hover:underline decoration-main decoration-2'
								>
									Profile
								</Link>
							</li>
							<li>
								<div
									to='/profile'
									className='block py-2 pr-4 pl-3 text-black font-bold hover:cursor-pointer hover:underline decoration-main decoration-2'
									onClick={() => keycloak.logout()}
								>
									Logout
								</div>
							</li>
						</ul>
					</div>}
				</div>
			</nav>
		</header>
	);
};

export default NavMenu;
