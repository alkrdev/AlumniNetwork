import React, { useState } from 'react';

import { AuthProvider }  from "../src/context/AuthContext"
import { BaseRouter } from './BaseRouter';

const App = () => {
	return (
		<AuthProvider>		
			<BaseRouter />
		</AuthProvider>
	);
};

export default App;
