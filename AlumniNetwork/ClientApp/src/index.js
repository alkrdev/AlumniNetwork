import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Loading from './components/Loading';
import { initialize } from './keycloak';

import './index.css';

/* const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const rootElement = document.getElementById('root');

ReactDOM.render(
  <BrowserRouter basename={baseUrl}>
    <App />
  </BrowserRouter>,
rootElement); */

const root = ReactDOM.createRoot(document.getElementById('root'));

// Display a loading screen when connecting to Keycloak
root.render(<Loading message='Connecting to Keycloak...' />);

// Initialize Keycloak
initialize()
	.then(() => {
		// If No Keycloak Error occurred - Display the App
		root.render(
			<App />
		);
	})
	.catch(() => {
		root.render(
			<p>Could Not Connect To Keycloak.</p>
		);
	});
