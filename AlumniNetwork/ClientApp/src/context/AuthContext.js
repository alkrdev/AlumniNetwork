import React, { createContext, useState, useEffect } from 'react';
import { fetchUser } from '../api/users';
import keycloak from '../keycloak';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
	const [authUser, setAuthUser] = useState({});

	const CheckAuthenticatedUser = async () => {
		const { user, error } = await fetchUser();

		if (user) {
			setAuthUser(user);
		}
	};

	useEffect(() => {
		// First check if the user is authenticated with keycloak
		if (keycloak.authenticated) {
			// Start the login flow. If it is already started, we skip it to prevent multiple calls.
			CheckAuthenticatedUser();
		} else {
			// Don't trigger the login flow if it has already begun.
			return;
		}
	}, []);

	return <AuthContext.Provider value={{ authUser, setAuthUser }}>{children}</AuthContext.Provider>;
};
