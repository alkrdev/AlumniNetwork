import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ROLES } from './const/roles';
import KeycloakRoute from './routes/KeycloakRoute';

import NavMenu from './components/NavMenu';

import Groups from './pages/Groups';
import Group from './pages/Groups/Group';
import Home from './pages/Home';
import Profile from './pages/Profile';
import Timelines from './pages/Timelines';
import Event from './pages/Event';

export const BaseRouter = () => {
	return (
		<BrowserRouter>
			<NavMenu />
			<Routes>
				<Route path='/' element={<Home />} />
				<Route
					path='/timelines'
					element={
						<KeycloakRoute role={ROLES.User}>
							<Timelines />
						</KeycloakRoute>
					}
				/>
				<Route
					path='/groups'
					element={
						<KeycloakRoute role={ROLES.User}>
							<Groups />
						</KeycloakRoute>
					}
				/>
				<Route
					path='/groups/:groupId'
					element={
						<KeycloakRoute role={ROLES.User}>
							<Group />
						</KeycloakRoute>
					}
				/>
				<Route
					path='/profile'
					element={
						<KeycloakRoute role={ROLES.User}>
							<Profile />
						</KeycloakRoute>
					}
				/>
				<Route
					path='/profile/:profileId'
					element={
						<KeycloakRoute role={ROLES.User}>
							<Profile />
						</KeycloakRoute>
					}
				/>
				<Route
					path='/event/:eventId'
					element={
						<KeycloakRoute role={ROLES.User}>
							<Event />
						</KeycloakRoute>
					}
				/>
			</Routes>
		</BrowserRouter>
	);
};
