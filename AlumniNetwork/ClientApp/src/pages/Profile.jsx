import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { fetchEvents, fetchAcceptedEvents, deleteRSVP } from '../api/events';
import { createRSVP } from '../api/events';
import { fetchDirectPosts, fetchDirectPostsId } from '../api/posts';
import { fetchUser, fetchUserById, setUser } from '../api/users';
import CreatePostModal from './Timelines/CreateSelfPostModal';
import { useForm } from 'react-hook-form';

import MGlass from '../images/icons/magnifying_glass.svg';

import Post from '../components/Post';
import Calendar from '../components/Calendar';
import { useNavigate, useParams } from 'react-router-dom';


const Profile = () => {
	const [posts, setPosts] = useState([]);
	const [events, setEvents] = useState([]);
	const [acceptedEvents, setAcceptedEvents] = useState([]);
	const [isOpen, setIsOpen] = useState(false);
	const [isEditing, setIsEditing] = useState(false);

	const [filterText, setFilterText] = useState('');
	const [data, setData] = useState({});
	const [loading, setLoading] = useState(true);

	const navigate = useNavigate();

	const handleCancelClick = (e) => {
		e.preventDefault();
		setIsEditing(false);
	};

	const handleEditClick = (e) => {
		e.preventDefault();
		setIsEditing(true);
	};

	// Enable React-hook-form
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm();

	// Define form validation for profile name
	const nameTextConfig = {
		required: true,
		minLength: 2,
		maxLength: 40,
		pattern: /^[a-zA-Z ]+$/g,
	};

	// Update the user profile.
	const onEditSubmit = async (e) => {
		let requestData = {
			name: e.newName,
			profileImage: e.newProfileImage,
			status: e.newStatus,
			funFact: e.newFunFact,
		};
		// Run setUser api function to update the profile.
		const { user, error } = await setUser(data.id, requestData);

		if (error == null) {
			const newData = { ...data, name: user.name, profileImage: user.profileImage, status: user.status, funFact: user.funFact };
			setData(() => ({ ...newData }));
		} else {
			formErrorMsg = 'Could not update profile';
		}
		setIsEditing(false);
		GetPosts();
	};

	// Check for edit profile form errors and display validation errors.
	const formErrorMsg = (() => {
		if (!errors.newName) {
			// Breaks out if no error
			return null;
		}
		if (errors.newName.type === 'required') {
			return <p className='text-red-500'> Name is required </p>;
		}
		if (errors.newName.type === 'pattern') {
			return <p className='text-red-500'> Only letter inputs </p>;
		}
		if (errors.newName.type === 'maxLength') {
			return <p className='text-red-500'> Name is too long (max 40 letters) </p>;
		}
		if (errors.newName.type === 'minLength') {
			return <p className='text-red-500'> Name is too short (min 2 letters) </p>;
		}
	})();

	const handleFilterText = (e) => {
		setFilterText(e.target.value);
	};

	const handleClick = (e) => {
		e.preventDefault();
		setIsOpen(true);
	};

	const NavigateToEvent = (id) => {
		navigate(`/event/${id}`);
	};

	const handleAttend = (e) => {
		createRSVP(e);

		var newEventArray = events.filter((x) => x.id !== e);
		newEventArray.sort((a, b) => a.startTime > b.startTime);
		setEvents(newEventArray);

		var newAcceptedArray = [...acceptedEvents, events.find((x) => x.id === e)];
		newAcceptedArray.sort((a, b) => a.startTime > b.startTime);
		setAcceptedEvents(newAcceptedArray);
	};

	const handleLeave = (e) => {
		deleteRSVP(e);

		var newAcceptedArray = acceptedEvents.filter((x) => x.id !== e);
		newAcceptedArray.sort((a, b) => a.startTime > b.startTime);
		setAcceptedEvents(newAcceptedArray);

		var newEventArray = [...events, acceptedEvents.find((x) => x.id === e)];
		newEventArray.sort((a, b) => a.startTime > b.startTime);
		setEvents(newEventArray);
	};

	const GetPosts = async () => {
		if (params.profileId) {
			const { posts, error } = await fetchDirectPostsId(params.profileId);
			setPosts(posts);
		} else {
			const { posts, error } = await fetchDirectPosts();
			setPosts(posts);
		}
	};

	const GetEvents = async () => {
		const { events, error } = await fetchEvents();

		var eventsArray = events;
		eventsArray.sort((a, b) => a.startTime > b.startTime);
		setEvents(eventsArray);
	};

	const GetAcceptedEvents = async () => {
		const { acceptedEvents, error } = await fetchAcceptedEvents();

		var acceptedArray = acceptedEvents;
		acceptedArray.sort((a, b) => a.startTime > b.startTime);
		setAcceptedEvents(acceptedArray);
	};

	let params = useParams();

	const GetUser = async () => {
		if (params.profileId) {
			const { user, error } = await fetchUserById(params.profileId);
			setData(user);
		} else {
			const { user, error } = await fetchUser();
			setData(user);
		}
	};

	useEffect(() => {
		GetUser().then(() => {
			Promise.all([
				GetPosts(), 
				GetEvents(),
				GetAcceptedEvents()
			]).then(() => {
				setLoading(false);
			})
		});
	}, []);

	const postPath = data.profileImage ? data.profileImage : 'https://via.placeholder.com/25';

	return (
		<div className='container mx-auto px-20 h-screen'>
			{!loading && (
				<>
					<section className='flex items-center gap-2'>
						<img className='rounded-full w-1/4' src={postPath} alt='profileimage'></img>
						<div className='w-2/3'>
							{!isEditing && (
								<div>
									<h2 className='text-3xl'>{data.name}</h2>
									<div>{data.status}</div>
									<div className='text-xs font-thin'>{data.funFact}</div>
									{!params.profileId && (
										<img
											onClick={handleEditClick}
											src='/images/edit_icon.png'
											className='w-8 hover:cursor-pointer'
											alt='edit_icon'
										></img>
									)}
								</div>
							)}
							{isEditing && (
								<div className='flex flex-col'>
									<form className='flex flex-col' onSubmit={handleSubmit(onEditSubmit)}>
										<label className='text-gray-400'>Name:</label>
										<input
											type='text'
											{...register('newName', nameTextConfig)}
											defaultValue={data.name}
											className='text-3xl hover:bg-gray-200'
											placeholder='Name'
										></input>
										<label className='text-gray-400'>Status:</label>
										<input
											type='text'
											{...register('newStatus')}
											defaultValue={data.status}
											className='hover:bg-gray-200'
											placeholder='status'
										></input>
										<label className='text-gray-400'>Fun fact:</label>
										<input
											type='text'
											{...register('newFunFact')}
											defaultValue={data.funFact}
											className='hover:bg-gray-200'
											placeholder='fun Fact'
										></input>
										<label className='text-gray-400'>Profile Image:</label>
										<input
											type='text'
											{...register('newProfileImage')}
											defaultValue={data.profileImage}
											className='hover:bg-gray-200'
											placeholder='image link'
										></input>
										<input
											type='image'
											className='w-8 hover:cursor-pointer'
											src='/images/checkmark_icon.png'
											alt='checkmark_icon'
										/>
										{formErrorMsg}
									</form>
									<img
										onClick={handleCancelClick}
										src='/images/redcross_icon.png'
										className='w-8 hover:cursor-pointer'
										alt='redcross_icon'
									></img>
								</div>
							)}
						</div>
					</section>
					<div className='bg-gray-200 text-sm rounded-lg p-2 mt-5'>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
						aliqua. Vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut aliquam purus sit. Mi proin
						sed libero enim sed faucibus turpis. Libero volutpat sed cras ornare arcu dui vivamus arcu felis. Sed nisi lacus sed
						viverra tellus in. At erat pellentesque adipiscing commodo. Sit amet nisl suscipit adipiscing. A condimentum vitae
						sapien pellentesque habitant morbi. Risus feugiat in ante metus dictum at. Scelerisque in dictum non consectetur a.
						Consectetur libero id faucibus nisl tincidunt eget.
					</div>
					<div className='flex'>
						<div className={!params.profileId ? 'w-3/5 flex flex-col gap-5' : 'flex w-full items-center flex-col gap-5'}>
							<div className='flex rounded-full border-2 w-11/12 px-2 my-6'>
								<img src={MGlass} alt='mglass'></img>
								<input
									onChange={handleFilterText}
									className='h-13 rounded-xl w-full outline-none pl-2'
									placeholder='Search by title, content, or replies...'
								></input>
								<div
									onClick={handleClick}
									className='h-9 w-20 text-white font-bold flex self-center items-center justify-center rounded-full bg-main hover:bg-maindark hover:cursor-pointer'
								>
									NEW +
								</div>
							</div>
							{posts
								.filter((p) => {
									return p.body.toLowerCase().includes(filterText.toLowerCase());
								})
								.map((post, index) => {
									return <Post key={index} post={post} />;
								})}
						</div>
						{!params.profileId && (
							<div className='w-2/5'>
								<div className='w-11/12'>
									<Calendar events={acceptedEvents} />
								</div>
								<div>ATTENDING EVENTS</div>
								<ul>
									{acceptedEvents.map((e) => {
										return (
											<li className='py-1'>
												<div className='flex rounded-2xl border-black border-2 overflow-hidden w-11/12'>
													<div className='flex justify-center flex-col p-2 text-xs w-full'>
														<div
															onClick={() => NavigateToEvent(e.id)}
															className='text-sm w-full font-bold hover:cursor-pointer'
														>
															{e.body}
														</div>
														<div className='flex w-full'>
															<div className='flex self-center gap-1'>
																<div>
																	{new Date(e.startTime).toLocaleDateString('en-uk', {
																		year: '2-digit',
																		month: '2-digit',
																		day: 'numeric',
																		hour: '2-digit',
																		minute: '2-digit',
																	})}
																</div>
																<span>-</span>
																<span>
																	{new Date(e.endTime).toLocaleDateString('en-uk', {
																		year: '2-digit',
																		month: '2-digit',
																		day: 'numeric',
																		hour: '2-digit',
																		minute: '2-digit',
																	})}
																</span>
															</div>
															<button
																onClick={() => handleLeave(e.id)}
																className=' rounded-2xl self-end ml-auto h-6 text-xs font-bold py-1 px-2 rounded bg-main text-white hover:bg-maindark'
															>
																Leave
															</button>
														</div>
													</div>
												</div>
											</li>
										);
									})}
								</ul>
								<div>INVITATIONS</div>
								<ul>
									{events
										.map((e, index) => {
											return (
												<li key={index} className='py-1'>
													<div className='flex rounded-2xl border-black border-2 overflow-hidden w-11/12'>
														<div className='flex justify-center flex-col p-2 text-xs w-full'>
															<div
																onClick={() => NavigateToEvent(e.id)}
																className='text-sm w-full font-bold hover:cursor-pointer'
															>
																{e.body}
															</div>
															<div className='flex w-full'>
																<div className='flex self-center gap-1'>
																	<div>
																		{new Date(e.startTime).toLocaleDateString('en-uk', {
																			year: '2-digit',
																			month: '2-digit',
																			day: 'numeric',
																			hour: '2-digit',
																			minute: '2-digit',
																		})}
																	</div>
																	<span>-</span>
																	<span>
																		{new Date(e.endTime).toLocaleDateString('en-uk', {
																			year: '2-digit',
																			month: '2-digit',
																			day: 'numeric',
																			hour: '2-digit',
																			minute: '2-digit',
																		})}
																	</span>
																</div>
																<button
																	onClick={() => handleAttend(e.id)}
																	className=' rounded-2xl self-end ml-auto h-6 text-xs font-bold py-1 px-2 rounded bg-accent text-white hover:bg-accentdark'
																>
																	Attend
																</button>
															</div>
														</div>
													</div>
												</li>
											);
										})}
								</ul>
							</div>
						)}
					</div>
				</>
			)}
			{isOpen ? <CreatePostModal setIsOpen={setIsOpen} targetUserId={params.profileId} /> : <></>}
		</div>
	);
};

export default Profile;
