import { useEffect, useState, useRef, useContext } from 'react';
import keycloak from '../keycloak';
import { fetchUser, createUser, fetchUserCount } from '../api/users';
import { useNavigate } from 'react-router-dom';
import Laugh from '../images/laughman.png';

import { AuthContext } from '../context/AuthContext';

const Home = () => {
	const loginFlowStarted = useRef(false);
	const [error, setError] = useState('');
	const navigate = useNavigate();
	const [userCount, setUserCount] = useState(-1);

	const { authUser, setAuthUser } = useContext(AuthContext);

	const GetUserCount = async () => {
		const { data, error } = await fetchUserCount();

		setUserCount(data);
	};

	// Create a new user
	const CreateNewUser = async () => {
		const { user, error } = await createUser();

		// Check if the user is created otherwise return an error.
		if (user) {
			setAuthUser(user);
			navigate('/timelines');
		} else {
			setError('Could not perform login, user could not be created');
		}
	};

	// Check if a user exist.
	const CheckAuthenticatedUser = async () => {
		const { user, error } = await fetchUser();

		if (user) {
			if (user.status === 404) {
				// User is not found in Alumni db, so we must create it.
				await CreateNewUser();
			} else {
				setAuthUser(user);
				// If the user is found, we navigate to timelines.
				navigate('/timelines');
			}
		} else {
			setError('Could not perform login');
		}
	};

	useEffect(() => {
		// First check if the user is authenticated with keycloak
		if (keycloak.authenticated) {
			// Start the login flow. If it is already started, we skip it to prevent multiple calls.
			if (!loginFlowStarted.current) {
				loginFlowStarted.current = true;
				CheckAuthenticatedUser();
			} else {
				// Don't trigger the login flow if it has already begun.
				return;
			}
		}
		if (userCount === -1) {
			GetUserCount();
		}
	}, []);

	return (
		<div className='flex container overflow-hidden mx-auto px-20 justify-center'>
			<div className='w-[450px] h-80 mt-36'>
				<h1 className='text-main uppercase font-extrabold text-5xl'>Join our alumni network</h1>
				<p className='text-3xl py-2'>
					Keep in touch with, and meet <span className='text-main underline'>{userCount}</span> fellow Noroff Alumni
				</p>
				<button
					onClick={() => keycloak.login()}
					className='w-full h-16 bg-accent hover:bg-accentdark text-5xl font-semibold rounded-xl shadow-md shadow-gray-400 border-none uppercase border px-3'
				>
					Login
				</button>
				{error && <p>{error}</p>}
			</div>

			<div className='absolute bottom-0 object-fill z-[-10] right-0'>
				<img src={Laugh} alt='Laughing Man' className='saturate-50 contrast-200 opacity-40 h-[700px] w-[700px]' />
			</div>
		</div>
	);
};

export default Home;
