import React, { useEffect, useState } from 'react';
import { fetchPosts } from '../../api/posts';
import { v4 as uuidv4 } from 'uuid';

import MGlass from '../../images/icons/magnifying_glass.svg';

import Post from '../../components/Post';
import CreatePostModal from './CreateSelfPostModal';
import keycloak from '../../keycloak';

const Timelines = () => {
	const [posts, setPosts] = useState([]);
	const [isOpen, setIsOpen] = useState(false);
	const [filterText, setFilterText] = useState('');

	const handleFilterText = (e) => {
		setFilterText(e.target.value);
	};

	const handleClick = (e) => {
		e.preventDefault();

		setIsOpen(true);
	};

	const GetPosts = async () => {
		const { posts, error } = await fetchPosts();

		setPosts(posts);
	};

	useEffect(() => {
		if (posts.length === 0) {
			GetPosts();
		}
	}, []);

	return (
		<div className='container mx-auto px-20 flex flex-col items-center gap-5'>
			<div className='flex rounded-full border-2 w-10/12 px-2 my-6'>
				<img src={MGlass} alt='mglass'></img>
				<input
					onChange={handleFilterText}
					className='h-13 rounded-xl w-full outline-none pl-2'
					placeholder='Search by title, content, or replies...'
				></input>
				<div
					onClick={handleClick}
					className='h-9 w-20 text-white font-bold flex self-center items-center justify-center rounded-full bg-main hover:bg-maindark hover:cursor-pointer'
				>
					NEW +
				</div>
			</div>

			{posts
				.filter((p) => {
					return p.body.toLowerCase().includes(filterText.toLowerCase());
				})
				.map((post) => {
					return <Post key={uuidv4()} post={post} />;
				})}

			{/* {events.map(event => {
                return <Event key={event.id} event={event} />
            })} */}

			{isOpen ? <CreatePostModal setIsOpen={setIsOpen} /> : <></>}
		</div>
	);
};

export default Timelines;
