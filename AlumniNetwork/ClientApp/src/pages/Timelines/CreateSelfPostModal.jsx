import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { createPost } from '../../api/posts';
import { AuthContext } from '../../context/AuthContext';

const CreateSelfPostModal = ({ setIsOpen, targetUserId }) => {
	const [topics, setTopics] = useState(['Pets', 'Computers']);
	const [body, setBody] = useState('');

	const { authUser, setAuthUser } = useContext(AuthContext);

	const navigate = useNavigate();

	const handlePost = (e) => {
		e.preventDefault();
		let requestData = {
			body: body,
			postTarget: 0,
			replyParent: 0,
			targetUser: targetUserId ? targetUserId : authUser.id, // UserId
			targetEvent: 0,
			targetGroup: 0,
			targetTopic: 0,
		};

		createPost(requestData).then(() => {
			navigate(0);
		});
	};

	return (
		<div className='w-full h-screen bg-black bg-opacity-40 absolute'>
			<div className='fixed w-3/5 bg-white h-60 left-1/2 top-1/2 translate-y-[-50%] translate-x-[-50%] rounded-lg'>
				<div
					className='absolute top-0 right-0 w-6 h-6 text-center align hover:bg-red-400 hover:cursor-pointer'
					onClick={() => setIsOpen(false)}
				>
					X
				</div>
				<form onSubmit={handlePost} className='absolute top-4 text-2xl left-1/2 translate-x-[-50%] w-full px-4'>
					<h2 className='flex text-main text-2xl justify-center'>Create Post</h2>
					<p className='w-full mt-2'>Hilarious content about</p>
					<div className='flex items-center gap-2 mt-2'>
						{topics.map((topic, index) => {
							return (
								<div key={index} className='text-white text-sm h-5 bg-main rounded-3xl px-2'>
									{topic}
								</div>
							);
						})}
						<div
							key={999}
							onClick={() => setTopics([...topics, 'New Category'])}
							className='text-white text-sm h-5 bg-main rounded-3xl px-2 hover:cursor-pointer'
						>
							+
						</div>
					</div>
					<textarea
						onChange={(e) => setBody(e.target.value)}
						defaultValue={body}
						className='w-full text-xs p-4 border mt-2'
					></textarea>
					<button type='submit' className='w-full bg-accent text-lg'>
						CREATE
					</button>
				</form>
			</div>
		</div>
	);
};

export default CreateSelfPostModal;
