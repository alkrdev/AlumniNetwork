import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { fetchEventById, fetchEvents } from '../api/events';
import { fetchPostsByEventId } from '../api/posts';
import MGlass from '../images/icons/magnifying_glass.svg';
import Post from '../components/Post';
import { v4 as uuidv4 } from 'uuid';
import CreateEventPostModal from './CreateEventPostModal.jsx';
import { AuthContext } from '../context/AuthContext';

const Event = () => {
	const [posts, setPosts] = useState([]);
	const [error, setError] = useState('');
	const [isOpen, setIsOpen] = useState(false);
	const [eventData, setEventData] = useState(null);
	const [filterText, setFilterText] = useState('');
	const [loading, setLoading] = useState(true);

	const { authUser, setAuthUser } = useContext(AuthContext);

	const GetEventPosts = async () => {
		const { posts, error } = await fetchPostsByEventId(params.eventId);
		setPosts(posts);
		setError(error);
	};

	const GetEventData = async () => {
		const { event, error } = await fetchEventById(params.eventId);
		setEventData(event);
		setError(error);
	};

	const handleClick = (e) => {
		e.preventDefault();
		setIsOpen(true);
	};

	const handleFilterText = (e) => {
		setFilterText(e.target.value);
	};

	let params = useParams();

	useEffect(() => {
		GetEventData();
		GetEventPosts();
		setLoading(false);
	}, []);
	
	if (eventData === null) {
		return (
			<div className='flex flex-col text-xl text-red-800 h-[70vh] justify-center items-center'>
				Event not found or you are not permitted to view this event.
				<img
					src='https://i.kym-cdn.com/entries/icons/original/000/002/144/You_Shall_Not_Pass!_0-1_screenshot.jpg'
					className='h-52 mt-5'
					alt='You shall not pass'
				></img>
			</div>
		);
	}
	return (
		<div className='container mx-auto px-20 h-screen'>
			{!loading && (
				<>
					<section
						className='flex h-60 bg-center bg-cover items-end justify-between rounded-lg'
						style={{ backgroundImage: 'url(/images/party.bmp)' }}
					>
						<div className='flex bg-main text-white rounded-tr-lg p-2 text-3xl'>{eventData.body}</div>
					</section>
					<div className='bg-gray-200 text-sm rounded-lg p-2 mt-5'>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
						aliqua. Vel elit scelerisque mauris pellentesque. Amet consectetur adipiscing elit ut aliquam purus sit. Mi proin
						sed libero enim sed faucibus turpis. Libero volutpat sed cras ornare arcu dui vivamus arcu felis. Sed nisi lacus sed
						viverra tellus in. At erat pellentesque adipiscing commodo. Sit amet nisl suscipit adipiscing. A condimentum vitae
						sapien pellentesque habitant morbi. Risus feugiat in ante metus dictum at. Scelerisque in dictum non consectetur a.
						Consectetur libero id faucibus nisl tincidunt eget.
					</div>
					<div className='flex'>
						<div className='flex w-full items-center flex-col gap-5'>
							<div className='flex rounded-full border-2 w-11/12 px-2 my-6'>
								<img src={MGlass} alt='mglass'></img>
								<input
									onChange={handleFilterText}
									className='h-13 rounded-xl w-full outline-none pl-2'
									placeholder='Search by title, content, or replies...'
								></input>
								<div
									onClick={handleClick}
									className='h-9 w-20 text-white font-bold flex self-center items-center justify-center rounded-full bg-main hover:cursor-pointer'
								>
									NEW +
								</div>
							</div>
							<div>
								<span className='bg-gray-200 text-sm rounded-lg p-2 mt-5 mx-5'>
									{eventData.attendingUsers.length} people are attending this event
								</span>
								<span className='bg-gray-200 text-sm rounded-lg p-2 mt-5'>
									From:{' '}
									{new Date(eventData.startTime).toLocaleDateString('en-uk', {
										year: '2-digit',
										month: '2-digit',
										day: 'numeric',
										hour: '2-digit',
										minute: '2-digit',
									})}
								</span>
								<span className='bg-gray-200 text-sm rounded-lg p-2 mt-5 mx-5'>
									To:{' '}
									{new Date(eventData.startTime).toLocaleDateString('en-uk', {
										year: '2-digit',
										month: '2-digit',
										day: 'numeric',
										hour: '2-digit',
										minute: '2-digit',
									})}
								</span>
							</div>

							{posts
								.filter((p) => {
									return p.body.toLowerCase().includes(filterText.toLowerCase());
								})
								.map((post) => {
									return <Post key={uuidv4()} post={post} />;
								})}
						</div>
					</div>
					{isOpen ? <CreateEventPostModal setIsOpen={setIsOpen} /> : <></>}
				</>
			)}
		</div>
	);
};
export default Event;
