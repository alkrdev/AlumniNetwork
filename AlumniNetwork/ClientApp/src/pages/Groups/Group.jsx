import React, { useState, useEffect, useContext } from 'react';
import { fetchGroupById, joinGroup, leaveGroup } from '../../api/groups';
import { fetchPostsByGroupId } from '../../api/posts';
import { fetchGroupEvents, fetchAcceptedEvents, createRSVP, deleteRSVP } from '../../api/events';
import { useNavigate, useParams } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import CreateGroupPostModal from './CreateGroupPostModal';

import Post from '../../components/Post';
import MGlass from '../../images/icons/magnifying_glass.svg';

import Calendar from '../../components/Calendar';

import { AuthContext } from '../../context/AuthContext';
import CreateEventModal from './CreateEventModal';

const Group = () => {
	const [posts, setPosts] = useState([]);
	const [error, setError] = useState('');
	const [isOpen, setIsOpen] = useState(false);
	const [isOpenEvent, setIsOpenEvent] = useState(false);
	const [group, setGroup] = useState(null);
	const [events, setEvents] = useState([]);
	const [acceptedEvents, setAcceptedEvents] = useState([]);
	const [filterText, setFilterText] = useState('');
	const [loading, setLoading] = useState(true);

	const navigate = useNavigate();

	const { authUser, setAuthUser } = useContext(AuthContext);

	const handleClick = (e) => {
		e.preventDefault();
		setIsOpen(true);
	};

	const handleClickEvent = (e) => {
		e.preventDefault();
		setIsOpenEvent(true);
	};

	const handleJoinClick = async (e) => {
		e.preventDefault();
		var { data, error } = await joinGroup(group.id);
		setGroup(data);
		setError(error);
	};

	const handleLeaveClick = async (e) => {
		e.preventDefault();
		setAuthUser(authUser);
		var { data, error } = await leaveGroup(group.id);
		setGroup(data);
		setError(error);
	};

	const NavigateToEvent = (id) => {
		navigate(`/event/${id}`);
	};

	const handleFilterText = (e) => {
		setFilterText(e.target.value);
	};

	const handleAttend = (e) => {
		createRSVP(e);

		var newEventArray = events.filter((x) => x.id !== e);
		newEventArray.sort((a, b) => a.startTime > b.startTime);
		setEvents(newEventArray);

		var newAcceptedArray = [...acceptedEvents, events.find((x) => x.id === e)];
		newAcceptedArray.sort((a, b) => a.startTime > b.startTime);
		setAcceptedEvents(newAcceptedArray);
	};

	const handleLeave = (e) => {
		deleteRSVP(e);

		var newAcceptedArray = acceptedEvents.filter((x) => x.id !== e);
		newAcceptedArray.sort((a, b) => a.startTime > b.startTime);
		setAcceptedEvents(newAcceptedArray);

		var newEventArray = [...events, acceptedEvents.find((x) => x.id === e)];
		newEventArray.sort((a, b) => a.startTime > b.startTime);
		setEvents(newEventArray);
	};

	const GetGroupData = async () => {
		const { group, error } = await fetchGroupById(params.groupId);
		setGroup(group);
		setError(error);
	};

	const GetGroupPosts = async () => {
		const { posts, error } = await fetchPostsByGroupId(params.groupId);
		setPosts(posts);
		setError(error);
	};

	const GetGroupEvents = async () => {
		const { events, error } = await fetchGroupEvents(params.groupId);
		var eventsArray = events;
		eventsArray.sort((a, b) => a.startTime > b.startTime);
		setEvents(eventsArray);
	};

	const GetAcceptedEvents = async () => {
		const { acceptedEvents, error } = await fetchAcceptedEvents();
		var acceptedArray = acceptedEvents;
		acceptedArray.sort((a, b) => a.startTime > b.startTime);
		setAcceptedEvents(acceptedArray);
	};

	let params = useParams();

	console.log(authUser);

	useEffect(() => {
		if (group === null) {
			GetGroupData();
		}
		if (posts.length === 0) {
			GetGroupPosts();
		}
		if (events.length === 0) {
			GetGroupEvents();
		}
		if (acceptedEvents.length === 0) {
			GetAcceptedEvents();
		}
		setLoading(false);
	}, []);

	const ids = acceptedEvents.map((x) => x.id);

	if (group === null) {
		return (
			<div className='flex flex-col text-xl text-red-800 h-[70vh] justify-center items-center'>
				Group not found or you are not permitted to view this event.
				<img
					src='https://i.kym-cdn.com/entries/icons/original/000/002/144/You_Shall_Not_Pass!_0-1_screenshot.jpg'
					className='h-52 mt-5'
					alt='You shall not pass'
				></img>
			</div>
		);
	}

	console.log(group);
	console.log(authUser.id);
	return (
		<div className='container mx-auto px-20 h-screen'>
			{!loading && (
				<>
					<section
						className='flex h-60 bg-center bg-cover items-end justify-between rounded-lg'
						style={{ backgroundImage: 'url(/images/party.bmp)' }}
					>
						<div className='flex bg-main text-white rounded-tr-lg p-2 text-3xl'>{group.name}</div>
						<div
							onClick={handleLeaveClick}
							className={
								group.members.includes(authUser.id)
									? 'flex justify-start bg-red-900 hover:cursor-pointer hover:bg-red-800 text-white rounded-tl-lg p-2 text-xs'
									: 'hidden'
							}
						>
							Leave Group
						</div>
					</section>
					<div className='bg-gray-200 text-sm rounded-lg p-2 mt-5'>{group.description}</div>
					<div className='flex'>
						<div
							className={
								group.members.includes(authUser.id)
									? 'w-3/5 flex flex-col gap-5'
									: 'flex w-full items-center flex-col gap-5'
							}
						>
							<div className='flex rounded-full border-2 w-11/12 px-2 my-6'>
								<img src={MGlass} alt='mglass'></img>
								<input
									onChange={handleFilterText}
									className='h-13 rounded-xl w-full outline-none pl-2'
									placeholder='Search by title, content, or replies...'
								></input>
								<div
									onClick={handleClick}
									className={
										group.members.includes(authUser.id)
											? 'h-9 w-20 text-white font-bold flex self-center items-center justify-center rounded-full bg-main hover:cursor-pointer'
											: 'hidden'
									}
								>
									NEW +
								</div>
								<div
									onClick={handleJoinClick}
									className={
										group.members.includes(authUser.id)
											? 'hidden'
											: 'h-9 w-20 text-white font-bold flex self-center items-center justify-center rounded-full bg-accent hover:cursor-pointer'
									}
								>
									Join
								</div>
							</div>
							{posts
								.filter((p) => {
									return p.body.toLowerCase().includes(filterText.toLowerCase());
								})
								.map((post) => {
									return <Post key={uuidv4()} post={post} />;
								})}
						</div>

						{group && group.members.includes(authUser.id) && (
							<div className='w-2/5'>
								<div className='w-11/12'>
									<Calendar events={events} />
									<button
										onClick={handleClickEvent}
										className='mx-auto h-6 text-xs font-bold py-1 px-2 rounded bg-accent text-white hover:bg-accentdark'
									>
										New Event
									</button>
								</div>

								<div>ATTENDING EVENTS</div>
								<ul>
									{acceptedEvents.map((e) => {
										return (
											<li className='py-1'>
												<div className='flex rounded-2xl border-black border-2 overflow-hidden w-11/12'>
													<div className='flex justify-center flex-col p-2 text-xs w-full'>
														<div
															onClick={() => NavigateToEvent(e.id)}
															className='text-sm w-full font-bold hover:cursor-pointer'
														>
															{e.body}
														</div>
														<div className='flex w-full'>
															<div className='flex self-center gap-1'>
																<div>
																	{new Date(e.startTime).toLocaleDateString('en-uk', {
																		year: '2-digit',
																		month: '2-digit',
																		day: 'numeric',
																		hour: '2-digit',
																		minute: '2-digit',
																	})}
																</div>
																<span>-</span>
																<span>
																	{new Date(e.endTime).toLocaleDateString('en-uk', {
																		year: '2-digit',
																		month: '2-digit',
																		day: 'numeric',
																		hour: '2-digit',
																		minute: '2-digit',
																	})}
																</span>
															</div>
															<button
																onClick={() => handleLeave(e.id)}
																className=' rounded-2xl self-end ml-auto h-6 text-xs font-bold py-1 px-2 rounded bg-main text-white hover:bg-maindark'
															>
																Leave
															</button>
														</div>
													</div>
												</div>
											</li>
										);
									})}
								</ul>
								<div>INVITATIONS</div>
								<ul>
									{events
										.filter((ae) => !ids.includes(ae.id))
										.map((e, index) => {
											return (
												<li key={index} className='py-1'>
													<div className='flex rounded-2xl border-black border-2 overflow-hidden w-11/12'>
														<div className='flex justify-center flex-col p-2 text-xs w-full'>
															<div
																onClick={() => NavigateToEvent(e.id)}
																className='text-sm w-full font-bold hover:cursor-pointer'
															>
																{e.body}
															</div>
															<div className='flex w-full'>
																<div className='flex self-center gap-1'>
																	<div>
																		{new Date(e.startTime).toLocaleDateString('en-uk', {
																			year: '2-digit',
																			month: '2-digit',
																			day: 'numeric',
																			hour: '2-digit',
																			minute: '2-digit',
																		})}
																	</div>
																	<span>-</span>
																	<span>
																		{new Date(e.endTime).toLocaleDateString('en-uk', {
																			year: '2-digit',
																			month: '2-digit',
																			day: 'numeric',
																			hour: '2-digit',
																			minute: '2-digit',
																		})}
																	</span>
																</div>
																<button
																	onClick={() => handleAttend(e.id)}
																	className=' rounded-2xl self-end ml-auto h-6 text-xs font-bold py-1 px-2 rounded bg-accent text-white hover:bg-accentdark'
																>
																	Attend
																</button>
															</div>
														</div>
													</div>
												</li>
											);
										})}
								</ul>
							</div>
						)}
					</div>
					{isOpen ? <CreateGroupPostModal setIsOpen={setIsOpen} /> : <></>}
					{isOpenEvent ? <CreateEventModal setIsOpenEvent={setIsOpenEvent} /> : <></>}
				</>
			)}
		</div>
	);
};

export default Group;
