import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { createGroup } from '../../api/groups';
import { useParams } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

const CreateGroupModal = ({ setIsOpen }) => {
	const [topics, setTopics] = useState(['Pets', 'Computers']);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [groupImage, setGroupImage] = useState('');
	const [isPrivate, setIsPrivate] = useState(false);

	//const { authUser, setAuthUser } = useContext(AuthContext);

	const navigate = useNavigate();

	const handlePost = (e) => {
		e.preventDefault();
		let requestData = {
			name: name,
			description: description,
			groupImage: groupImage,
			isPrivate: isPrivate,
		};

		createGroup(requestData).then(() => {
			navigate(0);
		});
	};

	const handleCheckbox = (e) => {
		setIsPrivate(!isPrivate);
	};

	let params = useParams();

	return (
		<div className='w-full h-screen bg-black bg-opacity-40 absolute'>
			<div className='fixed w-3/5 bg-white h-96 left-1/2 top-1/2 translate-y-[-50%] translate-x-[-50%] rounded-lg'>
				<div
					className='absolute top-0 right-0 w-6 h-6 text-center align hover:bg-red-400 hover:cursor-pointer'
					onClick={() => setIsOpen(false)}
				>
					X
				</div>
				<form onSubmit={handlePost} className='absolute top-4 text-2xl left-1/2 translate-x-[-50%] w-full px-4'>
					<h2 className='flex text-main text-2xl justify-center'>Create a new Group</h2>
					<p className='w-full mt-2'></p>
					<div className='flex items-center gap-2 mt-2'>
						{/* {topics.map((topic, index) => {
							return (
								<div key={index} className='text-white text-sm h-5 bg-main rounded-3xl px-2'>
									{topic}
								</div>
							);
						})}
						{<div key={999} onClick={() => setTopics([...topics, 'New Category'])} className='text-white text-sm h-5 bg-main rounded-3xl px-2 hover:cursor-pointer'>
							+
						</div>} */}
					</div>
					<p>Name:</p>
					<textarea
						onChange={(e) => setName(e.target.value)}
						defaultValue={name}
						className='w-full text-xs p-4 border mt-2'
					></textarea>
					<p>Description:</p>
					<textarea
						onChange={(e) => setDescription(e.target.value)}
						defaultValue={description}
						className='w-full text-xs p-4 border mt-2'
					></textarea>
					<p>Private:</p>
					<input onChange={handleCheckbox} type='checkbox' checked={isPrivate} />
					<button type='submit' className='w-full bg-accent text-lg'>
						CREATE
					</button>
				</form>
			</div>
		</div>
	);
};

export default CreateGroupModal;
