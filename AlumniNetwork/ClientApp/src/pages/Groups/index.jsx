import React, { useState, useEffect } from 'react';
import { fetchGroups } from '../../api/groups';
import { Link } from 'react-router-dom';
import CreateGroupModal from './CreateGroupModal';

import MGlass from '../../images/icons/magnifying_glass.svg';

const Groups = () => {
	const [groups, setGroups] = useState([]);
	const [error, setError] = useState('');
	const [isOpen, setIsOpen] = useState(false);
	const [filterText, setFilterText] = useState('');

	const handleClick = (e) => {
		e.preventDefault();

		setIsOpen(true);
	};

	const handleFilterText = (e) => {
		setFilterText(e.target.value);
	};

	const GetData = async () => {
		const { groups, error } = await fetchGroups();
		setGroups(groups);
		setError(error);
	};

	useEffect(() => {
		GetData();
	}, []);

	return (
		<div className='container mx-auto px-20 h-screen flex flex-col items-center gap-5 '>
			<div className='flex rounded-full border-2 w-10/12 px-2 my-6'>
				<img src={MGlass} alt='mglass'></img>
				<input onChange={handleFilterText} className='h-13 rounded-xl w-full outline-none pl-2' placeholder='Search by title, content, or replies...'></input>
				<div onClick={handleClick} className='h-9 w-20 text-white font-bold flex self-center items-center justify-center rounded-full bg-main hover:cursor-pointer'>
					NEW+
				</div>
			</div>

			{groups
				.filter((g) => {
					return g.name.toLowerCase().includes(filterText.toLowerCase());
				})
				.map((group) => {
					const labelColor = group.isPrivate === true ? 'bg-main' : 'bg-publicGroup';

					return (
						<div key={group.id} className='rounded-3xl border-black border-2 overflow-hidden w-11/12 hover:cursor-pointer hover:bg-slate-200'>
							<Link to={`${group.id}`} className='text-black'>
								<div className='flex gap-2'>
									<div className={`${labelColor} px-6 py-1 m-0 rounded-br-3xl`}>
										{!group.isPrivate && <>Public</>}
										{group.isPrivate && <>Private</>}
									</div>
									<div className='flex items-center gap-2 mr-4'>
										{/* Topics not set
								{group.topics.map((topic, index) => {
                                return <div key={index} className="text-white text-sm h-5 bg-main rounded-3xl px-2">{topic}</div>
                            })} */}
									</div>
								</div>
								<h2 className='text-3xl mx-4'>{group.name}</h2>
								<div className='flex items-center gap-2 m-4'>
									<div className='text-xs w-full'>{group.description}</div>
								</div>
							</Link>
						</div>
					);
				})}
			{isOpen ? <CreateGroupModal setIsOpen={setIsOpen} /> : <></>}
		</div>
	);
};

export default Groups;
