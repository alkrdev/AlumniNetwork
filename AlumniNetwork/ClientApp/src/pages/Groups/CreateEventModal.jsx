import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { createGroup } from '../../api/groups';
import { useParams } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { createEvent, createEventGroupInvite } from '../../api/events';

const CreateEventModal = ({ setIsOpenEvent }) => {
	const [topics, setTopics] = useState(['Pets', 'Computers']);
	const [body, setBody] = useState('');
	const [bannerImage, setBannerImage] = useState(
		'https://t4.ftcdn.net/jpg/01/20/28/25/360_F_120282530_gMCruc8XX2mwf5YtODLV2O1TGHzu4CAb.jpg'
	);
	const [startTime, setStartTime] = useState(new Date());
	const [endTime, setEndTime] = useState(new Date());

	//const { authUser, setAuthUser } = useContext(AuthContext);

	const navigate = useNavigate();

	const handlePost = (e) => {
		e.preventDefault();
		let requestData = {
			body: body,
			bannerImage: bannerImage,
			startTime: startTime.toISOString(),
			endTime: endTime.toISOString(),
		};

		createEvent(requestData).then((ce) => {
			console.log(ce);
			let requestData = {
				eventId: ce.event.id,
				groupId: params.groupId,
			};

			createEventGroupInvite(requestData).then(() => {
				navigate(0);
			});
		});
	};

	let params = useParams();

	return (
		<div className='w-full h-screen bg-black bg-opacity-40 absolute'>
			<div className='fixed w-3/5 bg-white h-[600px] left-1/2 top-1/2 translate-y-[-50%] translate-x-[-50%] rounded-lg'>
				<div
					className='absolute top-0 right-0 w-6 h-6 text-center align hover:bg-red-400 hover:cursor-pointer'
					onClick={() => setIsOpenEvent(false)}
				>
					X
				</div>
				<form onSubmit={handlePost} className='absolute top-4 text-2xl left-1/2 translate-x-[-50%] w-full px-4'>
					<h2 className='flex text-main text-2xl justify-center'>Create a new Event</h2>
					<p className='w-full mt-2'></p>
					<div className='flex items-center gap-2 mt-2'>
						{/* {topics.map((topic, index) => {
							return (
								<div key={index} className='text-white text-sm h-5 bg-main rounded-3xl px-2'>
									{topic}
								</div>
							);
						})}
						{<div key={999} onClick={() => setTopics([...topics, 'New Category'])} className='text-white text-sm h-5 bg-main rounded-3xl px-2 hover:cursor-pointer'>
							+
						</div>} */}
					</div>
					<p>Event Title:</p>
					<textarea
						onChange={(e) => setBody(e.target.value)}
						defaultValue={body}
						className='w-full h-11 text-xs p-4 border mt-2'
					></textarea>
					<p>Banner Image URL:</p>
					<textarea
						onChange={(e) => setBannerImage(e.target.value)}
						defaultValue={bannerImage}
						className='w-full h-11 text-xs p-4 border mt-2'
					></textarea>
					<p>Start Time:</p>
					<DatePicker
						className='text-xs p-4 border mt-2'
						selected={startTime}
						onChange={(date) => setStartTime(date)}
						selectsStart
						startDate={startTime}
						endDate={endTime}
						timeInputLabel='Time:'
						dateFormat='MM/dd/yyyy h:mm aa'
						showTimeSelect
					/>
					<p>End Time:</p>
					<DatePicker
						className='text-xs p-4 border mt-2'
						selected={endTime}
						onChange={(date) => setEndTime(date)}
						selectsEnd
						startDate={startTime}
						endDate={endTime}
						timeInputLabel='Time:'
						dateFormat='MM/dd/yyyy h:mm aa'
						showTimeSelect
					/>
					<button type='submit' className='w-full bg-accent text-lg'>
						CREATE
					</button>
				</form>
			</div>
		</div>
	);
};

export default CreateEventModal;
