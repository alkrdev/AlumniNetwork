import axios from '.';

/**
 * Fetch user from Users REST API
 * @returns { Promise<{ user: [], error: null | string }>} response
 */
export const fetchUser = async (id = null) => {
	let apiURL = 'api/user';

	if (id !== null) apiURL = `api/user/${id}`;

	try {
		const { data } = await axios.get(apiURL, {
			validateStatus: function (status) {
				// overriding default return status codes that result in a resolved promise.
				// We need to include 404 in case a user does not exist
				return status === 200 || status === 303 || status === 404;
			},
		});
		return Promise.resolve({
			user: data,
			error: null,
		});
	} catch (e) {
		return {
			user: [],
			error: e.message,
		};
	}
};

export const fetchUserCount = async () => {
	const apiURL = 'api/user/count';

	try {
		const { data } = await axios.get(apiURL);
		return Promise.resolve({
			data: data,
			error: null,
		});
	} catch (e) {
		return {
			data: 0,
			error: e.message,
		};
	}
};

/**
 * Fetch user by id from Users REST API
 * @returns { Promise<{ user: [], error: null | string }>} response
 */
export const fetchUserById = async (id) => {
	const apiURL = 'api/user/' + id;

	try {
		const { data } = await axios.get(apiURL);
		return Promise.resolve({
			user: data,
			error: null,
		});
	} catch (e) {
		return {
			user: [],
			error: e.message,
		};
	}
};

/**
 * Create user via Users REST API
 * @returns { Promise<{ user: [], error: null | string }>} response
 */
export const createUser = async () => {
	const apiURL = 'api/user';

	try {
		const { data } = await axios.post(apiURL);
		return Promise.resolve({
			user: data,
			error: null,
		});
	} catch (e) {
		return {
			user: [],
			error: e.message,
		};
	}
};

/**
 * Edits a user via api patch request to api/user/{userid}
 * @returns { Promise<{ user: {}, error: null | string }>} response
 */
export const setUser = async (userId, requestData) => {
	const apiURL = 'api/user/' + userId;

	try {
		const { data } = await axios.patch(apiURL, requestData);
		return Promise.resolve({
			user: data,
			error: null,
		});
	} catch (e) {
		return {
			user: [],
			error: e.message,
		};
	}
};
