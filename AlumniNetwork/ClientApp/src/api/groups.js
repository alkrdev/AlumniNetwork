import axios from '.';

/**
 * Fetch groups from the REST API
 * @returns { Promise<{ groups: [], error: null | string }>} response
 */
export const fetchGroups = async () => {
	const GroupsURL = 'api/group';

	try {
		const { data } = await axios.get(GroupsURL);
		return Promise.resolve({
			groups: data,
			error: null,
		});
	} catch (e) {
		return {
			groups: [],
			error: e.message,
		};
	}
};

/**
 * Fetch a Group by its id.
 * @param {number} groupId
 * @returns { Promise<{ group: [], error: null | string }>} response
 */
export const fetchGroupById = async (groupId) => {
	const groupURL = 'api/Group';

	try {
		const { data, status } = await axios.get(groupURL + '/' + groupId);
		return Promise.resolve({
			group: data,
			error: null,
		});
	} catch (e) {
		return {
			group: null,
			error: e.message,
		};
	}
};

/**
 * create new Group
 * @returns { Promise<{ group: {}, error: null | string }>} response
 */
export const createGroup = async (requestData) => {
	const groupURL = 'api/group';

	try {
		const { data } = await axios.post(groupURL, requestData);
		return Promise.resolve({
			group: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

export const joinGroup = async (requestData) => {
	const groupURL = 'api/group/' + requestData + '/join';

	try {
		const { data } = await axios.post(groupURL);
		return Promise.resolve({
			data: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

export const leaveGroup = async (requestData) => {
	const groupURL = 'api/group/' + requestData + '/leave';

	try {
		const { data } = await axios.delete(groupURL);
		return Promise.resolve({
			data: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};
