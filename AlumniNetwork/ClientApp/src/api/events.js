import axios from '.';

/**
 * SAMPLE FUNCTION: Fetch events from a REST API
 * @returns { Promise<{ events: [], error: null | string }>} response
 */
export const fetchEvents = async () => {
	const EventsURL = 'api/event';

	try {
		const { data } = await axios.get(EventsURL);
		return Promise.resolve({
			events: data,
			error: null,
		});
	} catch (e) {
		return {
			events: [],
			error: e.message,
		};
	}
};

export const createEvent = async (requestData) => {
	const eventURL = 'api/event';

	try {
		const { data } = await axios.post(eventURL, requestData);
		return Promise.resolve({
			event: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

export const createEventGroupInvite = async (requestData) => {
	const eventURL = 'api/event/' + requestData.eventId + '/invite/group/' + requestData.groupId;

	try {
		const { data } = await axios.post(eventURL, requestData);
		return Promise.resolve({
			event: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

export const fetchEventById = async (groupId) => {
	const eventURL = 'api/Event';

	try {
		const { data } = await axios.get(eventURL + '/' + groupId);
		return Promise.resolve({
			event: data,
			error: null,
		});
	} catch (e) {
		return {
			event: null,
			error: e.message,
		};
	}
};

/**
 * Fetch group events from the REST API
 * @returns { Promise<{ events: [], error: null | string }>} response
 */
export const fetchGroupEvents = async (groupId) => {
	const EventsURL = '/api/event/group/' + groupId;

	try {
		const { data } = await axios.get(EventsURL);
		return Promise.resolve({
			events: data,
			error: null,
		});
	} catch (e) {
		return {
			events: [],
			error: e.message,
		};
	}
};

export const deleteRSVP = async (id) => {
	const EventsURL = `api/event/${id}/rsvp`;
	try {
		await axios.delete(EventsURL);
		return Promise.resolve({
			error: null,
		});
	} catch (e) {
		return {
			error: e.message,
		};
	}
};

export const fetchAcceptedEvents = async () => {
	const EventsURL = 'api/event/user';

	try {
		const { data } = await axios.get(EventsURL);
		return Promise.resolve({
			acceptedEvents: data,
			error: null,
		});
	} catch (e) {
		return {
			acceptedEvents: [],
			error: e.message,
		};
	}
};

/**
 * SAMPLE FUNCTION: Create RSVP record from a REST API
 * @returns { Promise<{ events: [], error: null | string }>} response
 */
export const createRSVP = async (id) => {
	const EventsURL = `api/event/${id}/rsvp`;

	try {
		await axios.post(EventsURL);
		return Promise.resolve({
			error: null,
		});
	} catch (e) {
		return {
			error: e.message,
		};
	}
};

/**
 * Fetch a product by its id.
 * @param {number} productId
 * @returns {Promise<{product: { id, name, price, description, quantity } | null, error: null}>}
 */
export const fetchProductById = async (eventId) => {
	const productsURL = 'api/Event';

	try {
		const { data, status } = await axios.get(productsURL + '/' + eventId);
		return Promise.resolve({
			product: data,
			error: null,
		});
	} catch (e) {
		return {
			product: null,
			error: e.message,
		};
	}
};
