import axios from '.';

/**
 * Fetch posts from a REST API
 * @returns { Promise<{ posts: [], error: null | string }>} response
 */
export const fetchPosts = async () => {
	const PostsURL = 'api/post';

	try {
		const { data } = await axios.get(PostsURL);
		return Promise.resolve({
			posts: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

/**
 * SAMPLE FUNCTION: Fetch direct posts from a REST API
 * @returns { Promise<{ posts: [], error: null | string }>} response
 */
export const fetchDirectPosts = async () => {
	const PostsURL = 'api/post/user';

	try {
		const { data } = await axios.get(PostsURL);
		return Promise.resolve({
			posts: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

export const fetchDirectPostsId = async (id) => {
	const PostsURL = 'api/post/user/other/' + id;

	try {
		const { data } = await axios.get(PostsURL);
		return Promise.resolve({
			posts: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

/**
 * SAMPLE FUNCTION: create new post
 * @returns { Promise<{ post: {}, error: null | string }>} response
 */
export const createPost = async (requestData) => {
	const PostsURL = 'api/post';

	try {
		const { data } = await axios.post(PostsURL, requestData);
		return Promise.resolve({
			posts: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

/**
 * Fetches a list of post related to a specific group
 * @returns { Promise<{ posts: {}, error: null | string }>} response
 */
export const fetchPostsByGroupId = async (groupId) => {
	const postsURL = 'api/post/group';

	try {
		const { data } = await axios.get(postsURL + '/' + groupId);
		return Promise.resolve({
			posts: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};

export const fetchPostsByEventId = async (groupId) => {
	const postsURL = 'api/post/event';

	try {
		const { data } = await axios.get(postsURL + '/' + groupId);
		return Promise.resolve({
			posts: data,
			error: null,
		});
	} catch (e) {
		return {
			posts: [],
			error: e.message,
		};
	}
};
