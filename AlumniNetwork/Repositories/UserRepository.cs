﻿using AlumniNetwork.DbContexts;
using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using MessagePack.Formatters;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;

namespace AlumniNetwork.Repositories
{
    /// <summary>
    /// Implementation of the IUserRepository containing Entity Framework operations.
    /// </summary>
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly AlumniDb _ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="ctx"></param>
        public UserRepository(AlumniDb ctx)
        {
            _ctx = ctx;
        }
        
        /// <inheritdoc/>
        async public Task<User> GetUserAsync(string authtoken)
        {
           return await GetUserFromJWTAsync(authtoken);
        }

        /// <inheritdoc/>
        async public Task<User> GetUserByIdAsync(int userId)
        {
            // Return fetched user object from database.
            return await _ctx.Users.Include(u => u.Groups)
                .Include(u => u.Events)
                .Include(u => u.Topics)
                .Include(u => u.Groups)
                .Include(u => u.Posts)
                .Include(u => u.RSVPs)
                .FirstOrDefaultAsync(u => u.Id == userId);
        }

        /// <inheritdoc/>
        async public Task<User> CreateUserAsync(User user)
        {
            // Create a new user object in database.
            _ctx.Users.Add(user);
            await _ctx.SaveChangesAsync();
            return user;
        }

        /// <inheritdoc/>
        async public Task<User> UpdateUserByIdAsync(User user)
        {
            // Clear tracking
            _ctx.ChangeTracker.Clear();
            // Update user object in database
            _ctx.Entry(user).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();
            // Fetch and return updated user
            return await GetUserByIdAsync(user.Id);

        }

        

        /// <inheritdoc/>
       async public Task<User> GetUserFromJWTAsync(string jwt)
       {
            var jwtString = jwt.ToString().Replace("Bearer ", "");
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwtString);
            var subject = token.Subject;
            return await _ctx.Users.FirstOrDefaultAsync(u => u.Subject == subject);
       }
        
        /// <inheritdoc/>
        public bool UserExists(int userId)
        {
            // Check if user can be found in database
            var userExists = _ctx.Users.Any(u => u.Id == userId);
            return userExists;
        }

        ///<inheritdoc/>
        public int GetUserCountAsync()
        {
            var userCount = _ctx.Users.Count();
            return userCount;
        }
    }
}
