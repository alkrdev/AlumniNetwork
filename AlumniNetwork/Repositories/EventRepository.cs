﻿using AlumniNetwork.DbContexts;
using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace AlumniNetwork.Repositories
{
    /// <summary>
    /// Implementation of the IEventRepository containing Entity Framework operations.
    /// </summary>
    public class EventRepository: IEventRepository
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly AlumniDb _ctx;
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventRepository"/> class.
        /// </summary>
        /// <param name="ctx"></param>
        public EventRepository(AlumniDb ctx, IUserRepository userRepository)
        {
            _ctx = ctx;
            _userRepository = userRepository;
        }

        /// <inheritdoc/>
        public async Task<Event> CreateEventAsync(int userId, Event newEvent)
        {
            // Set creation time and last update property to now, ovveriding any value provided in the client.
            newEvent.Created = DateTime.UtcNow;
            newEvent.LastUpdated = DateTime.UtcNow;
            newEvent.StartTime = newEvent.StartTime.ToUniversalTime();
            newEvent.EndTime = newEvent.EndTime.ToUniversalTime();
            // Set the creator of the event to the user with the provided id.
            var creator = _ctx.Users.Where(u => u.Id == userId).FirstOrDefault();
            newEvent.Users = new List<User>();
            newEvent.Users.Add(creator);

            // Add the event to the database.
            await _ctx.Events.AddAsync(newEvent);

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();
            
            // Return the newly created event.
            return newEvent;
        }
        

        /// <inheritdoc/>
        public async Task CreateEventGroupInviteAsync(int eventId, int groupId)
        {
            // Add groupId to the list of invited groups for the event with the provided id.
            _ctx.Events.Include(e => e.Groups).Where(e => e.Id == eventId).FirstOrDefault().Groups.Add(_ctx.Groups.Where(g => g.Id == groupId).FirstOrDefault());

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();
        }


        /// <inheritdoc/>
        public async Task<RSVP> CreateEventRSVPAsync(int eventId, RSVP rsvp)
        {
            // Set the event id of the RSVP to the provided event id.
            rsvp.EventId = eventId;

            // Add the RSVP to the database.
            await _ctx.RSVPs.AddAsync(rsvp);

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();

            // Return the newly created RSVP.
            return rsvp;
        }


        /// <inheritdoc/>
        public async Task CreateEventTopicInviteAsync(int eventId, int topicId)
        {
            // Add topicId to the list of invited topics for the event with the provided id.
            _ctx.Events.Include(e => e.Topics).Where(e => e.Id == eventId).FirstOrDefault().Topics.Add(_ctx.Topics.Where(t => t.Id == topicId).FirstOrDefault());

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();
        }


        /// <inheritdoc/>
        public async Task CreateEventUserInviteAsync(int eventId, int invitedUserId)
        {
            // Add userId to the list of invited users for the event with the provided id.
            _ctx.Events.Where(e => e.Id == eventId).FirstOrDefault().Users.Add(_ctx.Users.Where(u => u.Id == invitedUserId).FirstOrDefault());

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();
        }


        /// <inheritdoc/>
        public async Task DeleteGroupEventInviteAsync(int eventId, int groupId)
        {
            // Remove groupId from the list of invited groups for the event with the provided id.
            _ctx.Events.Include(e => e.Groups).Where(e => e.Id == eventId).FirstOrDefault().Groups.Remove(_ctx.Groups.Where(g => g.Id == groupId).FirstOrDefault());

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();

        }


        /// <inheritdoc/>
        public async Task DeleteTopicEventInviteAsync(int eventId, int topicId)
        {
            // Remove topicId from the list of invited topics for the event with the provided id.
            _ctx.Events.Include(e => e.Topics).Where(e => e.Id == eventId).FirstOrDefault().Topics.Remove(_ctx.Topics.Where(t => t.Id == topicId).FirstOrDefault());

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();
        }


        /// <inheritdoc/>
        public async Task DeleteUserEventInviteAsync(int eventId, int invitedUserId)
        {
            // Remove userId from the list of invited users for the event with the provided id.
            _ctx.Events.Where(e => e.Id == eventId).FirstOrDefault().Users.Remove(_ctx.Users.Where(u => u.Id == invitedUserId).FirstOrDefault());

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();
        }


        /// <inheritdoc/>
        public async Task<List<Event>> GetEventsAsync(int userId, [Optional] string searchQuery, [Optional] int offset, [Optional] int limit)
        {
            // Create a list to hold the search results.
            var searchResults = new List<Event>();
            
            // Add events to search results, if user is a member of any group associated with the event.
            searchResults.AddRange(_ctx.Events.Include(e => e.Groups).Where(e => e.Groups.Any(g => g.Members.Any(m => m.Id == userId))));
            // Add events to search results, if user is a member of any topic associated with the event.
            //searchResults.AddRange(_ctx.Events.Include(e => e.Topics).Where(e => e.Topics.Any(t => t.Members.Any(m => m.Id == userId))));
            // Add events to search results, if user is directly invited to the event.
            searchResults.AddRange(_ctx.Events.Include(e => e.Users).Where(e => e.Users.Any(u => u.Id == userId)));

            // Filter search results with search query, if provided, matching either event body, group name or topic name.
            if (!string.IsNullOrEmpty(searchQuery)) 
            {
                searchQuery = searchQuery.ToLower();
                var filteredResults = new List<Event>();

                filteredResults.AddRange(searchResults.Where( e => e.Body.ToLower().Contains(searchQuery)));

                foreach(Event e in searchResults)
                {
                    if(e.Groups != null)
                    {
                        filteredResults.AddRange(searchResults.Where(e => e.Groups.Any(g => g.Name.ToLower().Contains(searchQuery))));
                    }
                    if(e.Topics != null)
                    {
                        filteredResults.AddRange(searchResults.Where(e => e.Topics.Any(t => t.Name.ToLower().Contains(searchQuery))));
                    }
                }

                searchResults = filteredResults;
            }

            // Order search results by reverse chronological order
            searchResults = searchResults.OrderByDescending(e => e.LastUpdated).ToList();

            // Make sure event list is unique
            searchResults = searchResults.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                searchResults = searchResults.Skip(offset).Take(limit).ToList();
            }

            // Return the search results.
            return searchResults;
        }

        /// <inheritdoc/>
        public async Task<List<Event>> GetEventsbyGroupAsync(int groupId)
        {
            // Create a list to hold the group events.
            var events = new List<Event>();

            // Add events where group matches the groupId
            events.AddRange(_ctx.Events.Include(e => e.Groups).Where(e => e.Groups.Any(g => g.Id == groupId)));

            // Order search results by reverse chronological order
            events = events.OrderByDescending(e => e.LastUpdated).ToList();

            // Make sure event list is unique
            events = events.Distinct().ToList();

            // Return the events.
            return events;
        }

        /// <inheritdoc/>
        public async Task<Event> GetEventByIdAsync(int eventId)
        {
            // Return event with provided Id and include groups
            return await _ctx.Events
                .Include(e => e.Groups).ThenInclude(g => g.Members)
                .Include(e => e.RSVPs).ThenInclude(r => r.User)
                .FirstOrDefaultAsync(e => e.Id == eventId);
        }

        /// <inheritdoc/>
        public async Task<Event> UpdateEventAsync(int eventId, Event updatedEvent)
        {
            // Get the event with the provided id.
            //var eventToUpdate = _ctx.Events.Where(e => e.Id == eventId).FirstOrDefault();

            // Set the last update property to now, ovveriding any value provided in the client.
            //eventToUpdate.LastUpdated = DateTime.UtcNow;
            updatedEvent.LastUpdated = DateTime.UtcNow;

            // Update the event with the provided values.
            _ctx.ChangeTracker.Clear();
            _ctx.Events.Update(updatedEvent);

            // Save the changes to the database.
            await _ctx.SaveChangesAsync();

            // Return the updated event.
            return updatedEvent;
        }

        /// <inheritdoc/>
        public bool CheckUserIsInvited(int userId, int eventId)
        {
            // Check if the user is invited to the event.
            var Event = _ctx.Events.Include(e => e.Users).Where(e => e.Id == eventId).FirstOrDefault();
            if(Event.Users != null)
            {
                return Event.Users.Any(u => u.Id == userId);
            }
            else
            {
                // Event has no users.
                return false;
            }
        }

        /// <inheritdoc/>
        public bool CheckUserIsOwner(int userId, int eventId)
        {
            // Check if the user is the owner of the event by checking if they are first listed user.
            var isOwner = _ctx.Events.Include(e => e.Users).Where(e => e.Id == eventId).FirstOrDefault().Users.First().Id == userId;
            //var isOwner = _ctx.Events.Where(e => e.Id == eventId).FirstOrDefault().Users.First().Id == userId;

            // Return the result.
            return isOwner;
        }

        /// <inheritdoc/>
        public bool CheckUserIsMember(int userId, Event checkEvent){
            var isGroupMember = false;
            var isTopicMember = false;
            if (checkEvent.Groups != null)
            {
                // Check if user belongs to any group in event
                isGroupMember = checkEvent.Groups.Any(g => g.Members.Any(m => m.Id == userId));   
            }
            if (checkEvent.Topics != null)
            {
                // Check if user belongs to any topic in event
                isTopicMember = checkEvent.Topics.Any(t => t.Members.Any(m => m.Id == userId));     
            }
            if (isGroupMember || isTopicMember)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public bool EventExists(int eventId)
        {
            {
                // Check if the event exists.
                var exists = _ctx.Events.Any(e => e.Id == eventId);

                // Return the result.
                return exists;
            }
        }

        /// <inheritdoc/>
        public bool RSVPExists(int eventId, int userId)
        {
            // Check if the RSVP exists.
            var exists = _ctx.RSVPs.Any(r => r.EventId == eventId && r.UserId == userId);

            // Return the result.
            return exists;
        }

        /// <inheritdoc/>
        public async Task<List<Event>> GetEventsForUserAsync(int userId)
        {
            // Get user RSVPs
            var userRSVPs = _ctx.RSVPs.Where(r => r.UserId == userId).ToList();
            // Get list of events from RSVPs
            var events = new List<Event>();
            foreach (RSVP rsvp in userRSVPs)
            {
                // check if event already in list
                if (!events.Any(e => e.Id == rsvp.EventId))
                {
                    // Add event to list
                    events.Add(await GetEventByIdAsync(rsvp.EventId));
                }
            }
            // Return the list of events
            return events;
        }

        /// <inheritdoc/>
        public async Task DeleteEventRSVPAsync(int eventId, int userId)
        {
            // Get the RSVP to delete.
            var rsvpToDelete = _ctx.RSVPs.Where(r => r.EventId == eventId && r.UserId == userId).FirstOrDefault();

            // Delete the RSVP.
            _ctx.RSVPs.Remove(rsvpToDelete);
            await _ctx.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task<List<AttendingUser>> GetAttendingUsersAsync(int eventId)
        {
            // Get the event
            var eventObject = await GetEventByIdAsync(eventId);
            // Get the list of users from RSVPs
            var attendingUsers = new List<AttendingUser>();
            if (eventObject.RSVPs != null)
            {
             foreach (RSVP rsvp in eventObject.RSVPs)
                {
                // Get user object
                var user = await _userRepository.GetUserByIdAsync(rsvp.UserId);
                // Create attending user object
                var attendingUser = new AttendingUser
                {
                    Id = user.Id,
                    Name = user.Name,
                    ProfilePicture = user.ProfileImage,
                };
                
                // Add attending user to list
                attendingUsers.Add(attendingUser);
            }   
            }
            // return list
            return attendingUsers;
        }
    }
}
