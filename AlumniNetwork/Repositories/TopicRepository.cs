﻿using AlumniNetwork.DbContexts;
using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;

namespace AlumniNetwork.Repositories
{
    public class TopicRepository : ITopicRepository
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly AlumniDb _ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="TopicRepository"/> class.
        /// </summary>
        /// <param name="ctx"></param>
        public TopicRepository(AlumniDb ctx)
        {
            _ctx = ctx;
        }

        /// <inheritdoc/>
        public async Task<Topic> CreateTopicAsync(int userId, Topic topic)
        {
            // Prepare empty lists
            topic.Members = new List<User>();
            topic.Events = new List<Event>();

            // Add creating user to membership record.
            topic.Members.Add(_ctx.Users.Where(u => u.Id == userId).FirstOrDefault());
            
            // Add the topic to the database
            await _ctx.Topics.AddAsync(topic);

            // Update the database.
            await _ctx.SaveChangesAsync();

            // Return the topic.
            return topic;
        }
        
        /// <inheritdoc/>
        public async Task<ActionResult> CreateTopicMembershipRecord(int userId, int topicId)
        {
            // Get topic from context by Id
            var topic = _ctx.Topics.Include(t => t.Members).Include(t => t.Events)
                .Where(t => t.Id == topicId).FirstOrDefault();

            topic.Members.Add(_ctx.Users.Where(u => u.Id == userId).FirstOrDefault());

            // Update topic in the database
            _ctx.Topics.Update(topic);
            
            // Update the database
            await _ctx.SaveChangesAsync();

            return new ObjectResult("User is added to membership record.") { StatusCode = 201 };

        }

        /// <inheritdoc/>
        public async Task<Topic> GetTopicByIdAsync(int topicId)
        {
            return await _ctx.Topics
                .Include(t => t.Events)
                .Include(t => t.Members)
                .FirstOrDefaultAsync(t => t.Id == topicId);
        }

        /// <inheritdoc/>
        public async Task<List<Topic>> GetTopicsAsync([Optional] string searchQueryString, [Optional] int offset, [Optional] int limit)
        {
            // Fetch all topics
            var topics = await _ctx.Topics
                .Include(t => t.Events)
                .Include(t => t.Members)
                .ToListAsync();

            // If search query string is not null, filter topics by search query string
            if (searchQueryString != null)
            {
                topics = topics.Where(t => t.Name.Contains(searchQueryString)).ToList();
            }

            // If offset and limit are not null, filter topics by offset and limit
            if (offset != 0)
            {
                topics = topics.Skip(offset).ToList();
            }

            if (limit != 0)
            {
                topics = topics.Take(limit).ToList();
            }

            return topics;
        }

        /// <inheritdoc/>
        public bool CheckUserIsMember(int userId, int topicId)
        {
            // Check if the user is a member of the topic by checking if they are a listed member
            var isMember = _ctx.Topics.Where(t => t.Id == topicId).FirstOrDefault().Members.Any(m => m.Id == userId);

            return isMember;

        }
    }
}
