﻿using AlumniNetwork.DbContexts;
using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Group = AlumniNetwork.Models.Domain.Group;

namespace AlumniNetwork.Repositories
{
    public class PostRepository : IPostRepository
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly AlumniDb _ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="PostRepository"/> class.
        /// </summary>
        /// <param name="ctx"></param>
        public PostRepository(AlumniDb ctx)
        {
            _ctx = ctx;
        }

        /// <inheritdoc/>
        public async Task<Post> CreatePostAsync(int userId, Post newPost)
        {
            // Set the created date
            newPost.Created = DateTime.UtcNow;
            newPost.LastUpdated = DateTime.UtcNow;
            // Add the post to the database
            await _ctx.Posts.AddAsync(newPost);
            // Save changes to database.
            await _ctx.SaveChangesAsync();

            // Return post
            return newPost;
        }

        /// <inheritdoc/>
        public async Task<Post> GetPostByIdAsync(int postId)
        {
            // Get the post from the database
            var post = await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .FirstOrDefaultAsync(p => p.Id == postId);

            // Return post
            return post;
        }

        /// <inheritdoc/>
        public async Task<List<Post>> GetDirectPostsAsync(int userId, [Optional] string searchQueryString, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of posts where user was the direct target
            var posts = await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetUser.Id == userId).ToListAsync();

            // Filter search results with search query if provided, matching the name of the sending user or any target group, topic, or event.
            if (searchQueryString != null)
            {
                posts = posts.Where(p => p.CreatedByUser.Name.Contains(searchQueryString) || p.TargetGroup.Name.Contains(searchQueryString) || p.TargetTopic.Name.Contains(searchQueryString) || p.TargetEvent.Body.Contains(searchQueryString)).ToList();
            }

            // Order posts by last updated.
            posts = posts.OrderByDescending(p => p.LastUpdated).ToList();

            // Remove duplicate posts.
            posts = posts.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                posts = posts.Skip(offset).Take(limit).ToList();
            }

            // Return posts.
            return posts;

        }

        /// <inheritdoc/>
        public async Task<List<Post>> GetDirectPostsByUserAsync(int requestingUserId, int sendingUserId, [Optional] string searchQueryString, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of posts where requesting user was the target of the post and sending speficic sending user by Id.
            var posts = await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetUser.Id == requestingUserId).Where(p => p.CreatedByUser.Id == sendingUserId).ToListAsync();

            // Filter search results with search query if provided, matching the name of the sending user or any target group, topic, or event.
            if (searchQueryString != null)
            {
                posts = posts.Where(p => p.CreatedByUser.Name.Contains(searchQueryString) || p.TargetGroup.Name.Contains(searchQueryString) || p.TargetTopic.Name.Contains(searchQueryString) || p.TargetEvent.Body.Contains(searchQueryString)).ToList();
            }

            // Order posts by last updated.
            posts = posts.OrderByDescending(p => p.LastUpdated).ToList();

            // Remove duplicate posts.
            posts = posts.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                posts = posts.Skip(offset).Take(limit).ToList();
            }

            // Return posts.
            return posts;
        }

        /// <inheritdoc/>
        public async Task<List<Post>> GetPostsByEventAsync(int eventId, [Optional] string searchQueryString, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of posts associated with a specific event by Id.
            var posts = await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetEvent.Id == eventId).ToListAsync();

            // Filter search results with search query if provided, matching the name of the sending user or any target group, topic, or event.
            if (searchQueryString != null)
            {
                posts = posts.Where(p => p.CreatedByUser.Name.Contains(searchQueryString) || p.TargetGroup.Name.Contains(searchQueryString) || p.TargetTopic.Name.Contains(searchQueryString) || p.TargetEvent.Body.Contains(searchQueryString)).ToList();
            }

            // Order posts by last updated.
            posts = posts.OrderByDescending(p => p.LastUpdated).ToList();

            // Remove duplicate posts.
            posts = posts.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                posts = posts.Skip(offset).Take(limit).ToList();
            }

            // Return posts.
            return posts;

        }

        /// <inheritdoc/>
        public async Task<List<Post>> GetPostsByGroupAsync(int groupId, [Optional] string searchQueryString, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of posts associated with a specific group by Id.
            var posts = await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetGroup.Id == groupId).ToListAsync();

            // Filter search results with search query if provided, matching the name of the sending user or any target group, topic, or event.
            if (searchQueryString != null)
            {
                posts = posts.Where(p => p.CreatedByUser.Name.Contains(searchQueryString) || p.TargetGroup.Name.Contains(searchQueryString) || p.TargetTopic.Name.Contains(searchQueryString) || p.TargetEvent.Body.Contains(searchQueryString)).ToList();
            }

            // Order posts by last updated.
            posts = posts.OrderByDescending(p => p.LastUpdated).ToList();

            // Remove duplicate posts.
            posts = posts.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                posts = posts.Skip(offset).Take(limit).ToList();
            }

            // Return posts.
            return posts;
        }

        /// <inheritdoc/>
        public async Task<List<Post>> GetPostsByTopicAsync(int topicId, [Optional] string searchQueryString, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of posts associated with a specific group by Id.
            var posts = await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetTopic.Id == topicId).ToListAsync();

            // Filter search results with search query if provided, matching the name of the sending user or any target group, topic, or event.
            if (searchQueryString != null)
            {
                posts = posts.Where(p => p.CreatedByUser.Name.Contains(searchQueryString) || p.TargetGroup.Name.Contains(searchQueryString) || p.TargetTopic.Name.Contains(searchQueryString) || p.TargetEvent.Body.Contains(searchQueryString)).ToList();
            }

            // Order posts by last updated.
            posts = posts.OrderByDescending(p => p.LastUpdated).ToList();

            // Remove duplicate posts.
            posts = posts.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                posts = posts.Skip(offset).Take(limit).ToList();
            }

            // Return posts.
            return posts;
        }

        /// <inheritdoc/>
        public async Task<List<Post>> GetSubsribedPostsAsync(int userId, [Optional] string searchQuery, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of posts if user is target of post, or a member of a group, event or topic that is
            var posts = new List<Post>();

            // Add posts to search results, if user is a member of any group associated with the post.
            posts.AddRange(await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                    .ThenInclude(g => g.Members)
                .Where(p => p.TargetGroup.Members.Any(m => m.Id == userId)).ToListAsync());
            // Add posts to search results, if user is a member of any topic associated with the post.
            posts.AddRange(await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetTopic.Members.Any(m => m.Id == userId)).ToListAsync());
            // Add posts to search results, if user is directly targeted by the post.
            posts.AddRange(await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.TargetUser.Id == userId).ToListAsync());
            // Add posts to search results, if user is creating user of the post.
            posts.AddRange(await _ctx.Posts
                .Include(p => p.CreatedByUser)
                .Include(p => p.ReplyParent)
                .Include(p => p.TargetUser)
                .Include(p => p.TargetEvent)
                .Include(p => p.TargetGroup)
                .Include(p => p.TargetTopic)
                .Where(p => p.CreatedByUser.Id == userId).ToListAsync());

            // Filter search results with search query if provided, matching the name of the sending user or any target group, topic, or event.
            if (!string.IsNullOrEmpty(searchQuery))
            {
                searchQuery = searchQuery.ToLower();
                var filteredResults = new List<Post>();
                
                filteredResults.AddRange(posts.Where(p => p.Body.ToLower().Contains(searchQuery)));

                filteredResults.AddRange(posts.Where(p => p.CreatedByUser.Name.Contains(searchQuery)));

                foreach (var post in posts)
                {
                    if(post.PostTarget == Target.Group && post.TargetGroup != null)
                    {
                        filteredResults.AddRange(posts.Where(p => p.TargetGroup.Name.ToLower().Contains(searchQuery)));
                    }
                    else if(post.PostTarget == Target.Topic && post.TargetTopic != null)
                    {
                        filteredResults.AddRange(posts.Where(p => p.TargetTopic.Name.ToLower().Contains(searchQuery)));
                    }
                    else if (post.PostTarget == Target.Event && post.TargetEvent != null)
                    {
                        filteredResults.AddRange(posts.Where(p => p.TargetEvent.Body.ToLower().Contains(searchQuery)));
                    }

                }

                posts = filteredResults;
            }

            // Order posts by last updated.
            posts = posts.OrderByDescending(p => p.LastUpdated).ToList();

            // Remove duplicate posts.
            posts = posts.Distinct().ToList();

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                posts = posts.Skip(offset).Take(limit).ToList();
            }




            // Return posts.
            return posts;

        }

        /// <inheritdoc/>
        public async Task<Post> UpdatePostAsync(int userId, int postId, Post updatedPost)
        {          
            // Set the last updated property
            updatedPost.LastUpdated = DateTime.UtcNow;

            // Update the post
            _ctx.Posts.Update(updatedPost);

            // Save changes to database.
            await _ctx.SaveChangesAsync();

            // Return updated post
            return updatedPost;
        }

        /// <inheritdoc/>
        public bool PostExists(int postId)
        {
            // Check if a post exists with the provided Id.
            return _ctx.Posts.Any(p => p.Id == postId);
        }

        /// <inheritdoc/>
        public bool AllowedToPostToTarget(int userId, Post post)
        {
            {
                // Check if user is allowed to post to targets
                if (post.TargetEvent != null)
                {
                    if (post.TargetEvent.Users != null)
                    {
                        // Check if user is a member of the event
                        if (post.TargetEvent.Users.Any(m => m.Id == userId))
                        {
                            return true;
                        }   
                    }
                    
                    if (post.TargetEvent.Groups != null)
                    {
                    // Check if user is a member of any group associated with the event
                        if (post.TargetEvent.Groups.Any(g => g.Members.Any(m => m.Id == userId)))
                        {
                            return true;
                        }
                    }                   
                }
                else if (post.TargetGroup != null)
                {
                    // Check if user is a member of the group
                    if (post.TargetGroup.Members.Any(m => m.Id == userId))
                    {
                        return true;
                    }
                }
                else if (post.TargetTopic != null)
                {
                    // Check if user is a member of the topic
                    if (post.TargetTopic.Members.Any(m => m.Id == userId))
                    {
                        return true;
                    }
                }
                else if (post.TargetUser != null) 
                {
                    // Check if user is the target user
                    return true;

                    // users should be allowed to post to each other.
                    //if (post.TargetUser.Id == userId)
                    //{
                    //    return true;
                    //}
                }
                else
                {
                    // If no target is provided, return false.
                    return false;
                }
                // If none of the above conditions are met, return false.
                return false;
            }
        }
    }
}
