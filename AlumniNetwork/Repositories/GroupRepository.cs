﻿using AlumniNetwork.DbContexts;
using AlumniNetwork.Interfaces;
using AlumniNetwork.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Group = AlumniNetwork.Models.Domain.Group;

namespace AlumniNetwork.Repositories
{
    /// <summary>
    /// Implementation of the IGroupRepository containing Entity Framework operations.
    /// </summary>
    public class GroupRepository : IGroupRepository
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly AlumniDb _ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupRepository"/> class.
        /// </summary>
        /// <param name="ctx"></param>
        public GroupRepository(AlumniDb ctx)
        {
            _ctx = ctx;
        }

        /// <inheritdoc/>
        public async Task<Group> AddGroupMemberAsync(int userId, int groupId)
        {
            var user = await _ctx.Users.FirstOrDefaultAsync(u => u.Id == userId);
            var group = await _ctx.Groups
                .Include(g => g.Members)
                .Include(g => g.Events)
                .FirstOrDefaultAsync(g => g.Id == groupId);

            group.Members.Add(user);

            await _ctx.SaveChangesAsync();
            return group;
        }

        /// <inheritdoc/>
        public async Task<Group> CreateGroupAsync(int userId, Group newGroup)
        {
            newGroup.Members = new List<User>();
            newGroup.Members.Add(_ctx.Users.FirstOrDefault(u => u.Id == userId));
            _ctx.Groups.Add(newGroup);

            await _ctx.SaveChangesAsync();
            return newGroup;
        }

        /// <inheritdoc/>
        public async Task<Group> GetGroupByIdAsync(int groupId)
        {
            return await _ctx.Groups
                .Include(g => g.Members)
                .FirstOrDefaultAsync(g => g.Id == groupId);
        }

        /// <inheritdoc/>
        public async Task<List<Group>> GetGroupsAsync(int userId, [Optional] string searchQuery, [Optional] int offset, [Optional] int limit)
        {
            // Get a list of groups that are not private.
            var groups = await _ctx.Groups
                .Include(g => g.Members)
                .Include(g => g.Events)
                .Where(g => g.IsPrivate == false).ToListAsync();

            // Add private groups to list if user is a member.
            groups.AddRange(await _ctx.Groups.Include(g => g.Members).Where(g => g.IsPrivate == true && g.Members.Any(m => m.Id == userId)).ToListAsync());

            // Filter groups by name, description, event names, or member names based on searchQuery
            if (!string.IsNullOrEmpty(searchQuery))
            {
                groups = groups.Where(g => g.Name.Contains(searchQuery) || g.Description.Contains(searchQuery) || g.Events.Any(e => e.Body.Contains(searchQuery)) || g.Members.Any(m => m.Name.Contains(searchQuery))).ToList();
            }

            // Paginate search results, if offset and limit are provided.
            if (offset > 0 && limit > 0)
            {
                groups = groups.Skip(offset).Take(limit).ToList();
            }
            return groups;
        }

        public async Task<Group> RemoveGroupMemberAsync(int userId, int groupId)
        {
            {
                var user = await _ctx.Users.FirstOrDefaultAsync(u => u.Id == userId);
                var group = await _ctx.Groups
                    .Include(g => g.Members)
                    .Include(g => g.Events)
                    .FirstOrDefaultAsync(g => g.Id == groupId);

                group.Members.Remove(user);

                await _ctx.SaveChangesAsync();
                return group;
            }
        }

        /// <inheritdoc/>
        bool IGroupRepository.CheckUserIsMember(int userId, int groupId)
        {
            // Check if the user is a member of the group by checking if they are a listed member
            var isMember = _ctx.Groups.Include(g => g.Members).Where(g => g.Id == groupId).FirstOrDefault().Members.Any(m => m.Id == userId);

            return isMember;

        }

        /// <inheritdoc/>
        bool IGroupRepository.CheckUserIsOwner(int userId, int groupId)
        {
            // Check if the user is the owner of the event by checking if they are first listed member.
            var isOwner = _ctx.Groups.Where(g => g.Id == groupId).FirstOrDefault().Members.First().Id == userId;

            return isOwner;
        }

        /// <inheritdoc/>
        public bool GroupExists(int groupId)
        {
            {
                // Check if the event exists.
                var exists = _ctx.Groups.Any(g => g.Id == groupId);

                // Return the result.
                return exists;
            }
        }
    }
}
