﻿using AlumniNetwork.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace AlumniNetwork.DbContexts
{
    /// <summary>
    /// Database context for the Alumni Network application.
    /// </summary>
    public class AlumniDb : DbContext
    {
        public AlumniDb(DbContextOptions<AlumniDb> options) : base(options)
        {
            //If 'builder.Services.AddDbContext' is used, then also ensure that DbContext type accepts a DbContextOptions<TContext>
            //object in its constructor and passes it to the base constructor for DbContext.
        }
        public DbSet<Event> Events { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<RSVP> RSVPs { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // Define many-to-many joining table names.
            modelBuilder.Entity<Group>()
                .HasMany(left => left.Members)
                .WithMany(right => right.Groups)
                .UsingEntity(join => join.ToTable("GroupMember"));

            modelBuilder.Entity<Topic>()
                .HasMany(left => left.Members)
                .WithMany(right => right.Topics)
                .UsingEntity(join => join.ToTable("TopicMember"));

            modelBuilder.Entity<Event>()
                .HasMany(left => left.Groups)
                .WithMany(right => right.Events)
                .UsingEntity(join => join.ToTable("EventGroupInvite"));

            modelBuilder.Entity<Event>()
                .HasMany(left => left.Users)
                .WithMany(right => right.Events)
                .UsingEntity(join => join.ToTable("EventUserInvite"));

            modelBuilder.Entity<Event>()
                .HasMany(left => left.Topics)
                .WithMany(right => right.Events)
                .UsingEntity(join => join.ToTable("EventTopicInvite"));


            // Add User seed data
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "Joey Tribbiani", FunFact = "Does not share food!", Status = "Is eating a whole turkey", Subject = "ebdcca85-9ece-4e10-90a5-584fc09449ec", ProfileImage = "https://i.imgur.com/Hd8FGCy.jpeg" },
                new User { Id = 2, Name = "Ross Geller", FunFact = "Has been married 3 times", Status = "Teaching a paleontology class", Subject = "46d8795e-9f5a-4877-b97b-a884bdcb4597", ProfileImage = "https://i.imgur.com/F7WpuUd.jpeg" },
                new User { Id = 3, Name = "Monica Geller", FunFact = "Is ultra-competitive", Status = "Cleaning my appartment", Subject = "e7c80f0c-b535-4774-b399-c9f947be4bc8", ProfileImage = "https://i.imgur.com/9UH4g7K.jpeg" },
                new User { Id = 4, Name = "Chandler Bing", FunFact = "I make jokes when i'm uncomfortable", Status = "Looking for my Duck, have you seen it?", Subject = "2096b784-b62f-4ec5-b40e-86cbdd72c883", ProfileImage = "https://i.imgur.com/WQizcSs.jpeg" },
                new User { Id = 5, Name = "Rachel Green", FunFact = "Is afraid of fish", Status = "Working at the Central Perk coffee shop", Subject = "b842ea46-b650-4939-bb37-29fc34c6a67d", ProfileImage = "https://i.imgur.com/AJPYRF6.jpeg" },
                new User { Id = 6, Name = "Phoebe Buffay", FunFact = "Was once Homeless", Status = "Playing my guitar in the subway", Subject = "42bedda6-836e-4201-90a7-88a4efc4f332", ProfileImage = "https://i.imgur.com/ChIyiSH.jpeg" }
            );

            // Add Group seed data
            modelBuilder.Entity<Group>().HasData(
                new Group { Id = 1, Name = "Beer Brewing", Description = "A group for everyone who loves beer" },
                new Group { Id = 2, Name = "Math Study Group", Description = "A group focused on studying Mathematics" },
                new Group { Id = 3, Name = "Car Enthusiasts", Description = "A group for Car Enthusiast" },
                new Group { Id = 4, Name = "The Private Group", Description = "A Private group that requires an invitation", IsPrivate = true },
                new Group { Id = 5, Name = "F.R.I.E.N.D.S", Description = "A group for all the F.R.I.E.N.D.S" }
            );

            // Add Topic seed data
            modelBuilder.Entity<Topic>().HasData(
                new { Id = 1, Name = "Beer", Description = "All things to do with Beer" },
                new { Id = 2, Name = "Drinks and booze", Description = "Great drinks and booze talk" },
                new { Id = 3, Name = "Math", Description = "All things to do with Math" },
                new { Id = 4, Name = "Cars", Description = "All things to do with Cars" },
                new { Id = 5, Name = "Cafés and Restaurants", Description = "Lets talk about the best Coffee and delicious food." },
                new { Id = 6, Name = "Socal Activities", Description = "Socal Activities and meetups." },
                new { Id = 7, Name = "Cake", Description = "We love cake" },
                new { Id = 8, Name = "Birthdays", Description = "Time to celebrate another year!" }

            );

            // Add Event seed data
            modelBuilder.Entity<Event>().HasData(
                new Event { Id = 1, Body = "Beer brewing contest, who can make the best Stout or IPA?", StartTime = new DateTime(2022, 11, 5, 12, 30, 0).ToUniversalTime(), EndTime = new DateTime(2022, 11, 5, 20, 00, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 21, 10, 0, 0).ToUniversalTime(), LastUpdated = new DateTime(2022, 10, 21, 11, 0, 0).ToUniversalTime() },
                new Event { Id = 2, Body = "Learn how to make beer at home. Simply prepare, brew, ferment, bottle, and enjoy!", StartTime = new DateTime(2022, 11, 1, 10, 0, 0).ToUniversalTime(), EndTime = new DateTime(2022, 11, 1, 16, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime(), LastUpdated = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime() },
                new Event { Id = 3, Body = "Meet up at Central Perk coffee shop", StartTime = new DateTime(2022, 11, 1, 10, 0, 0).ToUniversalTime(), EndTime = new DateTime(2022, 11, 1, 16, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime(), LastUpdated = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime() },
                new Event { Id = 4, Body = "Joeys Birthday party!", StartTime = new DateTime(2022, 11, 4, 18, 0, 0).ToUniversalTime(), EndTime = new DateTime(2022, 11, 4, 22, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime(), LastUpdated = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime() },
                new Event { Id = 5, Body = "Preparing cake for Joeys Birthday party!", StartTime = new DateTime(2022, 11, 3, 13, 0, 0).ToUniversalTime(), EndTime = new DateTime(2022, 11, 3, 16, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime(), LastUpdated = new DateTime(2022, 10, 22, 9, 0, 0).ToUniversalTime() }
            );

            // Add Event Members
            modelBuilder.Entity<Event>()
                .HasMany(left => left.Users)
                .WithMany(right => right.Events)
                .UsingEntity(join => join.HasData(
                    new { EventsId = 1, UsersId = 1 },
                    new { EventsId = 1, UsersId = 2 },
                    new { EventsId = 1, UsersId = 4 },
                    new { EventsId = 2, UsersId = 1 },
                    new { EventsId = 2, UsersId = 2 },
                    new { EventsId = 2, UsersId = 4 }
            ));

            // Add Event Topic Invites
            modelBuilder.Entity<Event>()
                .HasMany(left => left.Topics)
                .WithMany(right => right.Events)
                .UsingEntity(join => join.HasData(
                    new { EventsId = 1, TopicsId = 1 },
                    new { EventsId = 1, TopicsId = 2 },
                    new { EventsId = 1, TopicsId = 6 },
                    new { EventsId = 2, TopicsId = 1 },
                    new { EventsId = 2, TopicsId = 2 },
                    new { EventsId = 2, TopicsId = 6 },
                    new { EventsId = 4, TopicsId = 8 },
                    new { EventsId = 5, TopicsId = 7 }

            ));

            // Add Event Group Invites
            modelBuilder.Entity<Event>()
                .HasMany(left => left.Groups)
                .WithMany(right => right.Events)
                .UsingEntity(join => join.HasData(
                    new { EventsId = 1, GroupsId = 1 },
                    new { EventsId = 2, GroupsId = 1 },
                    new { EventsId = 3, GroupsId = 5 },
                    new { EventsId = 4, GroupsId = 5 },
                    new { EventsId = 5, GroupsId = 5 }
            ));


            // Add Group Members
            modelBuilder.Entity<Group>()
                .HasMany(left => left.Members)
                .WithMany(right => right.Groups)
                .UsingEntity(join => join.HasData(
                    new { GroupsId = 1, MembersId = 1 },
                    new { GroupsId = 1, MembersId = 2 },
                    new { GroupsId = 2, MembersId = 3 },
                    new { GroupsId = 3, MembersId = 1 },
                    new { GroupsId = 3, MembersId = 4 },
                    new { GroupsId = 4, MembersId = 3 },
                    new { GroupsId = 4, MembersId = 4 },
                    new { GroupsId = 5, MembersId = 1 },
                    new { GroupsId = 5, MembersId = 2 },
                    new { GroupsId = 5, MembersId = 3 },
                    new { GroupsId = 5, MembersId = 4 },
                    new { GroupsId = 5, MembersId = 5 },
                    new { GroupsId = 5, MembersId = 6 }
            ));

            // Add Topic Members
            modelBuilder.Entity<Topic>()
                .HasMany(left => left.Members)
                .WithMany(right => right.Topics)
                .UsingEntity(join => join.HasData(
                    new { TopicsId = 1, MembersId = 1 },
                    new { TopicsId = 1, MembersId = 2 },
                    new { TopicsId = 1, MembersId = 4 },
                    new { TopicsId = 4, MembersId = 5 },
                    new { TopicsId = 3, MembersId = 2 },
                    new { TopicsId = 3, MembersId = 4 },
                    new { TopicsId = 6, MembersId = 1 },
                    new { TopicsId = 6, MembersId = 2 },
                    new { TopicsId = 6, MembersId = 3 },
                    new { TopicsId = 6, MembersId = 4 },
                    new { TopicsId = 6, MembersId = 5 },
                    new { TopicsId = 6, MembersId = 6 },
                    new { TopicsId = 8, MembersId = 1 },
                    new { TopicsId = 8, MembersId = 2 },
                    new { TopicsId = 8, MembersId = 3 },
                    new { TopicsId = 8, MembersId = 4 },
                    new { TopicsId = 8, MembersId = 5 },
                    new { TopicsId = 8, MembersId = 6 }
            ));


            // Add Post seed data
            modelBuilder.Entity<Post>().HasData(
                // Debug Posts
                new { Id = 1, Body = "This is the first post, targeted a user", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 2, Body = "This is the Second post, targeted a group", PostTarget = Target.Group, TargetGroupId = 2, LastUpdated = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 3, Body = "This is the Fourth post, targeted a topic ", PostTarget = Target.Topic, TargetTopicId = 1, LastUpdated = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 4, Body = "This is the Third post, targeted a event", PostTarget = Target.Event, TargetEventId = 1, LastUpdated = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 1, 12, 0, 0).ToUniversalTime(), CreatedByUserId = 1 },
                
                // User to User posts
                new { Id = 5, Body = "Im hungover, great party last night!", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 2, 6, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 2, 6, 0, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 6, Body = "Hey Ross, I ate your leftover sandwitch, hope you don't mind? It was really good.", PostTarget = Target.User, TargetUserId = 2, LastUpdated = new DateTime(2022, 10, 2, 6, 1, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 2, 6, 1, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 7, Body = "Hi Monica, How You Doin'?", PostTarget = Target.User, TargetUserId = 3, LastUpdated = new DateTime(2022, 10, 2, 6, 2, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 2, 6, 2, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 8, Body = "Hey Chandler bing bong, have you seen our duck? I think he ran out the hallway.", PostTarget = Target.User, TargetUserId = 4, LastUpdated = new DateTime(2022, 10, 2, 6, 3, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 2, 6, 3, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 9, Body = "Hi Rachel, How You Doin'?", PostTarget = Target.User, TargetUserId = 5, LastUpdated = new DateTime(2022, 10, 2, 6, 4, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 2, 6, 4, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 10, Body = "Hey Phoebe, How You Doin'?", PostTarget = Target.User, TargetUserId = 6, LastUpdated = new DateTime(2022, 10, 2, 6, 5, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 2, 6, 5, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 11, Body = "You ate my sandwitch? MYYY SANDWITCH!!!", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 3, 6, 1, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 3, 6, 1, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 12, Body = "Just finished my Lecture for the day! Going to the Central Perk coffee shop", PostTarget = Target.User, TargetUserId = 2, LastUpdated = new DateTime(2022, 10, 3, 6, 2, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 3, 6, 2, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 13, Body = "Hi sister, meet me at the coffee shop", PostTarget = Target.User, TargetUserId = 3, LastUpdated = new DateTime(2022, 10, 3, 6, 2, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 3, 6, 2, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 14, Body = "Yo Chandler, still working on your W.E.N.U.S?", PostTarget = Target.User, TargetUserId = 4, LastUpdated = new DateTime(2022, 10, 3, 6, 3, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 3, 6, 3, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 15, Body = "Rachel call me back!", PostTarget = Target.User, TargetUserId = 5, LastUpdated = new DateTime(2022, 10, 3, 6, 4, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 3, 6, 4, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 16, Body = "Hey have you seen Joey today?", PostTarget = Target.User, TargetUserId = 6, LastUpdated = new DateTime(2022, 10, 3, 6, 5, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 3, 6, 5, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 17, Body = "Joey did you eat my homemade cookies?", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 4, 22, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 0, 0).ToUniversalTime(), CreatedByUserId = 3 },
                new { Id = 18, Body = "Ross you are late for our family dinner!", PostTarget = Target.User, TargetUserId = 2, LastUpdated = new DateTime(2022, 10, 4, 22, 1, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 1, 0).ToUniversalTime(), CreatedByUserId = 3 },
                new { Id = 19, Body = "Note to self. don't leave cookies where Joey can get them", PostTarget = Target.User, TargetUserId = 3, LastUpdated = new DateTime(2022, 10, 4, 22, 2, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 2, 0).ToUniversalTime(), CreatedByUserId = 3 },
                new { Id = 20, Body = "Chandler you better not start making jokes when, we get to my parrent house!", PostTarget = Target.User, TargetUserId = 4, LastUpdated = new DateTime(2022, 10, 4, 22, 3, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 3, 0).ToUniversalTime(), CreatedByUserId = 3 },
                new { Id = 21, Body = "Hey Rachel can I borrow a dress? I need it for that Birthday party.", PostTarget = Target.User, TargetUserId = 5, LastUpdated = new DateTime(2022, 10, 4, 22, 4, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 4, 0).ToUniversalTime(), CreatedByUserId = 3 },
                new { Id = 22, Body = "Hey Phoebe what are you up to?", PostTarget = Target.User, TargetUserId = 6, LastUpdated = new DateTime(2022, 10, 4, 22, 5, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 5, 0).ToUniversalTime(), CreatedByUserId = 3 },
                new { Id = 23, Body = "Hey Joey, no I haven't seen Duck Jr.?", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 4, 22, 6, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 6, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 24, Body = "Yeah Still working, boss needs to give me a raise!", PostTarget = Target.User, TargetUserId = 2, LastUpdated = new DateTime(2022, 10, 4, 22, 7, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 7, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 25, Body = "Yeah, I am going to be a late for dinner. Could I BE' working any more?", PostTarget = Target.User, TargetUserId = 3, LastUpdated = new DateTime(2022, 10, 4, 22, 8, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 8, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 26, Body = "Ohh Jolly time, I am stuck at work. The W.E.N.U.S is not working!", PostTarget = Target.User, TargetUserId = 4, LastUpdated = new DateTime(2022, 10, 4, 22, 9, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 9, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 27, Body = "Hey Rachel, wanna hear a joke? Knock knock..", PostTarget = Target.User, TargetUserId = 5, LastUpdated = new DateTime(2022, 10, 4, 22, 10, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 10, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 28, Body = "Phoebe, why did the chicken cross the road?", PostTarget = Target.User, TargetUserId = 6, LastUpdated = new DateTime(2022, 10, 4, 22, 11, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 4, 22, 11, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 29, Body = "Joey can you help me hang a few lamps?", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 15, 15, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 0, 0).ToUniversalTime(), CreatedByUserId = 5 },
                new { Id = 30, Body = "Ross I tried to call you, but your phone is going to straight voice!", PostTarget = Target.User, TargetUserId = 2, LastUpdated = new DateTime(2022, 10, 15, 15, 1, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 1, 0).ToUniversalTime(), CreatedByUserId = 5 },
                new { Id = 31, Body = "I am going shopping tomorrow, wanna go?", PostTarget = Target.User, TargetUserId = 3, LastUpdated = new DateTime(2022, 10, 15, 15, 2, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 2, 0).ToUniversalTime(), CreatedByUserId = 5 },
                new { Id = 32, Body = "Chandler you must help me plan a surprise party for Phoebe!", PostTarget = Target.User, TargetUserId = 4, LastUpdated = new DateTime(2022, 10, 15, 15, 3, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 3, 0).ToUniversalTime(), CreatedByUserId = 5 },
                new { Id = 33, Body = "How does this work?", PostTarget = Target.User, TargetUserId = 5, LastUpdated = new DateTime(2022, 10, 15, 15, 4, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 4, 0).ToUniversalTime(), CreatedByUserId = 5 },
                new { Id = 34, Body = "Phoebe are you still dating that Astronaut?", PostTarget = Target.User, TargetUserId = 6, LastUpdated = new DateTime(2022, 10, 15, 15, 5, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 5, 0).ToUniversalTime(), CreatedByUserId = 5 },
                new { Id = 35, Body = "Have your keys!", PostTarget = Target.User, TargetUserId = 1, LastUpdated = new DateTime(2022, 10, 15, 15, 6, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 6, 0).ToUniversalTime(), CreatedByUserId = 6 },
                new { Id = 36, Body = "I need to borrow money for a new Guitar!", PostTarget = Target.User, TargetUserId = 2, LastUpdated = new DateTime(2022, 10, 15, 15, 7, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 7, 0).ToUniversalTime(), CreatedByUserId = 6 },
                new { Id = 37, Body = "Monica what was the name of that movie again?", PostTarget = Target.User, TargetUserId = 3, LastUpdated = new DateTime(2022, 10, 15, 15, 8, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 8, 0).ToUniversalTime(), CreatedByUserId = 6 },
                new { Id = 38, Body = "I hope you like my new song! Smelly cat!", PostTarget = Target.User, TargetUserId = 4, LastUpdated = new DateTime(2022, 10, 15, 15, 9, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 9, 0).ToUniversalTime(), CreatedByUserId = 6 },
                new { Id = 39, Body = "I have a interesting story for you, see you in 10!", PostTarget = Target.User, TargetUserId = 5, LastUpdated = new DateTime(2022, 10, 15, 15, 10, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 10, 0).ToUniversalTime(), CreatedByUserId = 6 },
                new { Id = 40, Body = "Smelly cat smelly cat what are they feeding you!", PostTarget = Target.User, TargetUserId = 6, LastUpdated = new DateTime(2022, 10, 15, 15, 11, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 15, 15, 11, 0).ToUniversalTime(), CreatedByUserId = 6 },


                // User to Group Posts
                new { Id = 41, Body = "This new Stout called DarkMatter is awesome!", PostTarget = Target.Group, TargetGroupId = 1, LastUpdated = new DateTime(2022, 10, 12, 0, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 0, 0, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 42, Body = "I Really like a Royal brew!", PostTarget = Target.Group, TargetGroupId = 1, LastUpdated = new DateTime(2022, 10, 12, 1, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 1, 0, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 43, Body = "There is a discount on Budlight at Costco!", PostTarget = Target.Group, TargetGroupId = 1, LastUpdated = new DateTime(2022, 10, 12, 2, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 2, 0, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 44, Body = "Hey friends, have anyone seen my keys?, I can't find them", PostTarget = Target.Group, TargetGroupId = 5, LastUpdated = new DateTime(2022, 10, 12, 3, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 3, 0, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 45, Body = "Anyone wanna go grab a Sandwitch?", PostTarget = Target.Group, TargetGroupId = 5, LastUpdated = new DateTime(2022, 10, 12, 4, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 4, 0, 0).ToUniversalTime(), CreatedByUserId = 2 },
                new { Id = 46, Body = "Who's ready to paaarty, tonight?", PostTarget = Target.Group, TargetGroupId = 5, LastUpdated = new DateTime(2022, 10, 12, 5, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 5, 0, 0).ToUniversalTime(), CreatedByUserId = 4 },

                // User to Event Posts
                new { Id = 47, Body = "Who Buys a gift for Joey?", PostTarget = Target.Event, TargetEventId = 4, LastUpdated = new DateTime(2022, 10, 12, 1, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 1, 0, 0).ToUniversalTime(), CreatedByUserId = 6 },
                new { Id = 48, Body = "I can't I got to work", PostTarget = Target.Event, TargetEventId = 4, LastUpdated = new DateTime(2022, 10, 12, 1, 2, 0).ToUniversalTime(), Created = new DateTime(2022, 12, 10, 2, 0, 0).ToUniversalTime(), CreatedByUserId = 4 },
                new { Id = 49, Body = "I will buy the gift! No Fuss!", PostTarget = Target.Event, TargetEventId = 4, LastUpdated = new DateTime(2022, 10, 12, 3, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 3, 0, 0).ToUniversalTime(), CreatedByUserId = 5 },

                // User to Topic Posts
                new { Id = 50, Body = "Ford is a crappy car!", PostTarget = Target.Topic, TargetTopicId = 4, LastUpdated = new DateTime(2022, 12, 10, 13, 0, 0).ToUniversalTime(), Created = new DateTime(2022, 10, 12, 13, 0, 0).ToUniversalTime(), CreatedByUserId = 1 },
                new { Id = 51, Body = "Not a Mustang!", PostTarget = Target.Topic, TargetTopicId = 4, LastUpdated = new DateTime(2022, 10, 12, 1, 14, 0).ToUniversalTime(), Created = new DateTime(2022, 12, 10, 14, 0, 0).ToUniversalTime(), CreatedByUserId = 2 }

            );
        }
    }
}

    
