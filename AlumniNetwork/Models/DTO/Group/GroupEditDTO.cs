﻿namespace AlumniNetwork.Models.DTO.Group
{
    public class GroupEditDTO
    {
        /// <summary>
        /// The name of the group.
        /// </summary>
        /// <example>Noroff 2022 Fall Team Jylland</example>
        public string Name { get; set; }

        /// <summary>
        /// The description of the group.
        /// </summary>
        /// <example>The Jylland Danish Experis Academy Noroff Academy 2022 Fall Group.</example>
        public string Description { get; set; }

        /// <summary>
        /// The image of the group.
        /// </summary>
        /// <example>https://www.visitnordic.com/images/2020/Nordjylland/Gammel_Skagen_Mette_Johnsen-medium.jpg</example>
        public string GroupImage {get; set; }
        
        /// <summary>
        /// The privacy setting of the group.
        /// </summary>
        /// <example>0</example>
        public bool IsPrivate { get; set; }
    }
}
