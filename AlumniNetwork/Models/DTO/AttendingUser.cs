﻿namespace AlumniNetwork.Models.DTO
{
    public class AttendingUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? ProfilePicture { get; set; }
    }
}
