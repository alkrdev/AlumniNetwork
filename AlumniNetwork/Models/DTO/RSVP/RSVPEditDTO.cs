﻿using Microsoft.Build.Framework;

namespace AlumniNetwork.Models.DTO.RSVP
{
    public class RSVPEditDTO
    {
        /// <summary>
        /// The unique identifier for the event the RSVP is for
        /// </summary>
        /// <example>1</example>
        [Required]
        public int Event { get; set; }
        /// <summary>
        /// The unique identifier for the user the RSVP is for
        /// </summary>
        /// <example>1</example>
        [Required]
        public int User { get; set; }
    }
}
