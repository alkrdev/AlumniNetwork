﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetwork.Models.DTO.RSVP
{
    public class RSVPReadDTO
    {
        /// <summary>
        /// The unique identifier for the RSVP.
        /// </summary>
        /// <example>1</example>
        public int Id { get; set; }
        /// <summary>
        /// The quest count of the RSVP.
        /// </summary>
        /// <example>52</example>
        public int GuestCount { get; set; }
        /// <summary>
        /// The date and time of the last update
        /// </summary>
        public DateTime LastUpdated { get; set; }
        /// <summary>
        /// The unique identifier for the event the RSVP is for
        /// </summary>
        /// <example>1</example>
        public int Event { get; set; }
        /// <summary>
        /// The unique identifier for the user the RSVP is for
        /// </summary>
        /// <example>1</example>
        public int User { get; set; }
    }
}
