﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.Topic
{
    public class TopicReadDTO
    {
        /// <summary>
        /// The unique identifier for the topic.
        /// </summary>
        /// <example>1</example>
        public int Id { get; set; }

        /// <summary>
        /// The name of the topic.
        /// </summary>
        /// <example>Cars</example>
        public string Name { get; set; }

        /// <summary>
        /// Description of the topic.
        /// </summary>
        /// <example>All about cars.</example>
        public string Description { get; set; }

        /// <summary>
        /// A list of unique identifiers for members of the topic.
        /// </summary>
        public List<int> Members { get; set; }
        /// <summary>
        /// A list  of unique identifiers for events for the topic.
        /// </summary>
        public List<int> Events { get; set; }
    }
}
