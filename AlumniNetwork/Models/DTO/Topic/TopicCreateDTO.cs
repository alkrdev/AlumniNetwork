﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.Topic
{
    public class TopicCreateDTO
    {
        /// <summary>
        /// The name of the topic.
        /// </summary>
        /// <example>Cars</example>
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        /// <summary>
        /// Description of the topic.
        /// </summary>
        /// <example>All about cars.</example>
        [Required]
        [MaxLength(250)]
        public string Description { get; set; }
    }
}
