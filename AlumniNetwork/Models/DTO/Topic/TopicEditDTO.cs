﻿namespace AlumniNetwork.Models.DTO.Topic
{
    public class TopicEditDTO
    {
        /// <summary>
        /// The name of the topic.
        /// </summary>
        /// <example>Cars</example>
        public string Name { get; set; }

        /// <summary>
        /// Description of the topic.
        /// </summary>
        /// <example>All about cars.</example>
        public string Description { get; set; }
    }
}
