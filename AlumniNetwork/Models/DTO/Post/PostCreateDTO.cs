﻿using AlumniNetwork.Models.Domain;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.Post
{
    public class PostCreateDTO
    {

        /// <summary>
        /// The body of the post.
        /// </summary>
        /// <example>Hello, how are you?</example>
        [Required]
        [MaxLength(500)]
        public string Body { get; set; }
        /// <summary>
        /// The target of the post.
        /// </summary>
        [Required]
        public Target PostTarget { get; set; }
        
        /// <summary>
        /// The unique identifier for the post that this post is a reply to.
        /// </summary>
        /// <example>0</example>
        public int ReplyParent { get; set; }

        /// <summary>
        /// The unique identifier for the user that this post is a reply to.
        /// </summary>
        /// <example>0</example>
        public int TargetUser { get; set; }

        /// <summary>
        /// The unique identifier for the event that this post is a reply to.
        /// </summary>
        /// <example>0</example>
        public int TargetEvent { get; set; }

        /// <summary>
        /// The unique identifier for the group that this post is a reply to.
        /// </summary>
        /// <example>0</example>
        public int TargetGroup { get; set; }

        /// <summary>
        /// The unique identifier for the topic that this post is a reply to.
        /// </summary>
        /// <example>0</example>
        public int TargetTopic { get; set; }
    }
}
