﻿using AlumniNetwork.Models.Domain;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.Post
{
    public class PostEditDTO
    {

        /// <summary>
        /// The body of the post.
        /// </summary>
        /// <example>Hello, how are you?</example>
        [Required]
        [MaxLength(500)]
        public string Body { get; set; }
    }
}
