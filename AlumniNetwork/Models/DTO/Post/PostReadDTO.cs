﻿using AlumniNetwork.Models.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;

namespace AlumniNetwork.Models.DTO.Post
{
    public class PostReadDTO
    {
        /// <summary>
        /// The unique identifier for the post.
        /// </summary>
        /// <example>1</example>
        public int Id { get; set; }

        /// <summary>
        /// The body of the post.
        /// </summary>
        /// <example>Hello, how are you?</example>
        public string Body { get; set; }
        /// <summary>
        /// The target of the post.
        /// </summary>
        public Target PostTarget { get; set; }
        
        /// <summary>
        /// The date and time the post was created.
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// The date and time the post was last updated.
        /// </summary>
        public DateTime LastUpdated { get; set; }

        /// <summary>
        /// The unique identifier for the user who created the post.
        /// </summary>
        /// <example>1</example>
        public int CreatedByUser { get; set; }

        public string? CreatedByUserName {get; set; }

        public string? CreatedByUserImage {get; set; }
        
        /// <summary>
        /// The unique identifier for the post that this post is a reply to.
        /// </summary>
        /// <example>1</example>
        public int? ReplyParent { get; set; }

        /// <summary>
        /// The unique identifier for the user that this post is a reply to.
        /// </summary>
        /// <example>1</example>
        public int? TargetUser { get; set; }

        /// <summary>
        /// The name of the user that this post is a reply to.
        /// </summary>
        public string? TargetUserName {get; set;}

        /// <summary>
        /// The unique identifier for the event that this post is a reply to.
        /// </summary>
        /// <example>1</example>
        public int? TargetEvent { get; set; }

        /// <summary>
        /// The name of the event that this post is a reply to.
        /// </summary>
        public string? TargetEventName {get; set;}

        /// <summary>
        /// The unique identifier for the group that this post is a reply to.
        /// </summary>
        /// <example>1</example>
        public int? TargetGroup { get; set; }

        /// <summary>
        /// The name of the group that this post is a reply to.
        /// </summary>
        public string? TargetGroupName {get; set;}

        /// <summary>
        /// The unique identifier for the topic that this post is a reply to.
        /// </summary>
        /// <example>1</example>
        public int? TargetTopic { get; set; }

        /// <summary>
        /// The name of the topic that this post is a reply to.
        /// </summary>
        public string? TargetTopicName {get; set;}
    }
}
