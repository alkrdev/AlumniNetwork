﻿namespace AlumniNetwork.Models.DTO.User
{
    public class UserEditDTO
    {       
        /// <summary>
        /// The name of the user.
        /// </summary>
        /// <example>Chandler Bing</example>
        public string Name { get; set; }
        
        /// <summary>
        /// The URL of the profile image of the user.
        /// </summary>
        /// <example>https://static.wikia.nocookie.net/friends/images/2/2f/Chandler.png</example>
        public string ProfileImage { get; set; }

        /// <summary>
        /// The status of the user.
        /// </summary>
        /// <example>Sarcastic as usual</example>
        public string? Status { get; set; }

        /// <summary>
        /// A fun fact about the user.
        /// </summary>
        /// <example>I am the highest paid friend.</example>
        public string? FunFact { get; set; }
    }
}
