﻿using AlumniNetwork.Models.Domain;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.User
{
    public class UserReadDTO
    {
        /// <summary>
        /// The unique identifier for the user.
        /// </summary>
        /// <example>1</example>
        public int Id { get; set; }

        /// <summary>
        /// The name of the user.
        /// </summary>
        /// <example>Chandler Bing</example>
        public string Name { get; set; }
        
        /// <summary>
        /// The URL of the profile image of the user.
        /// </summary>
        /// <example>https://static.wikia.nocookie.net/friends/images/2/2f/Chandler.png</example>
        public string ProfileImage { get; set; }

        /// <summary>
        /// The status of the user.
        /// </summary>
        /// <example>Sarcastic as usual</example>
        public string? Status { get; set; }

        /// <summary>
        /// A fun fact about the user.
        /// </summary>
        /// <example>I am the highest paid friend.</example>
        public string? FunFact { get; set; }

        /// <summary>
        /// A list of unigue identifiers for events related to the user.
        /// </summary>
        public List<int> Events { get; set; }
        
        /// <summary>
        /// A list of unigue identifiers for groups related to the user.
        /// </summary>
        public List<int> Groups { get; set; }
        
        /// <summary>
        /// A list of unigue identifiers for topics related to the user.
        /// </summary>
        public List<int> Topics { get; set; }
        
        /// <summary>
        /// A list of unigue identifiers for posts related to the user.
        /// </summary>
        public List<int> Posts { get; set; }
        
        /// <summary>
        /// A list of unigue identifiers for RSVPs related to the user.
        /// </summary>
        public List<int> RSVPs { get; set; }
    }
}
