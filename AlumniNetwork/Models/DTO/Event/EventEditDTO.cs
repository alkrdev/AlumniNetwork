﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.Event
{
    public class EventEditDTO
    {
        /// <summary>
        /// The main body content of the event.
        /// </summary>
        /// <example>Noroff 2020 Winter 5 Year Anniversary</example>
        [Required]
        [MaxLength(500)]
        public string Body {get; set;}

        /// <summary>
        /// URL to the banner image of the event.
        /// </summary>
        /// <example>https://cdn.pixabay.com/photo/2017/07/21/23/57/concert-2527495__480.jpg</example>
        [Url]
        public string BannerImage {get; set;}

        /// <summary>
        /// The start time of the event.
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The end time of the event.
        /// </summary>
        public DateTime EndTime { get; set; }
    }
}
