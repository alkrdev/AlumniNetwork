﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.DTO.Event
{
    public class EventReadDTO
    {
        /// <summary>
        /// The unique identifier for the event.
        /// </summary>
        /// <example>1</example>
        public int Id {get; set;}
        
        /// <summary>
        /// The main body content of the event.
        /// </summary>
        /// <example>Noroff 2020 Winter 5 Year Anniversary</example>
        public string Body {get; set;}

        /// <summary>
        /// URL to the banner image of the event.
        /// </summary>
        /// <example>https://cdn.pixabay.com/photo/2017/07/21/23/57/concert-2527495__480.jpg</example>
        public string BannerImage {get; set;}

        /// <summary>
        /// The start time of the event.
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The end time of the event.
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// List of posts tied to the event by Id.
        /// </summary>
        /// <example>[
        /// 2
        /// ]</example>
        public List<int> Posts {get; set;}
        
        /// <summary>
        /// List of users tied to the event by Id.
        /// </summary>
        /// <example>[
        /// 2
        /// ]</example>
        public List<int> Users {get; set;}
        
        /// <summary>
        /// List of RSVPs tied to the event by Id.
        /// </summary>
        /// <example>[
        /// 2
        /// ]</example>        
        public List<int> RSVPs {get; set;}
        
        /// <summary>
        /// List of groups tied to the event by Id.
        /// </summary>
        /// <example>[
        /// 2
        /// ]</example>        
        public List<int> Groups {get; set;}
        
        /// <summary>
        /// List of topics tied to the event by Id.
        /// </summary>
        /// <example>[
        /// 2
        /// ]</example>        
        public List<int> Topics {get; set;}     
        
        public List<AttendingUser>? AttendingUsers {get; set;}
    }
}
