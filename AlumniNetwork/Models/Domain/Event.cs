﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Represents a single event.
    /// </summary>
    public class Event
    {
        /// <summary>
        /// The unique identifier for the event.
        /// </summary>
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(500)]
        public string Body { get; set; }
        
        [Url]
        public string? BannerImage { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        //[Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public DateTime LastUpdated { get; set; }

        // Relationships
        // public User CreatedByUser { get; set; }
        public ICollection<Post>? Posts { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<RSVP>? RSVPs { get; set; }
        public ICollection<Group>? Groups { get; set; }
        public ICollection<Topic>? Topics { get; set; }
    }
}
