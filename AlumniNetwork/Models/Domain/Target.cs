﻿namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Enum of possible targets for a post.
    /// </summary>
    public enum Target
    {
        User,
        Group,
        Topic,
        Event
    }
}
