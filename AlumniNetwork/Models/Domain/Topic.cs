﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Represents a single topic.
    /// </summary>
    public class Topic
    {
        /// <summary>
        /// The unique identifier for the topic.
        /// </summary>
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [Required]
        [MaxLength(250)]
        public string Description { get; set; }


        // Relationships
        public ICollection<User>? Members { get; set; }
        public ICollection<Event>? Events { get; set; }
    }
}
