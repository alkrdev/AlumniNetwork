﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Represents a single user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// The unique identifier for the user.
        /// </summary>
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string Subject { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        [Url]
        public string? ProfileImage { get; set; }

        [MaxLength(250)]
        public string? Status { get; set; }

        [MaxLength(250)]
        public string? FunFact { get; set; }

        // Relationships

        public ICollection<Event>? Events { get; set; }
        public ICollection<Group>? Groups { get; set; }
        public ICollection<Topic>? Topics { get; set; }
        public ICollection<Post>? PostsCreated { get; set; }
        public ICollection<Post>? Posts { get; set; }
        public ICollection<RSVP>? RSVPs { get; set; }

    }
}
