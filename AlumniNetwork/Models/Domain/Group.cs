﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Represents a single group.
    /// </summary>
    public class Group
    {
        /// <summary>
        /// The unique identifier for the group.
        /// </summary>
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [Required]
        [MaxLength(250)]
        public string Description { get; set; }

        [Url]
        public string? GroupImage {get; set; }
        public bool IsPrivate { get; set; }

        // Relationships
        public ICollection<User>? Members { get; set; }
        public ICollection<Event>? Events { get; set; }
    }
}
