﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Represents a single RSVP.
    /// </summary>
    public class RSVP
    {
        /// <summary>
        /// The unique identifier for the RSVP.
        /// </summary>
        [Key]
        public int Id { get; set; }
        public int GuestCount { get; set; }
        public DateTime LastUpdated { get; set; }

        // Relationships
        public int EventId { get; set; }
        [ForeignKey("EventId")]
        public Event Event { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

    }
}
