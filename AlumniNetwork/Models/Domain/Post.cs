﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetwork.Models.Domain
{
    /// <summary>
    /// Represents a single post.
    /// </summary>
    public class Post
    {
        /// <summary>
        /// The unique identifier for the post.
        /// </summary>
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(500)]
        public string Body { get; set; }
        public Target PostTarget { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        // Relationships

        public int? CreatedByUserId { get; set; }
        public int? TargetUserId { get; set; }

        [ForeignKey("CreatedByUserId")]
        [InverseProperty("PostsCreated")]
        public User? CreatedByUser { get; set; }

        [ForeignKey("TargetUserId")]
        [InverseProperty("Posts")]
        public User? TargetUser { get; set; }
        public Event? TargetEvent { get; set; }
        public Group? TargetGroup { get; set; }
        public Topic? TargetTopic { get; set; }
        public Post? ReplyParent { get; set; }
    }
}
