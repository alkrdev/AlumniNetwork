﻿using AlumniNetwork.Models.Domain;

namespace AlumniNetwork.Interfaces
{
    /// <summary>
    /// Interface for the UserRepository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Get a authenticated user reference.
        /// </summary>
        /// <param name="authtoken">The authenticated token</param>
        /// <returns>Returns <see cref="StatusCodes.Status303SeeOther"/> with the location header set to the URL of the currently authenticated user’s profile.</returns>
        Task<User> GetUserAsync(string authtoken);

        /// <summary>
        /// Get a user by user Id.
        /// </summary>
        /// <param name="userId">The Id of the user to get</param>
        /// <returns><see cref="User"/>: Profile information pertaining to the referenced user</returns>
        Task<User> GetUserByIdAsync(int userId);
        
        /// <summary>
        /// Return the user Id of user with the given subject.
        /// </summary>
        /// <param name="jwt">bearer token</param>
        Task<User> GetUserFromJWTAsync(string jwt);


        /// <summary>
        /// Create a new user in the database
        /// </summary>
        /// <param name="user">A User object with the new user values</param>
        /// <returns><see cref="User"/> Object with the newly inserted values.</returns>
        Task<User> CreateUserAsync(User user);

        /// <summary>
        /// Update a user by Id. Makes a partial update to the user object with the options that have been supplied.
        /// </summary>
        /// <param name="user">A User object with the updated values</param>
        /// <returns><see cref="User"/> Object with the updated values.</returns>
        Task<User> UpdateUserByIdAsync(User user);

        /// <summary>
        /// Check if a user exists by Id.
        /// </summary>
        /// <param name="id">Id to check for</param>
        /// <returns><see cref="bool"/>: True if user is found in database, else false</returns>
        bool UserExists(int id);

        /// <summary>
        /// Get the current amount of users in the database
        /// </summary>
        /// <returns>amount of users</returns>
        int GetUserCountAsync();
    }
}
