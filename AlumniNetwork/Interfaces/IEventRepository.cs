﻿using AlumniNetwork.Models.Domain;
using AlumniNetwork.Models.DTO;

namespace AlumniNetwork.Interfaces
{
    /// <summary>
    /// Interface for the EventRepository
    /// </summary>
    public interface IEventRepository
    {
        
        /// <summary>
        /// Get a page of events the user is subscribed to.
        /// <br/> Optionally use a search query matching a group's, event's, or topic's name.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="userId">User Id of the authenticated user</param>
        /// <param name="searchQuery">Search query string used to filter events with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit"></param>
        /// <returns><see cref="List{T}"/>: List of <see cref="Event"/> objects</returns>
        Task<List<Event>> GetEventsAsync(int userId, string searchQuery, int offset, int limit);

        /// <summary>
        /// Get an event by its Id.
        /// </summary>
        /// <param name="eventId">Id of the event to fetch</param>
        /// <returns>The <see cref="Event"/> Object with designated id</returns>
        Task<Event> GetEventByIdAsync(int eventId);

        /// <summary>
        /// Get events by group Id.
        /// </summary>
        /// <param name="groupId">Id of the group to fetch events from</param>
        /// <returns>List of <see cref="Event"/> Objects found in the group</returns>
        Task<List<Event>> GetEventsbyGroupAsync(int groupId);

        /// <summary>
        /// Create an event. Accepts appropriate parameters in the request body as application/json.
        /// </summary>
        /// <param name="userId">User Id of the authenticated user</param>
        /// <param name="newEvent"> Event object created with parameters in the body</param>
        /// <returns>Newly created <see cref="Event"/> object</returns>
        Task<Event> CreateEventAsync(int userId, Event newEvent);

        /// <summary>
        /// Update an existing event by event Id.
        /// Accepts appropriate parameters in the request body as application/json.
        /// </summary>
        /// <param name="eventId">Event Id of event to update</param>
        /// <param name="updatedEvent">New Event object created with parameters in the body</param>
        /// <returns>Updated <see cref="Event"/> object</returns>
        Task<Event> UpdateEventAsync(int eventId, Event updatedEvent);

        /// <summary>
        /// Create an invite for an event for a specified group.
        /// </summary>
        /// <param name="eventId">Event Id of event to create invite for</param>
        /// <param name="groupId">Group Id of group to create invite for</param>
        Task CreateEventGroupInviteAsync(int eventId, int groupId);

        /// <summary>
        /// Remove exisiting invite for an event for a group.
        /// </summary>
        /// <param name="eventId">Event Id of event to remove invite for</param>
        /// <param name="groupId">Group Id of group to remove invite for</param>
        Task DeleteGroupEventInviteAsync(int eventId, int groupId);

        /// <summary>
        /// Create an invite for an event for a specified topic.
        /// </summary>
        /// <param name="eventId">Event Id of event to create invite for</param>
        /// <param name="topicId">Topic Id of topic to create invite for</param>
        Task CreateEventTopicInviteAsync(int eventId, int topicId);

        /// <summary>
        /// Remove existing invite for an event for a specified topic.
        /// </summary>
        /// <param name="eventId">Event Id of event to remove invite for</param>
        /// <param name="topicId">Topic Id of event to remove invite for</param>
        Task DeleteTopicEventInviteAsync(int eventId, int topicId);

        /// <summary>
        /// Create an invite for an event for a specific user.
        /// </summary>
        /// <param name="eventId">Event ID of event to create invite for</param>
        /// <param name="invitedUserId">User Id of user to create invite for</param>
        Task CreateEventUserInviteAsync(int eventId, int invitedUserId);

        /// <summary>
        /// Remove an invite for an event, for a specific user.
        /// </summary>
        /// <param name="eventId">Event Id to remove invite for</param>
        /// <param name="invitedUserId">User Id to remove invite for</param>
        Task DeleteUserEventInviteAsync(int eventId, int invitedUserId);

        /// <summary>
        /// Create a new RSVP. Accepts appropriate parameters in the body as application/json.
        /// </summary>
        /// <param name="eventId">Event Id to create RSVP for</param>
        /// <param name="rsvp">RSVP object created with parameters in the body</param>
        /// <returns>Newly created <see cref="RSVP"/> object</returns>
        Task<RSVP> CreateEventRSVPAsync(int eventId, RSVP rsvp);

        /// <summary>
        /// Get a list of events user has RSVPed for
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<Event>> GetEventsForUserAsync(int userId);

        /// <summary>
        /// Checks if the user is invited to the event.
        /// </summary>
        /// <param name="userId">User Id to check with</param>
        /// <param name="eventId">Event Id to check for</param>
        /// <returns><see cref="bool"/>: True if user is invited or member of group or topic that is. Else false.</returns>
        bool CheckUserIsInvited(int userId, int eventId);


        /// <summary>
        /// Checks if the user is the owner of an event.
        /// </summary>
        /// <param name="userId">User Id to check with</param>
        /// <param name="eventId">Event Id to check for</param>
        /// <returns><see cref="bool"/>: True if user is owner of event, false if not.</returns>
        bool CheckUserIsOwner(int userId, int eventId);

        /// <summary>
        /// Checks if the user is a member of target audiences of an event.
        /// </summary>
        /// <param name="userId">User Id to check</param>
        /// <param name="checkEvent">Event to check</param>
        bool CheckUserIsMember(int userId, Event checkEvent);
        
        /// <summary>
        /// Check if the event exists in the database
        /// </summary>
        /// <param name="eventId">Id of the event to check</param>
        bool EventExists(int eventId);

        /// <summary>
        /// Check if a specific RSVP already exists.
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        bool RSVPExists(int eventId, int userId);

        /// <summary>
        /// Delete a RSVP
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        Task DeleteEventRSVPAsync(int eventId, int userId);

        /// <summary>
        /// Get a list of users who have confirmed they are attending an event
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        Task<List<AttendingUser>> GetAttendingUsersAsync(int eventId);
    }
}
