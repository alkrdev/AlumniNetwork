﻿using AlumniNetwork.Models.Domain;
using Microsoft.AspNetCore.Mvc;

namespace AlumniNetwork.Interfaces
{
    /// <summary>
    /// Interface for the GroupRepository
    /// </summary>
    public interface IGroupRepository
    {
        /// <summary>
        /// Add a new group member.
        /// </summary>
        /// <param name="userId">User Id of the authenticated user</param>
        /// <param name="groupId">The group Id to add the membership in</param>
        /// <returns><see cref="Group"/> object with the added members.</returns>
        Task<Group> AddGroupMemberAsync(int userId, int groupId);

        /// <summary>
        /// Create a new group. Simultaneously creates a group membership record, adding the
        /// group’s creator as the first member of the group.
        /// </summary>
        /// <param name="userId">User Id of the authenticated user</param>
        /// <param name="newGroup">Group object to create</param>
        /// <returns>Newly Created <see cref="Group"/> object.</returns>
        Task<Group> CreateGroupAsync(int userId, Group newGroup);


        /// <summary>
        /// Get a list of all groups.
        /// <br/> Optionally use a search query matching a group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <remarks>Groups that are private of which the requesting user is not a member is filtered out of search results before returning.</remarks>
        /// <param name="userId">User Id of the authenticated user</param>
        /// <param name="searchQuery">Search query string used to filter groups with</param>
        /// <param name="limit">Limit to stop the pagination at</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <returns><see cref="List{T}"/>: List of <see cref="Group"/> objects.</returns>
        Task<List<Group>> GetGroupsAsync(int userId, string searchQuery, int offset, int limit);

        /// <summary>
        /// Get a Group by group Id.
        /// </summary>
        /// <param name="groupId">The Id of the group to get</param>
        /// <returns>Fetched <see cref="Group"/> object.</returns>
        Task<Group> GetGroupByIdAsync(int groupId);

        /// <summary>
        /// Remove a member from a group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        Task<Group> RemoveGroupMemberAsync(int userId, int groupId);

        /// <summary>
        /// Check if the user is the owner of a group.
        /// </summary>
        /// <param name="groupId">The Id of the group to check</param>
        /// <param name="userId">The Id of the user to check</param>
        /// <returns><see cref="bool"/>: True if user is owner of group, false if not.</returns>
        bool CheckUserIsOwner(int userId, int groupId);

        /// <summary>
        /// Check if the user is a member of a group.
        /// </summary>
        /// <param name="groupId">The Id of the group to check</param>
        /// <param name="userId">The Id of the user to check</param>
        /// <returns><see cref="bool"/>: True if user is member of group, false if not.</returns>
        bool CheckUserIsMember(int userId, int groupId);

        /// <summary>
        /// Check if a group exist in the database
        /// </summary>
        /// <param name="groupId">The Id of the group to check</param>
        /// <returns><see cref="bool"/>: True if the group exists, false if not.</returns>
        bool GroupExists(int groupId);


    }
}
