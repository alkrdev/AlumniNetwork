﻿using AlumniNetwork.Models.Domain;
using Microsoft.AspNetCore.Mvc;

namespace AlumniNetwork.Interfaces
{
    /// <summary>
    /// Interface for handling requests related to groups.
    /// </summary>
    public interface IPostRepository
    {
        /// <summary>
        /// Get a list of posts to groups, topics, and events for which the user is subscribed.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="userId">User Id of the authenticated user</param>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<List<Post>> GetSubsribedPostsAsync(int userId, string searchQuery, int offset, int limit);

        /// <summary>
        /// Get a list of posts that were sent as direct messages to the requesting user.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="userId">Id of the user requesting the posts</param>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<List<Post>> GetDirectPostsAsync(int userId, string searchQueryString, int offset, int limit);

        /// <summary>
        /// Get a post by its unique identifier.
        /// </summary>
        /// <param name="postId">Id of post to fetch</param>
        /// <returns><see cref="Post"/>: Fetched post object.</returns>
        Task<Post> GetPostByIdAsync(int postId);

        /// <summary>
        /// Get a list of posts that were sent as direct messages to the requesitng user, by a specific user.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="requestingUserId">User Id of user requesting posts</param>
        /// <param name="sendingUserId">User Id of user sending posts</param>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<List<Post>> GetDirectPostsByUserAsync(int requestingUserId, int sendingUserId, string searchQueryString, int offset, int limit);

        /// <summary>
        /// Get a list of posts that were sent with the group described by group Id as the target audience.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="groupId">Group Id of group specified as target for posts</param>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<List<Post>> GetPostsByGroupAsync(int groupId, string searchQueryString, int offset, int limit);

        /// <summary>
        /// Get a list of posts that were sent with the topic described by topic Id as the target audience.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="topicId">Topic Id of topic specified as target for posts</param>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<List<Post>> GetPostsByTopicAsync(int topicId, string searchQueryString, int offset, int limit);

        /// <summary>
        /// Get a list of posts that were sent with the event described by event Id as the target audience.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="eventId">Event Id of event specified as target for posts</param>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<List<Post>> GetPostsByEventAsync(int eventId, string searchQueryString, int offset, int limit);

        /// <summary>
        /// Create a new posts. Accepts appropriate parameters in the request body as application/json.
        /// </summary>
        /// <remarks>Attempts to post to an audience for which the requesting user is not a member will result in a <see cref="StatusCodes.Status403Forbidden"/> response.</remarks>
        /// <param name="userId">Authenticated user Id</param>
        /// <param name="newPost">New post object created with parameters in the body</param>
        /// <returns><see cref="List{T}"/>: List of fetched <see cref="Post"/> objects.</returns>
        Task<Post> CreatePostAsync(int userId, Post newPost);

        /// <summary>
        /// Update an existing post by Id. Accepts appropriate parameters in the request body as application/json.
        /// </summary>
        /// <remarks>The audience of a post may not be changed after creation. Attempts to do so will result in a <see cref="StatusCodes.Status403Forbidden"/> response.</remarks>
        /// <param name="userId">Authenticated user Id</param>
        /// <param name="postId">Post Id of post to update</param>
        /// <param name="updatedPost">New post object created with parameters in the body</param>
        /// <returns>Newly created <see cref="Post"/> object.</returns>
        Task<Post> UpdatePostAsync(int userId, int postId, Post updatedPost);

        /// <summary>
        /// Check if a post exists in the database
        /// </summary>
        /// <param name="postId">Id used to check for post</param>
        /// <returns>Updated <see cref="Post"/> object.</returns>
        bool PostExists(int postId);
        
        /// <summary>
        /// Check if the requesting user is allowed to post to the target audience
        /// </summary>
        /// <param name="userId">Id of requesting user</param>
        /// <param name="post">Post to check targets of</param>
        /// <returns><see cref="bool"/>: True if user is allowed to post to a target, false if not.</returns>
        bool AllowedToPostToTarget(int userId, Post post);

    }
}
