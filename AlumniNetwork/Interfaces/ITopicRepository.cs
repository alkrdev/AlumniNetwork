﻿using AlumniNetwork.Models.Domain;
using Microsoft.AspNetCore.Mvc;

namespace AlumniNetwork.Interfaces
{
    /// <summary>
    /// Interface for handling requests related to groups.
    /// </summary>
    public interface ITopicRepository
    {
        /// <summary>
        /// Get a list of topics.
        /// <br/> Optionally use a search query matching a posts body, sending user's name, or target group's, event's, or topic's name or description.
        /// <para>Supports offset and limit.</para>
        /// </summary>
        /// <param name="searchQueryString">Search query string used to filter posts with</param>
        /// <param name="offset">Offset to start the pagination at</param>
        /// <param name="limit">Limit to stop the paginiation at</param>
        /// <returns><see cref="List{T}"/> of <see cref="Topic"/> objects.</returns>
        Task<List<Topic>> GetTopicsAsync(string searchQueryString, int offset, int limit);

        /// <summary>
        /// Get a list of topics corresponding to the provided topic Id.
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns>Fetched <see cref="Topic"/> object.</returns>
        Task<Topic> GetTopicByIdAsync(int topicId);

        /// <summary>
        /// Create a new topic. Accepts appropriate parameters in the request body as application/json.
        /// </summary>
        /// <param name="topic">Topic object created with parameters in the body</param>
        /// <returns>Newly created <see cref="Topic"/> object.</returns>
        Task<Topic> CreateTopicAsync(int userId, Topic topic);

        /// <summary>
        /// Create a new topic membership record.
        /// </summary>
        /// <param name="topicId"></param>
        Task<ActionResult> CreateTopicMembershipRecord(int userId, int topicId);

        /// <summary>
        /// Check if a user is member of a topic
        /// </summary>
        /// <param name="userId">The user Id to check</param>
        /// <param name="topicId">The topic Id to check against</param>
        /// <returns>True if the user is a member.</returns>
        bool CheckUserIsMember(int userId, int topicId);
    }
}
